import { expect } from 'chai';
import sinon from 'sinon';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import { MessageBox } from 'client/components/MessageBox';
import { Modal, Glyphicon } from 'react-bootstrap';

function setup() {
  const props = {
    messageBox: [{ message: 'test message', style: 'success', title: 'Test Title' }],
    closeMessageBox: sinon.spy()
  };

  const renderer = TestUtils.createRenderer();
  renderer.render(<MessageBox {...props} />);
  const output = renderer.getRenderOutput();

  return {
    props,
    output,
    renderer
  };
}

describe('components/MessageBox', () => {
  it('should render correctly', () => {
    const { props, output } = setup();

    expect(output.type).to.equal(Modal);
    expect(output.props.className).to.equal('message-box');
    expect(output.props.show).to.equal(true);

    const [modalHeader, modalBody, modalFooter] = output.props.children;
    const modalTitle = modalHeader.props.children;

    expect(modalTitle.props.children).to.deep.equal([
      <Glyphicon glyph="ok" className="icon-ok" />,
      <span>{props.messageBox[0].title}</span>
    ]);

    expect(modalBody.props.children).to.deep.equal(<div>{props.messageBox[0].message}</div>);
  });

  it('should call closeMessageBox when clicking OK button', () => {
    const { props, output } = setup();
    const modalFooter = output.props.children[2];
    const okButton = modalFooter.props.children;

    okButton.props.onClick();
    sinon.assert.calledOnce(props.closeMessageBox);
  });
});
