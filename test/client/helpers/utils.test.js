import { expect, assert } from 'chai';
import nock from 'nock';
import sinon from 'sinon';
import config from 'client/config';
import * as utils from 'client/helpers/utils';
import consts from 'client/helpers/consts';

describe('helpers/utils', () => {

  describe('callApi function', () => {
    afterEach(() => {
      nock.cleanAll();
    });

    it('should make GET request when invalid method is provided', () => {
      const expectedResponse = { body: { success: true } };

      nock(config.API_URL)
        .get('/test')
        .reply(200, expectedResponse);

      const callback = sinon.spy();

      return utils.callApi('/test', 'invalid_method')
        .then(callback)
        .then(() => {
          sinon.assert.calledWith(callback, expectedResponse);
        });
    });
  });

  describe('createApiReducer function', () => {
    beforeEach(() => {
      sinon.spy(utils, 'createApiReducer');
    });
    afterEach(() => {
      utils.createApiReducer.restore();
    });

    it('should throw an error when number of passed arguments are not three', () => {
      // test using sinon.spy
      try {
        utils.createApiReducer(['request_mock_type', 'success_mock_type']);
      } catch (e) { // eslint-disable-line no-empty
      }
      assert(utils.createApiReducer.threw());
    });

    it('should throw an error when passed arguments are not strings', () => {
      expect(utils.createApiReducer.bind(null, [1, 2, 3]))
        .to.throw('API reducer generator: Expected action types to be strings.');
    });

    describe('should create a reducer function ', () => {
      const reducer = utils.createApiReducer(['request_mock_type', 'success_mock_type', 'failure_mock_type']);

      it('that returns initial state with API_NOT_LOADED status', () => {
        expect(reducer(undefined, {}))
          .to.deep.equal({ status: consts.API_NOT_LOADED, error: '' });
      });

      it('that returns state with API_LOADING status for API request action type', () => {
        expect(reducer({}, { type: 'request_mock_type' }))
          .to.deep.equal({ status: consts.API_LOADING, error: '' });
      });

      it('that returns state with API_LOADED_SUCCESS status for API success action type', () => {
        expect(reducer({}, { type: 'success_mock_type' }))
          .to.deep.equal({ status: consts.API_LOADED_SUCCESS, error: '' });
      });

      it('that returns state with API_LOADED_ERROR status for API failure action type', () => {
        expect(reducer({}, { type: 'failure_mock_type', error: { message: 'mock error message' } }))
          .to.deep.equal({ status: consts.API_LOADED_ERROR, error: { message: 'mock error message' } });
      });

    });
  });

});
