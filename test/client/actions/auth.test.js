import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import apiMiddleware from 'client/middleware/api';
import nock from 'nock';
import * as authActions from 'client/actions/auth';
import config from 'client/config';

const mockStore = configureMockStore([thunkMiddleware, apiMiddleware]);

describe('actions/auth', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('signupUser makes API request for signup and creates SIGNUP_SUCCESS action when successful', () => {
    const signupData = {
      name: 'Test Person',
      email: 'test@email.com',
      password: 'testp@ss'
    };

    nock(config.API_URL)
      .post('/signup', signupData)
      .reply(200, { body: { success: true } });

    const expectedActions = [
      { type: authActions.SIGNUP_REQUEST },
      { type: authActions.SIGNUP_SUCCESS, response: { body: { success: true } } }
    ];
    const store = mockStore({});

    return store.dispatch(authActions.signupUser(signupData))
      .then(() => {
        expect(store.getActions()).to.deep.equal(expectedActions);
      });
  });

  it('signupUser makes API request for signup and creates SIGNUP_FAILURE action when failed', () => {
    const signupData = {
      name: 'Test Person',
      email: 'test@email.com',
      password: 'testp@ss'
    };

    nock(config.API_URL)
      .post('/signup', signupData)
      .reply(400, { error: { message: 'Email already exists' } });

    const expectedActions = [
      { type: authActions.SIGNUP_REQUEST },
      { type: authActions.SIGNUP_FAILURE, error: { message: 'Email already exists' } }
    ];
    const store = mockStore({});

    return store.dispatch(authActions.signupUser(signupData))
      .catch(() => {
        expect(store.getActions()).to.deep.equal(expectedActions);
      });
  });
});
