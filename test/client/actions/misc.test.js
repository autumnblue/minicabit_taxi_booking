import { expect } from 'chai';
import * as miscActions from 'client/actions/misc';

describe('actions/misc', () => {
  it('should create an action to open a message box', () => {
    const testPayload = {
      message: 'test message box',
      style: 'success',
      title: 'Test'
    };
    const expectedAction = {
      type: miscActions.OPEN_MESSAGEBOX,
      ...testPayload
    };
    expect(miscActions.openMessageBox(testPayload.message, testPayload.style, testPayload.title))
      .to.deep.equal(expectedAction);
  });
});
