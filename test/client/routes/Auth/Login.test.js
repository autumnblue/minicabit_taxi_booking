import { expect } from 'chai';
import sinon from 'sinon';
import TestUtils from 'react-addons-test-utils';
import React from 'react';
import { Login } from 'client/routes/Auth/Login';

const innerSubmitHandler = sinon.spy();

function setup() {
  const props = {
    fields: {
      email: {},
      password: {}
    },
    handleSubmit: () => innerSubmitHandler,
    submitting: false
  };

  const renderer = TestUtils.createRenderer();
  renderer.render(<Login {...props} />);
  const output = renderer.getRenderOutput();

  return {
    props,
    output,
    renderer
  };
}

describe('routes/Auth/Login', () => {
  it('should render correctly', () => {
    const { output } = setup();

    expect(output.type).to.equal('div');
    expect(output.props.id).to.equal('page-login');
  });

  it('should call handleSubmit when submitting form', () => {
    const { output } = setup();
    const form = output.props.children[1];

    form.props.onSubmit();
    sinon.assert.calledOnce(innerSubmitHandler);
  });
});
