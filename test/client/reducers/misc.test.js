import { expect } from 'chai';
import miscReducers from 'client/reducers/misc';
import * as miscActions from 'client/actions/misc';

const { messageBox } = miscReducers;

describe('reducers/misc', () => {

  describe('messageBox reducer', () => {

    it('should return initial state null', () => {
      expect(messageBox(undefined, {}))
        .to.deep.equal([]);
    });

    it('should handle openning message box', () => {
      expect(messageBox(
        [],
        { message: 'test message box 1', style: 'success', title: 'Test1', type: miscActions.OPEN_MESSAGEBOX }
      ))
        .to.deep.equal([
        { message: 'test message box 1', style: 'success', title: 'Test1' }
        ]);

      expect(messageBox(
        [{ message: 'test message box 1', style: 'success', title: 'Test1' }],
        { message: 'test message box 2', style: 'error', title: 'Test2', type: miscActions.OPEN_MESSAGEBOX }
      ))
        .to.deep.equal([
          { message: 'test message box 1', style: 'success', title: 'Test1' },
          { message: 'test message box 2', style: 'error', title: 'Test2' }
        ]);
    });

    it('should handle closing message box', () => {
      expect(messageBox([
          { message: 'test message box 1', style: 'success', title: 'Test1' },
          { message: 'test message box 2', style: 'error', title: 'Test2' }
      ],
        { type: miscActions.CLOSE_MESSAGEBOX }
      ))
        .to.deep.equal([{ message: 'test message box 2', style: 'error', title: 'Test2' }]);

      expect(messageBox(
        [{ message: 'test message box 1', style: 'success', title: 'Test1' }],
        { type: miscActions.CLOSE_MESSAGEBOX }
      ))
        .to.deep.equal([]);

      expect(messageBox(
        [],
        { type: miscActions.CLOSE_MESSAGEBOX }
      ))
        .to.deep.equal([]);
    });
    
  });

});
