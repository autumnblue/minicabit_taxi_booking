import { expect } from 'chai';
import { user } from 'client/reducers/auth';
import * as authActions from 'client/actions/auth';

describe('reducers/auth', () => {

  describe('user reducer', () => {

    it('should return initial state null', () => {
      expect(user(undefined, {}))
        .to.deep.equal(null);
    });

    it('should handle login and logout', () => {
      expect(user({}, { type: authActions.LOGIN_SUCCESS, response: { user_details: 'mock data' } }))
        .to.deep.equal({ user_details: 'mock data' });
      expect(user({}, { type: authActions.LOGOUT_SUCCESS }))
        .to.deep.equal(null);
    });

  });

});
