/**
 * Created by dejan on 26/07/16.
 */

var testHelper = exports = module.exports = {};

testHelper.check = function check(done, f) {
  try {
    f();
    done();
  } catch (e) {
    done(e);
  }
};
