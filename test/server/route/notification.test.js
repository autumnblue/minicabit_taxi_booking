/**
 * Created by dejan on 11/08/16.
 */

var should = require('chai').should;
var assert = require('chai').assert;

var app = require('../../../server/server.js');
var request = require('supertest');
var testHelper = require('../testHelper');

describe('test suit for notifications', function () {

  var restApiRoot;

  before(function () {
    restApiRoot = app.get('restApiRoot');
  });

  this.timeout(2000);

  it('POST an valid request for resend confirmation email for customer and get 200 OK', function (done) {
    var validRequest = {
      bookingRef: '3369327-1445',
      receiver: 'customer',
      type: 'confirmation'
    };

    request(app)
      .post(restApiRoot + '/notifications/resend')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
          assert.equal(res.body.data, 'Confirmation mail send');
        });
      });
  });

  it('POST an valid request for resend cancellation email for pho and get 200 OK', function (done) {
    var validRequest = {
      bookingRef: '1855574272-1445',
      receiver: 'pho',
      type: 'cancellation'
    };

    request(app)
      .post(restApiRoot + '/notifications/resend')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
          assert.equal(res.body.data, 'Cancellation email send to pho');
        });
      });
  });

  it('POST an invalid request for resend email and get 400', function (done) {
    var validRequest = {
      bookingRef: '1855574272-1445',
      receiver: 'pho',
      type: 'cancellationsadas'
    };

    request(app)
      .post(restApiRoot + '/notifications/resend')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
        });
      });
  });
});
