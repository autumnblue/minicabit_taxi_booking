/**
 * Created by dejan on 26/07/16.
 */

var should = require('chai').should;
var assert = require('chai').assert;

var app = require('../../../server/server.js');
var request = require('supertest');
var testHelper = require('../testHelper');

describe('test suit for pricings', function () {

  var restApiRoot;

  before(function () {
    restApiRoot = app.get('restApiRoot');
  });

  this.timeout(20000);
  it('PUT an valid request for update uplift pricing and get 201 OK', function (done) {

    var validRequest = {
      '1-4Estate': {
        multiply: 10,
        multiplyType: 2
      }
    };

    request(app)
      .put(restApiRoot + '/pricing/general/uplift/3324')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
        });
      });
  });

  it('PUT an valid request for update uplift pricing and get 201 OK', function (done) {

    var validRequest = {
      '5-6': {
        multiply: 20,
        multiplyType: 2
      }
    };

    request(app)
      .put(restApiRoot + '/pricing/general/uplift/3324')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
        });
      });
  });

  it('PUT an valid request for update uplift pricing and get 201 OK', function (done) {

    var validRequest = {
      '7': {
        multiply: 50,
        multiplyType: 1
      }
    };

    request(app)
      .put(restApiRoot + '/pricing/general/uplift/3324')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
        });
      });
  });

  it('PUT an valid request for update uplift pricing and get 201 OK', function (done) {

    var validRequest = {
      '8': {
        multiply: 100,
        multiplyType: 1
      }
    };

    request(app)
      .put(restApiRoot + '/pricing/general/uplift/3324')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
        });
      });
  });

  it('should successfully get uplift pricing records for mco 3324', function (done) {
    request(app)
      .get(restApiRoot + '/pricing/general/uplift/3324')
      .expect(200)
      .end(function (err, res) {
        assert.ifError(err);
        assert.equal(res.body.data.estateLuggageMultiplayer, 10);
        assert.equal(res.body.data.estateLuggageMultiplayerType, 2);
        done();
      });
  });

  it('PUT an invalid request for update uplift pricing and get 400', function (done) {

    var invalidRequest = {
      '1-4Estatelalala': {
        multiply: 10,
        multiplyType: 2
      }
    };

    request(app)
      .put(restApiRoot + '/pricing/general/uplift/3324')
      .set('Content-Type', 'application/json')
      .send(invalidRequest)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
        });
      });
  });

  it('PUT an invalid request for update uplift pricing and get 400', function (done) {

    var invalidRequest = {
      '6': {
        multiplyType: 2
      }
    };

    request(app)
      .put(restApiRoot + '/pricing/general/uplift/3324')
      .set('Content-Type', 'application/json')
      .send(invalidRequest)
      .expect(400)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
        });
      });
  });

  it('should successfully get graduatedPricing records for mco 3324', function (done) {
    request(app)
      .get(restApiRoot + '/pricing/mileage/3324')
      .expect(200)
      .end(function (err, res) {
        assert.ifError(err);
        done();
      });
  });

  it('PUT an valid request for update ceiling and get 201 OK', function (done) {

    var validRequest = {
      tier1Ceiling: 2,
      tier2Ceiling: 10,
      tier3Ceiling: 20,
      tier4Ceiling: 50,
      tier5Ceiling: 500
    };

    request(app)
      .put(restApiRoot + '/pricing/mileage/ceiling/3324')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        testHelper.check(done, function () {
        });
      });
  });

  it('PUT an valid request for update rate and get 201 OK', function (done) {

    var validRequest = {
      '1-4': {
        tier1Rate: 28,
        tier2Rate: 22,
        tier3Rate: 22,
        tier4Rate: 22,
        tier5Rate: 24
      }
    };

    var returnedData;

    request(app)
      .put(restApiRoot + '/pricing/graduate/rate/3324')
      .set('Content-Type', 'application/json')
      .send(validRequest)
      .expect(201)
      .expect('Content-Type', /json/)
      .end(function (err, res) {
        if (err) {
          return done(err);
        }
        for (let i = 0; i < res.body.data.length; i++) {
          if (res.body.data[i].numPassengers === 4) {
            returnedData = res.body.data[i];
            break;
          }
        }

        testHelper.check(done, function () {
          assert.equal(returnedData.mcoId, 3324);
          assert.equal(returnedData.numPassengers, 4);
          assert.equal(returnedData.tier1Rate, 28);
          assert.equal(returnedData.tier2Rate, 22);
          assert.equal(returnedData.tier3Rate, 22);
          assert.equal(returnedData.tier4Rate, 22);
          assert.equal(returnedData.tier5Rate, 24);
        });
      });
  });

  it('GET mileage price for a particular MCO for 12 mileage, and code 200', function (done) {
    request(app)
      .get(restApiRoot + '/pricing/mileage/calculator/3324?mileage=12')
      .expect(200)
      .end(function (err, res) {
        assert.ifError(err);
        assert.equal(res.body.data['1-4'].price, 276);
        assert.equal(res.body.data['1-4Estate'].price, 286);
        assert.equal(res.body.data['5-6'].price, 296);
        assert.equal(res.body.data['7'].price, 414);
        assert.equal(res.body.data['8'].price, 552);
        done();
      });
  });

  it('GET mileage price for a particular MCO for 1 mileage, and code 200', function (done) {
    request(app)
      .get(restApiRoot + '/pricing/mileage/calculator/3324?mileage=1')
      .expect(200)
      .end(function (err, res) {
        assert.ifError(err);
        assert.equal(res.body.data['1-4'].price, 28);
        assert.equal(res.body.data['1-4Estate'].price, 38);
        assert.equal(res.body.data['5-6'].price, 48);
        assert.equal(res.body.data['7'].price, 42);
        assert.equal(res.body.data['8'].price, 56);
        done();
      });
  });

  it('GET mileage price for a particular MCO for 34 mileage, and code 200', function (done) {
    request(app)
      .get(restApiRoot + '/pricing/mileage/calculator/3324?mileage=34')
      .expect(200)
      .end(function (err, res) {
        assert.ifError(err);
        assert.equal(res.body.data['1-4'].price, 760);
        assert.equal(res.body.data['1-4Estate'].price, 770);
        assert.equal(res.body.data['5-6'].price, 780);
        assert.equal(res.body.data['7'].price, 1140);
        assert.equal(res.body.data['8'].price, 1520);
        done();
      });
  });
});
