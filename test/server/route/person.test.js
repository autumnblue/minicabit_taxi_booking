/*
import chai from 'chai';
import server from './../../../server/server';
import supertest from 'supertest';
import _ from 'lodash';
let expect = chai.expect;
let request = supertest(server);

before((done) => {
  var person = server.models.person;
  var roleMapping = server.models.roleMapping;

  person.find({ where: { username: 'johndoe' }}, function(err, personResult) {
    if (err) {
      done(err);
    } else {
      if (!_.isEmpty(personResult)) {
        person.destroyById(personResult[0].id, function(err, result) {
          if (err) {
            done(err);
          } else {
            roleMapping.destroyAll({ where: { principalId: personResult[0].id }}, function(err, result) {
              if (err) {
                done(err);
              } else {
                done();
              }
            });
          }
        });
      } else {
        done();
      }
    }
  });
});

describe('POST /api/v1/person/mco/register', () => {
  it("should register MCO", (done) => {
    request
      .post("/api/v1/person//mco/register")
      .send({
        username : 'johndoe',
        password : 'test123',
        passwordRepeat: 'test123',
        officeEmail: 'john.doe@test.com',
        registeredName: 'testRegisteredName',
        dispatchSystem: 'testDispatchSystem',
        tradingName: 'testTradingName',
        website: 'http://test.com',
        operatorLicenseNumber: 'testOperatorLicenceNum',
        licenceExpiryDate: '2015-02-06 23:59:59',
        localAuthority: 'testLocalAuthoryty',
        fleetLabel: 'testExecutiveFleet',
        fleetSize: '25-60',
        officeContactTel: '123456789',
        mcoMobileNumber: '123456789'
      })
      .expect("Content-type",/json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          var data = res.body.data;
          expect(data.username).eq('johndoe');
          expect(data.email).eq('john.doe@test.com');
          done();
        }
      });
  });

  it("MCO should have MinicabitOperatorRole", (done) => {
    var person = server.models.person;
    var roleMapping = server.models.roleMapping;
    person.find({ where: { username: 'johndoe' }}, function(err, personResult) {
      if (err) {
        done(err);
      } else {
        if (!_.isEmpty(personResult)) {
          roleMapping.find({ where: { principalId: personResult[0].id }}, function(err, result) {
            if (err) {
              done(err);
            } else {
              expect(result[0].roleId).eq(3);
              done();
            }
          });
        } else {
          done();
        }
      }
    });
  });
});

describe('POST /api/v1/person/login', () => {
  it("should login user and return access token in response", (done) => {
    request
      .post("/api/v1/person/login")
      .send({
        username : 'johndoe',
        password : 'test123',
        ttl: 80400
      })
      .expect("Content-type",/json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
            done(err);
        } else {
          var data = res.body;
          expect(data.ttl).eq(80400);
          done();
        }
      });
  });

  it("should failed on login", (done) => {
    request
      .post("/api/v1/person/login")
      .send({
        username : 'johndoe',
        password : 'test1',
        ttl: 80400
      })
      .expect("Content-type",/json/)
      .expect(401)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          var data = res.body;
          expect(data.error.name).eq('Error');
          expect(data.error.message).eq('login failed');
          done();
        }
      });
  });
});
*/
