/**
 * Created by dejan on 12/08/16.
 */

var should = require('chai').should;
var assert = require('chai').assert;

var app = require('../../../server/server.js');
var request = require('supertest');
var testHelper = require('../testHelper');

describe('test suit for statement', function () {

  var restApiRoot;

  before(function () {
    restApiRoot = app.get('restApiRoot');
  });

  this.timeout(2000);

  it('should successfully get pending bookings for mco 3324', function (done) {
    request(app)
      .get(restApiRoot + '/statements/pendingBookings/3324')
      .expect(200)
      .end(function (err, res) {
        assert.ifError(err);
        done();
      });
  });

  it('should successfully get disputed bookings for mco 3324', function (done) {
    request(app)
      .get(restApiRoot + '/statements/disputedBookings/3324')
      .expect(200)
      .end(function (err, res) {
        assert.ifError(err);
        done();
      });
  });

  it('should successfully get outstanding bookings for mco 3324', function (done) {
    request(app)
      .get(restApiRoot + '/statements/outstandingBookings/3324')
      .expect(200)
      .end(function (err, res) {
        assert.ifError(err);
        testHelper.check(done, function () {
          assert.isNotNull(res.body.data.totalSummary);
          assert.isNotNull(res.body.data.bookings);
        });
      });
  });
});
