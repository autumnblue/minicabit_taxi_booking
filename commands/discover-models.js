'use strict';
var server = require('../server/server');
var dataSource = server.dataSources.db;

dataSource.discoverSchema(process.argv[2], {
  associations: true
}, function(er, schema) {
  if (er) throw er;
  console.log(JSON.stringify(schema, null, '  '));
});
