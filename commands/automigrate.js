'use strict';
var server = require('./server/server');
var dataSource = server.dataSources.db;

dataSource.automigrate(['person', 'acl', 'role', 'scope', 'roleMapping'], function(err) {
  console.log(err);
});
