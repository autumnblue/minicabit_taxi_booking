# Minicabit Admin

### Get Started

#### 1. Download the project

```shell
$ git clone -b master https://github.com/minicabit/admin.git
$ cd admin
```

#### 2. Install dependencies

- **If the installation is only for APIs on production server**
```shell
$ npm install --production
```
- **Otherwise**
```shell
$ npm install
```

#### 3. Install Gulp CLI
```shell
$ npm install --global gulp-cli
```


### Development

- **Run LoopBack server APIs and client app together for local development (with nodemon and HMR)**

```
$ gulp
```

- **You can simulate production environment using _--prod_ argument.**

```
$ gulp --prod
```

- **You can run only the server APIs without compiling and serving client app.**
    (Should be useful for backend developers... :yum:)

```
$ gulp --api-only
```

- **You can also combine _--prod_ and _--api-only_ together.**

```
$ gulp --prod --api-only
```

- **Build client in development environment**

```
$ gulp build
```


### Testing and Linting

(Append suffix **_:server_** to test/lint for server-side code, and **_:client_** for client-side code. e.g. `$ gulp mocha:client`)

- **Unit testing (suffix _:server_ and _:client_ available)**

```
$ gulp mocha
```

- **Javascript linting (suffix _:server_, _:client_ and _:tests_ available)**
```
$ gulp eslint
```

- **CSS/SASS linting**

```
$ gulp csslint
$ gulp sasslint
```

- **Unit testing and linting together (suffix _:server_ and _:client_ available)**

```
$gulp test
```


### Deployment

- **Build client for production deployment**

```
$ gulp build --prod
```
