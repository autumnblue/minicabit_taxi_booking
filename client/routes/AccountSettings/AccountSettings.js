import React, { Component, PropTypes } from 'react';
import PageTitle from 'client/components/PageTitle';
import Tabs from 'client/components/Tabs';

class AccountSettings extends Component {

  static propTypes = {
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/settings/company',
        title: 'Company details'
      },
      {
        url: '/settings/account',
        title: 'Account details'
      }
    ];
  }

  render() {
    return (
      <div id="page-settings" className="container">
        <PageTitle>Account settings</PageTitle>
        <div className="row tabs-row">
          <div className="col-md-12">
            <Tabs items={this.tabItems} />
          </div>
        </div>
        <div className="row tab-content">
          <div className="col-md-12">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default AccountSettings;
