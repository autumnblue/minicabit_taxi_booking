import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class AccountDetails extends Component {
  render() {
    return (
      <div id="page-settings-account" className="container">
        <PageTitle>Account Details</PageTitle>
      </div>
    );
  }
}

export default AccountDetails;
