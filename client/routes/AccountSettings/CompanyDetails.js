import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class CompanyDetails extends Component {
  render() {
    return (
      <div id="page-settings-company" className="container">
        <PageTitle>Company Details</PageTitle>
      </div>
    );
  }
}

export default CompanyDetails;
