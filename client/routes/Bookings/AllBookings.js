import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class AllBookings extends Component {
  render() {
    return (
      <div id="page-all-bookings" className="container">
        <PageTitle>All Bookings</PageTitle>
      </div>
    );
  }
}

export default AllBookings;
