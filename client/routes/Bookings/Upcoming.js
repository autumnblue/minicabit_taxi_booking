import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class Upcoming extends Component {
  render() {
    return (
      <div id="page-upcoming" className="container">
        <PageTitle>Upcoming bookings</PageTitle>
      </div>
    );
  }
}

export default Upcoming;
