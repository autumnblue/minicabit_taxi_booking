import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class Alerts extends Component {
  render() {
    return (
      <div id="page-alerts" className="container">
        <PageTitle>Alerts</PageTitle>
      </div>
    );
  }
}

export default Alerts;
