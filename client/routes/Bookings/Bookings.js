import React, { Component, PropTypes } from 'react';
import PageTitle from 'client/components/PageTitle';
import Tabs from 'client/components/Tabs';

class Bookings extends Component {

  static propTypes = {
    location: PropTypes.object.isRequired,
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/bookings/alerts',
        title: 'Alerts'
      },
      {
        url: '/bookings/upcoming',
        title: 'Upcoming bookings'
      },
      {
        url: '/bookings/all',
        title: 'All bookings'
      }
    ];
  }

  render() {
    return (
      <div id="page-bookings" className="container">
        <PageTitle>Bookings</PageTitle>
        <div className="row tabs-row">
          <div className="col-md-12">
            <Tabs items={this.tabItems} />
          </div>
        </div>
        <div className="row tab-content">
          <row className="col-md-12">
            {this.props.children}
          </row>
        </div>
      </div>
    );
  }
}

export default Bookings;
