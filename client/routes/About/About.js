import React, { Component } from 'react';
import Helmet from 'react-helmet';
import PageTitle from 'client/components/PageTitle';
import Breadcrumbs from 'client/components/Breadcrumbs';

class About extends Component {
  render() {
    return (
      <div id="page-about" className="container">
        <Helmet title="About Us | Minicabit Admin" />
        <Breadcrumbs items={[
          { url: '/', title: 'Dashboard' },
          { title: 'About Us' }
        ]} />
        <PageTitle>About Us</PageTitle>
      </div>
    );
  }
}

export default About;
