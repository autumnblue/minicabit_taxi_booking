import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class Minicab extends Component {
  render() {
    return (
      <div id="page-minicab" className="container">
        <PageTitle>Minicab</PageTitle>
      </div>
    );
  }
}

export default Minicab;
