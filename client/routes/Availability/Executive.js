import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class Executive extends Component {
  render() {
    return (
      <div id="page-executive" className="container">
        <PageTitle>Executive</PageTitle>
      </div>
    );
  }
}

export default Executive;
