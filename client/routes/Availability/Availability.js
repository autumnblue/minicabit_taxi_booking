import React, { Component, PropTypes } from 'react';
import PageTitle from 'client/components/PageTitle';
import Tabs from 'client/components/Tabs';

class Availability extends Component {

  static propTypes = {
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/availability/minicab',
        title: 'Minicab'
      },
      {
        url: '/availability/executive',
        title: 'Executive'
      },
      {
        url: '/availability/premium',
        title: 'Premium executive'
      }
    ];
  }

  render() {
    return (
      <div id="page-availability" className="container">
        <PageTitle>Availability</PageTitle>
        <div className="row tabs-row">
          <div className="col-md-12">
            <Tabs items={this.tabItems} />
          </div>
        </div>
        <div className="row tab-content">
          <div className="col-md-12">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default Availability;
