import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class Premium extends Component {
  render() {
    return (
      <div id="page-premium" className="container">
        <PageTitle>Premium Executive</PageTitle>
      </div>
    );
  }
}

export default Premium;
