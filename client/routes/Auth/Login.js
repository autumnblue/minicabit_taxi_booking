import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { reduxForm } from 'redux-form';
import * as validation from '../../helpers/validation';
import { loginUser } from '../../actions';
import PageTitle from 'client/components/PageTitle';

// Styles at /styles/routes/Auth/Login.scss

export class Login extends Component {
  constructor(...args) {
    super(...args);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(form, dispatch) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, 1000);
    });
  }

  render() {
    const { fields: { email, password }, handleSubmit, submitting } = this.props;

    return (
      <div id="page-login" className="container">

        <Helmet title="Login | Minicabit Admin" />

        <form onSubmit={handleSubmit(this.onSubmit)}>
          <div className="row">
            <div className="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4 clearfix">
              <PageTitle>Log In</PageTitle>
              <div className="form-group">
                <label htmlFor="email">Email Address</label>
                <input type="text" className="form-control" id="email" placeholder="Email" {...email} />
                {email.touched && email.error && <div className="form_validation_error">{email.error}</div>}
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input type="password" className="form-control" id="password" placeholder="Password" {...password} />
                {password.touched && password.error && <div className="form_validation_error">{password.error}</div>}
              </div>
              <button type="submit" className="btn btn-primary pull-right" disabled={submitting}>
                {submitting ? <i /> : <noscript />} Login
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired
};

export default reduxForm({
  form: 'login',
  fields: ['email', 'password'],
  validate: validation.createValidator({
    email: [validation.required, validation.email],
    password: [validation.required, validation.minLength(6)]
  })
})(Login);
