import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { reduxForm } from 'redux-form';
import * as validation from '../../helpers/validation';
import { signupUser } from '../../actions';
import PageTitle from 'client/components/PageTitle';
import consts from '../../helpers/consts';
import { Link } from 'react-router';

// Styles at /styles/routes/Auth/Signup.scss

export class Signup extends Component {
  constructor(...args) {
    super(...args);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(form, dispatch) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, 1000);
    });
  }

  render() {
    const { fields: {
      officeEmail, confirmOfficeEmail,
      registeredName,
      dispatchSystem, dispatchSystemOther,
      tradingName,
      website,
      operatorLicenceNumber,
      licenceExpiryDate,
      localAuthority, localAuthorityOther,
      fleetLabel,
      fleetSize,
      password, confirmPassword,
      postcode,
      registeredAddressId, street, locality, town, county, countyOther,
      officeContactTel, mcoMobileNumber,
      contactPersonDay, contactPersonNight,
      howHeardAboutUs, howHeardAboutUsOther
    }, handleSubmit, submitting } = this.props;

    return (
      <div id="page-signup" className="container">

        <Helmet title="MCO Registration | Minicabit Admin" />
        <PageTitle>MiniCab Operator Registration</PageTitle>

        <form onSubmit={handleSubmit(this.onSubmit)} className="form-horizontal signup-form">
          <div className="row">
            <div className="col-lg-10 col-lg-offset-1 clearfix">

              <div className="form-group">
                <label htmlFor="officeEmail" className="col-sm-3 control-label required">
                  Office email address<em />
                </label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="officeEmail" {...officeEmail} />
                  {officeEmail.touched && officeEmail.error &&
                    <div className="form_validation_error">{officeEmail.error}</div>}
                </div>
                <div className="col-sm-4 form_field_comment">
                  (where you can easily receive our updates on bookings at anytime)
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="confirmOfficeEmail" className="col-sm-3 control-label required">
                  Confirm office email address<em />
                </label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="confirmOfficeEmail" {...confirmOfficeEmail} />
                  {confirmOfficeEmail.touched && confirmOfficeEmail.error &&
                    <div className="form_validation_error">{confirmOfficeEmail.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="registeredName" className="col-sm-3 control-label required">
                  Minicab company name<em />
                </label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="registeredName" {...registeredName} />
                  {registeredName.touched && registeredName.error &&
                    <div className="form_validation_error">{registeredName.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="dispatchSystem" className="col-sm-3 control-label required">
                  Dispatch system<em />
                </label>
                <div className="col-sm-5">
                  <select className="form-control" id="dispatchSystem"
                          {...dispatchSystem} value={dispatchSystem.value || ''}>
                    <option value="">-- Please select --</option>
                    {
                      consts.MCO_REGISTRATION.DISPATCH_SYSTEM_OPTIONS.map(
                        option => <option key={option}>{option}</option>
                      )
                    }
                  </select>
                  {dispatchSystem.touched && dispatchSystem.error &&
                    <div className="form_validation_error">{dispatchSystem.error}</div>}

                  {
                    dispatchSystem.value === 'Other' ?
                      <div>
                        <input type="text" className="form-control" id="dispatchSystemOther"
                               {...dispatchSystemOther} />
                        {dispatchSystemOther.touched && dispatchSystemOther.error &&
                          <div className="form_validation_error">{dispatchSystemOther.error}</div>}
                      </div>
                    :
                      <noscript />
                  }
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="tradingName" className="col-sm-3 control-label">
                  Legal company name
                </label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="tradingName" {...tradingName} />
                  {tradingName.touched && tradingName.error &&
                    <div className="form_validation_error">{tradingName.error}</div>}
                </div>
                <div className="col-sm-4 form_field_comment">
                  (if applicable)
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="website" className="col-sm-3 control-label required">Website<em /></label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="website" {...website} />
                  {website.touched && website.error && <div className="form_validation_error">{website.error}</div>}
                </div>
                <div className="col-sm-4 form_field_comment">
                  If you do not have a website, please enter 'NA'.
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="operatorLicenceNumber" className="col-sm-3 control-label required">
                  Private Hire Operator Licence Number<em />
                </label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="operatorLicenceNumber" {...operatorLicenceNumber} />
                  {operatorLicenceNumber.touched && operatorLicenceNumber.error &&
                    <div className="form_validation_error">{operatorLicenceNumber.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="licenceExpiryDate" className="col-sm-3 control-label required">
                  Licence expiries on<em />
                </label>
                <div className="col-sm-5">
                  <input type="date" className="form-control" id="licenceExpiryDate" {...licenceExpiryDate} />
                  {licenceExpiryDate.touched && licenceExpiryDate.error &&
                    <div className="form_validation_error">{licenceExpiryDate.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="localAuthority" className="col-sm-3 control-label required">
                  Licensing Local Authority<em />
                </label>
                <div className="col-sm-5">
                  <select className="form-control" id="localAuthority"
                          {...localAuthority} value={localAuthority.value || ''}>
                    <option value="">-- Please select --</option>
                    <option>Transport for London (Public Carriage Office)</option>
                    <option disabled>--------------------------</option>
                    {
                      consts.MCO_REGISTRATION.LOCAL_AUTHORITY_OPTIONS.map(
                        option => <option key={option}>{option}</option>
                      )
                    }
                  </select>
                  {localAuthority.touched && localAuthority.error &&
                    <div className="form_validation_error">{localAuthority.error}</div>}

                  {
                    localAuthority.value === 'Other' ?
                      <div>
                        <input type="text" className="form-control" id="localAuthorityOther"
                               {...localAuthorityOther} />
                        {localAuthorityOther.touched && localAuthorityOther.error &&
                          <div className="form_validation_error">{localAuthorityOther.error}</div>}
                      </div>
                      :
                      <noscript />
                  }
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="fleetLabel" className="col-sm-3 control-label required">Fleet Type<em /></label>
                <div className="col-sm-5">
                  <select className="form-control" id="fleetLabel" {...fleetLabel} value={fleetLabel.value || ''}>
                    <option value="">-- Please select --</option>
                    {
                      consts.MCO_REGISTRATION.FLEET_LABEL_OPTIONS.map(option => <option key={option}>{option}</option>)
                    }
                  </select>
                  {fleetLabel.touched && fleetLabel.error &&
                    <div className="form_validation_error">{fleetLabel.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="fleetSize" className="col-sm-3 control-label required">Fleet Size<em /></label>
                <div className="col-sm-5">
                  <select className="form-control" id="fleetSize" {...fleetSize} value={fleetSize.value || ''}>
                    <option value="">-- Please select --</option>
                    {
                      consts.MCO_REGISTRATION.FLEET_SIZE_OPTIONS.map(option => <option key={option}>{option}</option>)
                    }
                  </select>
                  {fleetSize.touched && fleetSize.error &&
                    <div className="form_validation_error">{fleetSize.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="password" className="col-sm-3 control-label required">Password<em /></label>
                <div className="col-sm-5">
                  <input type="password" className="form-control" id="password" {...password} />
                  {password.touched && password.error && <div className="form_validation_error">{password.error}</div>}
                </div>
                <div className="col-sm-4 form_field_comment">
                  (where you can easily receive our updates on bookings at anytime)
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="confirmPassword" className="col-sm-3 control-label required">
                  Confirm password<em />
                </label>
                <div className="col-sm-5">
                  <input type="password" className="form-control" id="confirmPassword" {...confirmPassword} />
                  {confirmPassword.touched && confirmPassword.error &&
                    <div className="form_validation_error">{confirmPassword.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="postcode" className="col-sm-3 control-label required">Postcode<em /></label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="postcode" {...postcode} />
                  {postcode.touched && postcode.error && <div className="form_validation_error">{postcode.error}</div>}
                </div>
                <div className="col-sm-4">
                  <button type="button" className="btn btn-info" id="findAddress">Find address</button>
                </div>
              </div>

              <input type="hidden" id="registeredAddressId" {...registeredAddressId} />

              <div className="form-group">
                <label htmlFor="street" className="col-sm-3 control-label">Address (line 1)</label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="street" {...street} />
                  {street.touched && street.error && <div className="form_validation_error">{street.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="locality" className="col-sm-3 control-label">Address (line 2)</label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="locality" {...locality} />
                  {locality.touched && locality.error && <div className="form_validation_error">{locality.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="town" className="col-sm-3 control-label required">City/Town<em /></label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="town" {...town} />
                  {town.touched && town.error && <div className="form_validation_error">{town.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="county" className="col-sm-3 control-label required">County<em /></label>
                <div className="col-sm-5">
                  <select className="form-control" id="county" {...county} value={county.value || ''}>
                    <option value="">-- Please select --</option>
                    {
                      consts.MCO_REGISTRATION.COUNTY_OPTIONS.map(option => <option key={option}>{option}</option>)
                    }
                  </select>
                  {county.touched && county.error && <div className="form_validation_error">{county.error}</div>}

                  {
                    county.value === 'Other' ?
                      <div>
                        <input type="text" className="form-control" id="countyOther" {...countyOther} />
                        {countyOther.touched && countyOther.error &&
                          <div className="form_validation_error">{countyOther.error}</div>}
                      </div>
                      :
                      <noscript />
                  }
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="officeContactTel" className="col-sm-3 control-label required">
                  Office contact number<em /><br />
                  <small>incl. STD code</small>
                </label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="officeContactTel" {...officeContactTel} />
                  {officeContactTel.touched && officeContactTel.error &&
                    <div className="form_validation_error">{officeContactTel.error}</div>}
                </div>
                <div className="col-sm-4 form_field_comment">
                  (for customers to contact you for additional trip requirements)
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="mcoMobileNumber" className="col-sm-3 control-label">Mobile number</label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="mcoMobileNumber" {...mcoMobileNumber} />
                  {mcoMobileNumber.touched && mcoMobileNumber.error &&
                    <div className="form_validation_error">{mcoMobileNumber.error}</div>}
                </div>
                <div className="col-sm-4 form_field_comment">
                  (for emergencies)
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="contactPersonDay" className="col-sm-3 control-label required">
                  Authorized contact person<em />
                </label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="contactPersonDay" {...contactPersonDay} />
                  {contactPersonDay.touched && contactPersonDay.error &&
                    <div className="form_validation_error">{contactPersonDay.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="contactPersonNight" className="col-sm-3 control-label">
                  Authorized contact person<br />
                  <small>(night time, if different)</small>
                </label>
                <div className="col-sm-5">
                  <input type="text" className="form-control" id="contactPersonNight" {...contactPersonNight} />
                  {contactPersonNight.touched && contactPersonNight.error &&
                    <div className="form_validation_error">{contactPersonNight.error}</div>}
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="howHeardAboutUs" className="col-sm-3 control-label">How did you hear about us?</label>
                <div className="col-sm-5">
                  <select className="form-control" id="howHeardAboutUs" {...howHeardAboutUs}
                          value={howHeardAboutUs.value || ''}>
                    <option value="">-- Please select --</option>
                    {
                      consts.MCO_REGISTRATION.HOW_HEARD_ABOUT_US_OPTIONS.map(
                        option => <option key={option}>{option}</option>
                      )
                    }
                  </select>
                  {howHeardAboutUs.touched && howHeardAboutUs.error &&
                    <div className="form_validation_error">{howHeardAboutUs.error}</div>}

                  {
                    howHeardAboutUs.value === 'Other' ?
                      <div>
                        <input type="text" className="form-control" id="howHeardAboutUsOther" {...howHeardAboutUsOther} />
                        {howHeardAboutUsOther.touched && howHeardAboutUsOther.error &&
                        <div className="form_validation_error">{howHeardAboutUsOther.error}</div>}
                      </div>
                      :
                      <noscript />
                  }
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-offset-3 col-sm-8 terms-conditions">
                  On clicking Register, you are acknowledging that your acceptance of
                  our <Link to="/terms">Terms & Conditions</Link> will become effective on the date of accepting
                  your first Booking with minicabit or when minicabit receives your signed
                  Account form (to be posted to you), whichever is sooner.
                  These <Link to="/terms">Terms & Conditions</Link> can be viewed online at your convenience.
                  Without minicabit receiving your signed Account form and a copy of your valid
                  Private Hire Operator Licence, you will not be able to receive payments for Bookings
                  where the Customer has paid in advance by Card.
                </div>
              </div>

              <div className="form-group">
                <div className="col-sm-offset-3 col-sm-5 submit">
                  <button type="submit" className="btn btn-primary" disabled={submitting}>
                    {submitting ? <i /> : <noscript />} Register
                  </button>
                </div>
              </div>

            </div>
          </div>
        </form>
      </div>
    );
  }
}

Signup.propTypes = {
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired
};

export default reduxForm({
  form: 'signup',
  fields: [
    'officeEmail', 'confirmOfficeEmail', 'registeredName', 'dispatchSystem', 'dispatchSystemOther',
    'tradingName', 'website', 'operatorLicenceNumber', 'licenceExpiryDate', 'localAuthority', 'localAuthorityOther',
    'fleetLabel', 'fleetSize', 'password', 'confirmPassword', 'postcode',
    'registeredAddressId', 'street', 'locality', 'town', 'county', 'countyOther',
    'officeContactTel', 'mcoMobileNumber', 'contactPersonDay', 'contactPersonNight',
    'howHeardAboutUs', 'howHeardAboutUsOther'
  ],
  validate: validation.createValidator({
    officeEmail: [validation.required, validation.email],
    confirmOfficeEmail: [validation.required, validation.email, validation.match('officeEmail')],
    registeredName: [validation.required],
    dispatchSystem: [validation.required],
    dispatchSystemOther: [validation.required],
    website: [validation.required],
    operatorLicenceNumber: [validation.required],
    licenceExpiryDate: [validation.required],
    localAuthority: [validation.required],
    localAuthorityOther: [validation.required],
    fleetLabel: [validation.required],
    fleetSize: [validation.required],
    password: [validation.required, validation.minLength(8), validation.atLeastOneLetterOneNumber],
    confirmPassword: [validation.required, validation.match('password')],
    postcode: [validation.required],
    registeredAddressId: [validation.required],
    town: [validation.required],
    county: [validation.required],
    countyOther: [validation.required],
    officeContactTel: [validation.required, validation.phone, validation.minLength(11)],
    mcoMobileNumber: [validation.phone],
    contactPersonDay: [validation.required]
  })
})(Signup);
