import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

import Sidebar from './../../components/Sidebar/Sidebar';

class Dashboard extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    children: PropTypes.node
  };

  render() {
    return (
      <div id="page-dashboard" className="container">
        <Helmet title="Dashboard | Minicabit Admin" />
        <Link to={'/addDriver'} className="btn btn-default">
          Add driver
        </Link>
        <div className="row">
          <div className="col-md-2 col-xs-12">
            <Sidebar />
          </div>
          <div className="col-md-10 col-xs-12">
            {this.props.children || 'Hello!'}
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(Dashboard);
