import React, { Component } from 'react';
import Helmet from 'react-helmet';
import PageTitle from 'client/components/PageTitle';

class Privacy extends Component {
  render() {
    return (
      <div id="page-terms" className="container">
        <Helmet title="Terms & Conditions | Minicabit Admin" />
        <PageTitle>Terms & Conditions</PageTitle>
      </div>
    );
  }
}

export default Privacy;
