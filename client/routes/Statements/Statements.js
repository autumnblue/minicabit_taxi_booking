import React, { Component, PropTypes } from 'react';
import PageTitle from 'client/components/PageTitle';
import Tabs from 'client/components/Tabs';

class Statements extends Component {

  static propTypes = {
    location: PropTypes.object.isRequired,
    children: PropTypes.node,
  };
  
  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/statements/pending',
        title: 'Pending/Disputed',
      },
      {
        url: '/statements/outstanding',
        title: 'Outstanding',
      },
      {
        url: '/statements/history',
        title: 'History',
      },
    ];
  }


  render() {
    return (
      <div id="page-statements" className="container">
        <PageTitle>Statements</PageTitle>
        <div className="row tabs-row">
          <div className="col-md-12">
            <Tabs items={this.tabItems} />
          </div>
        </div>
        <div className="row tab-content">
          <div className="col-md-12">
            {this.props.children}
          </div>
        </div>
      </div>    
    );
  }
}

export default Statements;
