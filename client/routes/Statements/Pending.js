import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class Pending extends Component {
  render() {
    return (
      <div id="page-pending" className="container">
        <PageTitle>Pending</PageTitle>
      </div>
    );
  }
}

export default Pending;
