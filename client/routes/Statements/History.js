import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class StatementsHistory extends Component {
  render() {
    return (
      <div id="page-history" className="container">
        <PageTitle>History</PageTitle>
      </div>
    );
  }
}

export default StatementsHistory;
