import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class Outstanding extends Component {
  render() {
    return (
      <div id="page-outstanding" className="container">
        <PageTitle>Outstanding</PageTitle>
      </div>
    );
  }
}

export default Outstanding;
