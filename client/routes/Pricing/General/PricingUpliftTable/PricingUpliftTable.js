import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ComponentHeader from 'client/components/ComponentHeader';
import UpliftBlock from './UpliftBlock/UpliftBlock';
import { getUpliftPricing, postGeneralPricing } from 'client/actions';

class PricingUpliftTable extends Component {
  static propTypes = {
    getUpliftPricing: PropTypes.func.isRequired,
    postGeneralPricing: PropTypes.func.isRequired,
    readyForUpdate: PropTypes.object
  }
  constructor(...args) {
    super(...args);
    this.updateTimeout = null;
    this.componentDescription = () => {
      return (
        <div>
          <p>
            {'The pricing for your larger sized vehicles is calculated on your saloon pricing. '}
            {'It effects your mileage, location and postcode area pricing types, '}
            {'therefore saving you from having to manually enter individual prices for each car size. '}
            {'This uplift can be based on either:'}
          </p>
          <ol style={{ fontWeight: 'bold' }}>
            <li>{'A (£) flat fee more than your saloon car pricing'}</li>
            <li>{'A (%) uplift over your saloon prices'}</li>
          </ol>
        </div>
      );
    };
    this.triggerPricingUpdate = this.triggerPricingUpdate.bind(this);
  }

  componentWillMount() {
    this.props.getUpliftPricing(3324);
  }

  triggerPricingUpdate() {
    clearTimeout(this.updateTimeout);
    this.updateTimeout = setTimeout(() => {
      this.props.postGeneralPricing(3324, this.props.readyForUpdate);
    }, 2000);
  }

  render() {
    return (
      <div className="pricing-general">
        <div className="row">
          <div className="col-xs-12">
            <ComponentHeader title="Pricing uplift table" description={this.componentDescription()} />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12 text-center">
            <UpliftBlock canEdit title="Estate" numPass={4} triggerPricingUpdate={this.triggerPricingUpdate} />
            <UpliftBlock canEdit title="5-6 Passengers" numPass={6} triggerPricingUpdate={this.triggerPricingUpdate} />
            <UpliftBlock canEdit title="7 Passengers" numPass={7} triggerPricingUpdate={this.triggerPricingUpdate} />
            <UpliftBlock canEdit title="8 Passengers" numPass={8} triggerPricingUpdate={this.triggerPricingUpdate} />
            <UpliftBlock canEdit title="9 Passengers" numPass={9} triggerPricingUpdate={this.triggerPricingUpdate} />
            <UpliftBlock canEdit title="14 Passengers" numPass={14} triggerPricingUpdate={this.triggerPricingUpdate} />
            <UpliftBlock canEdit title="16 Passengers" numPass={16} triggerPricingUpdate={this.triggerPricingUpdate} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { upliftPricing, readyForUpdate } = state.general;
  return { upliftPricing, readyForUpdate };
};

const mapDispatchToProps = {
  getUpliftPricing,
  postGeneralPricing
};

export default connect(mapStateToProps, mapDispatchToProps)(PricingUpliftTable);
