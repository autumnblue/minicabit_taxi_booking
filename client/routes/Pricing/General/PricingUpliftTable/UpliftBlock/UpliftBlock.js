import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { addGeneralPricingToUpdate } from 'client/actions';
import _ from 'lodash';

class UpliftBlock extends Component {
  static propTypes = {
    canEdit: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    numPass: PropTypes.number.isRequired,
    flatRate: PropTypes.number,
    upliftRate: PropTypes.number,
    pricingType: PropTypes.number,
    updating: PropTypes.bool,
    addGeneralPricingToUpdate: PropTypes.func,
    triggerPricingUpdate: PropTypes.func.isRequired
  };

  constructor(...args) {
    super(...args);
    this.flatRadioId = _.kebabCase(this.props.title) + '-flat';
    this.upliftRadioId = _.kebabCase(this.props.title) + '-uplift';
    this.updateTimeout = null;

    this.onCheckChanged = this.onCheckChanged.bind(this);
    this.onInputChanged = this.onInputChanged.bind(this);
  }

  onCheckChanged(e) {
    const { numPass, upliftRate, flatRate } = this.props;
    const { value } = e.target;
    if (value === '1' || value === 1) {
      this.props.addGeneralPricingToUpdate(numPass, flatRate, 1);
    } else if (value === '2' || value === 2) {
      this.props.addGeneralPricingToUpdate(numPass, upliftRate, 2);
    } else {
      return;
    }
    this.props.triggerPricingUpdate();
  }

  onInputChanged(e) {
    const { value, name } = e.target;
    const { numPass, pricingType } = this.props;
    if (name === 'flatRate') {
      if (!isNaN(parseFloat(value))) {
        this.props.addGeneralPricingToUpdate(numPass, parseFloat(value), pricingType);
      } else {
        this.props.addGeneralPricingToUpdate(numPass, value, pricingType);
      }
    } else if (name === 'upliftRate') {
      if (!isNaN(parseInt(value, 10))) {
        this.props.addGeneralPricingToUpdate(numPass, parseFloat(value), pricingType);
      } else {
        this.props.addGeneralPricingToUpdate(numPass, value, pricingType);
      }
    } else {
      return;
    }
    this.props.triggerPricingUpdate();
  }

  render() {
    const { flatRate, upliftRate, pricingType, updating, canEdit } = this.props;
    return (
      <div className="uplift-block">
        <div className="header text-left">
          {this.props.title}
        </div>
        <form>
          <div className="input-row">
            <div className="radio">
              <input name="pricingType" type="radio" id={this.flatRadioId} value="1"
                checked={pricingType === 1} disabled={!canEdit || updating} onChange={this.onCheckChanged} />
              <label htmlFor={this.flatRadioId}>£</label>
            </div>
            <div className="input">
              <input type="text" name="flatRate" ref="flatRate" disabled={!canEdit || updating} 
                value={flatRate || 0} onChange={this.onInputChanged} />
            </div>
          </div>
          <div className="input-row">
            <div className="radio">
              <input name="pricingType" type="radio" id={this.upliftRadioId} value="2"
                checked={pricingType === 2} disabled={!canEdit || updating} onChange={this.onCheckChanged} />
              <label htmlFor={this.upliftRadioId}>%</label>
            </div>
            <div className="input">
              <input type="text" name="upliftRate" ref="upliftRate" disabled={!canEdit || updating} 
                value={upliftRate || 0} onChange={this.onInputChanged} />
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let [pricingValue, pricingType, updating] = [1, 1, false];
  if (state.general.upliftPricing) {
    updating = state.general.updating;
    if (ownProps.numPass === 4) {
      pricingValue = state.general.upliftPricing.estateLuggageMultiplayer;
      pricingType = state.general.upliftPricing.estateLuggageMultiplayerType;
    } else {
      pricingValue = state.general.upliftPricing['multiplayer' + ownProps.numPass];
      pricingType = state.general.upliftPricing['multiplayerType' + ownProps.numPass];
    }
  }
  return { flatRate: pricingValue, upliftRate: pricingValue, pricingType, updating };
};

const mapDispatchToProps = { addGeneralPricingToUpdate };

export default connect(mapStateToProps, mapDispatchToProps)(UpliftBlock);
