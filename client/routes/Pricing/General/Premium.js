import React from 'react';
import PricingUpliftTable from './PricingUpliftTable';

export default () => {
  return (
    <PricingUpliftTable pricingType="premium" />
  );
};
