import React, { Component, PropTypes } from 'react';
import PageTitle from 'client/components/PageTitle';
import Tabs from 'client/components/Tabs';

class Pricing extends Component {

  static propTypes = {
    location: PropTypes.object.isRequired,
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/pricing/general',
        title: 'General'
      },
      {
        url: '/pricing/mileage',
        title: 'By mileage'
      },
      {
        url: '/pricing/location',
        title: 'By location'
      },
      {
        url: '/pricing/postcode',
        title: 'By postcode area'
      },
      {
        url: '/pricing/flash',
        title: 'Flash sale'
      }
    ];
  }

  render() {
    return (
      <div id="page-pricing">
        <PageTitle>Pricing</PageTitle>
        <div className="row tabs-row">
          <div className="col-md-12">
            <Tabs items={this.tabItems} />
          </div>
        </div>
        <div className="row tab-content">
          <row className="col-md-12">
            {this.props.children}
          </row>
        </div>
      </div>
    );
  }
}

export default Pricing;
