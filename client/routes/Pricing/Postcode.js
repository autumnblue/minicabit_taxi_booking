import React, { Component, PropTypes } from 'react';
import SubTabs from 'client/components/SubTabs';

class Postcode extends Component {

  static propTypes = {
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/pricing/postcode/minicab',
        title: 'Minicab'
      },
      {
        url: '/pricing/postcode/executive',
        title: 'Executive'
      },
      {
        url: '/pricing/postcode/premium',
        title: 'Premium executive'
      }
    ];
  }

  render() {
    return (
      <div id="page-pricing-postcode" className="container">
        <div className="row">
          <SubTabs items={this.tabItems} />
        </div>
        <div className="row">
          <div className="col-md-12">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default Postcode;
