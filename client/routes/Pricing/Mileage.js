import React, { Component, PropTypes } from 'react';
import SubTabs from 'client/components/SubTabs';

class Mileage extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/pricing/mileage/minicab',
        title: 'Minicab'
      },
      {
        url: '/pricing/mileage/executive',
        title: 'Executive'
      },
      {
        url: '/pricing/mileage/premium',
        title: 'Premium executive'
      }
    ];
  }

  render() {
    return (
      <div id="page-pricing-mileage" className="container">
        <div className="row">
          <SubTabs items={this.tabItems} />
        </div>
        <div className="row">
          <div className="col-md-12">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default Mileage;
