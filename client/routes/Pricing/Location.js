import React, { Component, PropTypes } from 'react';
import SubTabs from 'client/components/SubTabs';

class Location extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/pricing/location/minicab',
        title: 'Minicab'
      },
      {
        url: '/pricing/location/executive',
        title: 'Executive'
      },
      {
        url: '/pricing/location/premium',
        title: 'Premium executive'
      }
    ];
  }


  render() {
    return (
      <div id="page-pricing-location" className="container">
        <div className="row">
          <SubTabs items={this.tabItems} />
        </div>
        <div className="row">
          <div className="col-md-12">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default Location;
