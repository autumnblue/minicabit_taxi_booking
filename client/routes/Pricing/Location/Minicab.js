import React from 'react';
import PricingMinicab from 'client/components/PricingMinicab';

export default () => {
  return (
    <PricingMinicab pricingType="location" />
  );
};
