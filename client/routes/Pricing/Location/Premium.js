import React from 'react';
import PricingPremium from 'client/components/PricingPremium';

export default () => {
  return (
    <PricingPremium pricingType="location" />
  );
};
