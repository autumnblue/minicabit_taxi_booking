import React from 'react';
import PricingExecutive from 'client/components/PricingExecutive';

export default () => {
  return (
    <PricingExecutive pricingType="postcode" />
  );
};
