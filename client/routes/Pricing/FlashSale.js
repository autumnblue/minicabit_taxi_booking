import React, { Component, PropTypes } from 'react';
import SubTabs from 'client/components/SubTabs';

class FlashSale extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/pricing/flash/minicab',
        title: 'Minicab'
      },
      {
        url: '/pricing/flash/executive',
        title: 'Executive'
      },
      {
        url: '/pricing/flash/premium',
        title: 'Premium executive'
      }
    ];
  }


  render() {
    return (
      <div id="page-pricing-flash" className="container">
        <div className="row">
          <SubTabs items={this.tabItems} />
        </div>
        <div className="row">
          <div className="col-md-12">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default FlashSale;
