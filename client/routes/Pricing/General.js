import React, { Component, PropTypes } from 'react';
import SubTabs from 'client/components/SubTabs';

class General extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  constructor(...args) {
    super(...args);
    this.tabItems = [
      {
        url: '/pricing/general/minicab',
        title: 'Minicab'
      },
      {
        url: '/pricing/general/executive',
        title: 'Executive'
      },
      {
        url: '/pricing/general/premium',
        title: 'Premium executive'
      }
    ];
  }


  render() {
    return (
      <div id="page-pricing-general">
        <div className="row">
          <SubTabs items={this.tabItems} />
        </div>
        <div className="row">
          <div className="col-md-12">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export default General;
