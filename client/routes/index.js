import React, { Component, PropTypes } from 'react';
import { Router, Route, IndexRedirect } from 'react-router';

import App                            from '../components/App';
import Dashboard                      from './Dashboard/Dashboard';
import About                          from './About/About';
import Privacy                        from './Privacy/Privacy';
import Terms                          from './Terms/Terms';
import Login                          from './Auth/Login';
import Signup                         from './Auth/Signup';

import Bookings                       from './Bookings/Bookings';
import BookingAlerts                  from './Bookings/Alerts';
import BookingUpcoming                from './Bookings/Upcoming';
import BookingAll                     from './Bookings/AllBookings';

import Market                         from './Market/Market';

import Availability                   from './Availability/Availability';
import Minicab                        from './Availability/Minicab';
import Executive                      from './Availability/Executive';
import Premium                        from './Availability/Premium';

import Pricing                        from './Pricing/Pricing';
import GeneralPricing                 from './Pricing/General';
import GeneralMinicab                 from './Pricing/General/Minicab';
import GeneralExecutive               from './Pricing/General/Executive';
import GeneralPremium                 from './Pricing/General/Premium';

import MileagePricing                 from './Pricing/Mileage';
import MileageMinicab                 from './Pricing/Mileage/Minicab';
import MileageExecutive               from './Pricing/Mileage/Executive';
import MileagePremium                 from './Pricing/Mileage/Premium';

import LocationPricing                from './Pricing/Location';
import LocationMinicab                from './Pricing/Location/Minicab';
import LocationExecutive              from './Pricing/Location/Executive';
import LocationPremium                from './Pricing/Location/Premium';

import PostcodePricing                from './Pricing/Postcode';
import PostcodeMinicab                from './Pricing/Postcode/Minicab';
import PostcodeExecutive              from './Pricing/Postcode/Executive';
import PostcodePremium                from './Pricing/Postcode/Premium';

import FlashSalePricing               from './Pricing/FlashSale';
import FlashMinicab                   from './Pricing/FlashSale/Minicab';
import FlashExecutive                 from './Pricing/FlashSale/Executive';
import FlashPremium                   from './Pricing/FlashSale/Premium';

import Statements                     from './Statements/Statements';
import StatementsPending              from './Statements/Pending';
import StatementsOutstanding          from './Statements/Outstanding';
import StatementsHistory              from './Statements/History';

import AccountSettings                from './AccountSettings/AccountSettings';
import CompanyDetails                 from './AccountSettings/CompanyDetails';
import AccountDetails                 from './AccountSettings/AccountDetails';

import Help                           from './Help/Help';

import AddDriver                      from './AddDriver/AddDriver';

export default class Routes extends Component {
  render() {
    return (
      <Router history={this.props.history}>
        <Route        path="/"                    component={App}>

          <Route                                  component={Dashboard}>

            <IndexRedirect                        to="bookings" />

            <Route    path="addDriver"            component={AddDriver} />
            <Route    path="bookings"             component={Bookings}>
              <IndexRedirect                      to="alerts" />
              <Route  path="alerts"               component={BookingAlerts} />
              <Route  path="upcoming"             component={BookingUpcoming} />
              <Route  path="all"                  component={BookingAll} />
            </Route>
            <Route    path="market"               component={Market} />
            <Route    path="availability"         component={Availability}>
              <IndexRedirect                      to="minicab" />
              <Route  path="minicab"              component={Minicab} />
              <Route  path="executive"            component={Executive} />
              <Route  path="premium"              component={Premium} />
            </Route>
            <Route    path="pricing"              component={Pricing} >
              <IndexRedirect                      to="general" />
              <Route  path="general"              component={GeneralPricing}>
                <IndexRedirect                    to="minicab" />
                <Route path="minicab"             component={GeneralMinicab} />
                <Route path="executive"           component={GeneralExecutive} />
                <Route path="premium"             component={GeneralPremium} />
              </Route>
              <Route  path="mileage"              component={MileagePricing}>
                <IndexRedirect                    to="minicab" />
                <Route path="minicab"             component={MileageMinicab} />
                <Route path="executive"           component={MileageExecutive} />
                <Route path="premium"             component={MileagePremium} />
              </Route>
              <Route  path="location"             component={LocationPricing}>
                <IndexRedirect                    to="minicab" />
                <Route path="minicab"             component={LocationMinicab} />
                <Route path="executive"           component={LocationExecutive} />
                <Route path="premium"             component={LocationPremium} />
              </Route>
              <Route  path="postcode"             component={PostcodePricing}>
                <IndexRedirect                    to="minicab" />
                <Route path="minicab"             component={PostcodeMinicab} />
                <Route path="executive"           component={PostcodeExecutive} />
                <Route path="premium"             component={PostcodePremium} />
              </Route>
              <Route  path="flash"                component={FlashSalePricing}>
                <IndexRedirect                    to="minicab" />
                <Route path="minicab"             component={FlashMinicab} />
                <Route path="executive"           component={FlashExecutive} />
                <Route path="premium"             component={FlashPremium} />
              </Route>
            </Route>
            <Route    path="statements"           component={Statements}>
              <IndexRedirect                      to="pending" />
              <Route  path="pending"              component={StatementsPending} />
              <Route  path="outstanding"          component={StatementsOutstanding} />
              <Route  path="history"              component={StatementsHistory} />
            </Route>
            <Route    path="settings"             component={AccountSettings}>
              <IndexRedirect                      to="company" />
              <Route  path="company"              component={CompanyDetails} />
              <Route  path="account"              component={AccountDetails} />
            </Route>
            <Route    path="help"                 component={Help} />
          </Route>

          <Route      path="about"                component={About} />
          <Route      path="privacy"              component={Privacy} />
          <Route      path="terms"                component={Terms} />
          <Route      path="login"                component={Login} />
          <Route      path="signup"               component={Signup} />

        </Route>
      </Router>
    );
  }
}

Routes.propTypes = {
  history: PropTypes.object.isRequired
};
