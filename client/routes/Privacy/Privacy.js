import React, { Component } from 'react';
import Helmet from 'react-helmet';
import PageTitle from 'client/components/PageTitle';
import Breadcrumbs from 'client/components/Breadcrumbs';

class Privacy extends Component {
  render() {
    return (
      <div id="page-privacy" className="container">
        <Helmet title="Privacy Policy | Minicabit Admin" />
        <Breadcrumbs items={[
          { url: '/', title: 'Dashboard' },
          { title: 'Privacy Policy' }
        ]} />
        <PageTitle>Our Privacy Policy</PageTitle>
      </div>
    );
  }
}

export default Privacy;
