import React, { Component } from 'react';
import PageTitle from 'client/components/PageTitle';

class AddDriver extends Component {
  render() {
    return (
      <div id="page-add-driver" className="container">
        <PageTitle>Add Driver</PageTitle>
      </div>    
    );
  }
}

export default AddDriver;
