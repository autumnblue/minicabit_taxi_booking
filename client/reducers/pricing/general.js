import { combineReducers } from 'redux';
import * as ActionTypes from '../../actions';

export const upliftPricing = (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.API_GET_UPLIFT_PRICING_SUCCESS:
    case ActionTypes.API_POST_GENERAL_PRICING_SUCCESS:
      return action.response.data;
    case ActionTypes.UPDATE_MILTIPLIER_TYPE: {
      const oldState = { ...state };
      const { index, multiply, multiplyType } = action;
      if (index === 4) {
        oldState.estateLuggageMultiplayer = multiply;
        oldState.estateLuggageMultiplayerType = multiplyType;
      } else {
        oldState['multiplayer' + index] = multiply;
        oldState['multiplayerType' + index] = multiplyType;
      }
      return oldState;
    }
    default:
      return state;
  }
};

export const readyForUpdate = (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.UPDATE_MILTIPLIER_TYPE: {
      const oldState = { ...state };
      const { multiply, multiplyType, index } = action;
      const indexToKeyMap = { 4: '1-4Estate', 6: '5-6', 7: '7', 8: '8', 9: '9', 14: '10-14', 16: '15-16' };
      if (!indexToKeyMap[index]) return state;
      oldState[indexToKeyMap[index]] = {
        multiply,
        multiplyType
      };
      return oldState;
    }
    case ActionTypes.API_GET_UPLIFT_PRICING_SUCCESS:
    case ActionTypes.API_POST_GENERAL_PRICING_SUCCESS:
      return {};
    default:
      return state;
  }
};

export const updating = (state = false, action) => {
  switch (action.type) {
    case ActionTypes.API_GET_UPLIFT_PRICING_SUCCESS:
    case ActionTypes.API_POST_GENERAL_PRICING_SUCCESS:
    case ActionTypes.API_GET_UPLIFT_PRICING_FAILURE:
    case ActionTypes.API_POST_GENERAL_PRICING_FAILURE:
      return false;
    case ActionTypes.API_GET_UPLIFT_PRICING_REQUEST:
    case ActionTypes.API_POST_GENERAL_PRICING_REQUEST:
      return true;
    default:
      return state;
  }
};

export default {
  general: combineReducers({
    upliftPricing,
    readyForUpdate,
    updating
  })
};
