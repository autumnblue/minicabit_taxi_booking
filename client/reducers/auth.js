import { combineReducers } from 'redux';
import * as ActionTypes from '../actions';
import { createApiReducer } from '../helpers/utils';

/**
 * Reducer to store logged in user object
 */
export const user = (state = null, action) => {
  switch (action.type) {
    case ActionTypes.LOGIN_SUCCESS:
      return action.response;
    case ActionTypes.LOGOUT_SUCCESS:
      return null;
    default:
      return state;
  }
};

/**
 * Reducer to store Signup API status, created using generator
 */
const signup = createApiReducer([
  ActionTypes.SIGNUP_REQUEST,
  ActionTypes.SIGNUP_SUCCESS,
  ActionTypes.SIGNUP_FAILURE
]);

/**
 * Reducer to store Login API status, created using generator
 */
const login = createApiReducer([
  ActionTypes.LOGIN_REQUEST,
  ActionTypes.LOGIN_SUCCESS,
  ActionTypes.LOGIN_FAILURE
]);

/**
 * Reducer to store Logout API status, created using generator
 */
const logout = createApiReducer([
  ActionTypes.LOGOUT_REQUEST,
  ActionTypes.LOGOUT_SUCCESS,
  ActionTypes.LOGOUT_FAILURE
]);


/**
 * Reducer for all authentication activities, comprised of child reducers such as user, signup, login, logout, etc
 */
export default {
  auth: combineReducers({
    user,
    signup,
    login,
    logout
  })
};
