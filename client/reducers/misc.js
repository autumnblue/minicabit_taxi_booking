import * as ActionTypes from '../actions';

/**
 * Reducer for modal-style Message Boxes
 * style can be one of 'success', 'warning', 'error'; default is 'warning'
 */
export default {
  messageBox: (state = [], action) => {
    switch (action.type) {
      case ActionTypes.OPEN_MESSAGEBOX:
        return [
          ...state,
          {
            message: action.message,
            style: action.style,
            title: action.title
          }
        ];
      case ActionTypes.CLOSE_MESSAGEBOX:
        return state.slice(1);
      default:
        return state;
    }
  }
};
