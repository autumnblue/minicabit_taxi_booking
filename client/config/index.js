const defaultConfig = require('./config.default');
let envConfig;

switch (process.env.NODE_ENV) {
  case 'production':
    envConfig = require('./config.prod'); break;
  default:
    envConfig = require('./config.dev');
}

module.exports = Object.assign({}, defaultConfig, envConfig);
