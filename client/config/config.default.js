/**
 * configuration regardless of environment
 */
const API_HOST = (typeof location !== 'undefined' && '//' + location.host) || 'http://localhost:3000';
module.exports = {
  API_HOST,
  API_URL: API_HOST + '/api/v1',
};
