import { callApi } from '../helpers/utils';
import { routerActions } from 'react-router-redux';

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = Symbol('CALL API');

// A Redux middleware that interprets actions with CALL_API info specified.
// Performs the call and promises when such actions are dispatched.
export default store => next => action => {
  const apiParams = action[CALL_API];
  if (typeof apiParams === 'undefined') {
    return next(action);
  }

  let { endpoint } = apiParams;
  const { types, method, data, successRedirect, failureRedirect } = apiParams;

  if (typeof endpoint === 'function') {
    endpoint = endpoint(store.getState());
  }

  if (typeof endpoint !== 'string') {
    throw new Error('API middleware: Specify a string endpoint URL.');
  }
  if (!Array.isArray(types) || types.length !== 3) {
    throw new Error('API middleware: Expected an array of three action types.');
  }
  if (!types.every(type => typeof type === 'string')) {
    throw new Error('API middleware: Expected action types to be strings.');
  }

  function actionWith(nextActionData) {
    const finalAction = Object.assign({}, action, nextActionData);
    delete finalAction[CALL_API];
    return finalAction;
  }

  const [requestType, successType, failureType] = types;
  next(actionWith({ type: requestType }));

  return callApi(endpoint, method, data).then(
    response => {
      next(actionWith({
        type: successType,
        response
      }));
      successRedirect && store.dispatch(routerActions.push(successRedirect));
      return response;
    },
    error => {
      failureRedirect && store.dispatch(routerActions.push(failureRedirect));
      // Redirect before FAILURE action dispatch to allow error notification
      next(actionWith({
        type: failureType,
        error: error.error
        // error: error.message || 'Something went wrong'
      }));
      return Promise.reject(error);
    }
  );
};
