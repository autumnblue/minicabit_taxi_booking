import React, { Component, PropTypes } from 'react';

class ComponentHeader extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.node.isRequired
  };

  render() {
    return (
      <div className="component-header">
        <span className="title">{this.props.title}</span>
        {this.props.description}
      </div>
    );
  }
}

export default ComponentHeader;
