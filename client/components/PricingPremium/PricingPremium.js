import React, { Component, PropTypes } from 'react';

class PricingPremium extends Component {

  static propTypes = {
    pricingType: PropTypes.string.isRequired,
  }

  render() {
    return (
      <h1>{this.props.pricingType} > Premium executive</h1> 
    );
  }
}

export default PricingPremium;
