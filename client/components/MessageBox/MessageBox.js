import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { closeMessageBox } from '../../actions';
import { Modal, Button, Glyphicon } from 'react-bootstrap';
import { capitalize } from 'lodash';

// Styles at /styles/components/MessageBox.scss

export function MessageBox({ messageBox, closeMessageBox }) {
  let message = '', title = '', iconStyle = '';
  if (messageBox.length) {
    const curMsg = messageBox[0];
    message = curMsg.message;
    title = curMsg.title || (curMsg.style ? capitalize(curMsg.style) : 'MinicabitAdmin says');
    iconStyle = curMsg.style === 'success' ? 'ok' : (curMsg.style === 'error' ? 'remove' : 'alert');
  }
  return (
    <Modal className="message-box" show={Boolean(messageBox.length)} backdrop="static" onHide={closeMessageBox}>
      <Modal.Header closeButton>
        <Modal.Title>
          <Glyphicon glyph={iconStyle} className={`icon-${iconStyle}`} />
          <span>{title}</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>{message}</div>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="primary" onClick={closeMessageBox}> OK </Button>
      </Modal.Footer>
    </Modal>
  );
}

MessageBox.propTypes = {
  messageBox: PropTypes.array.isRequired,
  closeMessageBox: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  const { messageBox } = state;
  return { messageBox };
};

export default connect(mapStateToProps, { closeMessageBox })(MessageBox);
