import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class Sidebar extends Component {

  static contextTypes = {
    location: PropTypes.object.isRequired
  };

  pageActive(path) {
    return this.context.location.pathname.indexOf(path) === 0;
  }

  render() {
    return (
      <div className="nav-sidebar">
        <ul className="nav nav-pills nav-stacked">
          <li className={this.pageActive('/bookings') ? 'active' : ''}>
            <Link to={'/bookings'}>Bookings</Link>
          </li>
          <li className={this.pageActive('/market') ? 'active' : ''}>
            <Link to={'/market'}>Market</Link>
          </li>
          <li className={this.pageActive('/availability') ? 'active' : ''}>
            <Link to={'/availability'}>Availability</Link>
          </li>
          <li className={this.pageActive('/pricing') ? 'active' : ''}>
            <Link to={'/pricing'}>Pricing</Link>
          </li>
          <li className={this.pageActive('/statements') ? 'active' : ''}>
            <Link to={'/statements'}>Statements</Link>
          </li>
          <li className={this.pageActive('/settings') ? 'active' : ''}>
            <Link to={'/settings'}>Account Settings</Link>
          </li>
          <li className={this.pageActive('/help') ? 'active' : ''}>
            <Link to={'/help'}>Help</Link>
          </li>
          <li>
            <Link to={'/logout'}>Logout</Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default Sidebar;
