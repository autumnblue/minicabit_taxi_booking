import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class Tabs extends Component {
  static contextTypes = {
    location: PropTypes.object.isRequired
  };

  static propTypes = {
    items: PropTypes.array.isRequired
  };

  tabActive(path) {
    return this.context.location.pathname.indexOf(path) === 0;
  }

  render() {
    return (
      <ul className="nav nav-tabs">
        {
          this.props.items.map((tab, i) => {
            return (
              <li key={i} className={this.tabActive(tab.url) ? 'active' : ''}>
                <Link to={tab.url}>{tab.title}</Link>
              </li>
            );
          })
        }
      </ul>
    );
  }
}

export default Tabs;
