import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions';
import consts from '../../helpers/consts';

class Logout extends Component {

  constructor(...args) {
    super(...args);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();

    if (this.props.apiStatus !== consts.API_LOADING) {
      this.props.dispatch(logoutUser());
    }
  }

  render() {
    return (
      <a href="#" onClick={this.handleClick}>Log out</a>
    );
  }
}

Logout.propTypes = {
  apiStatus: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  const { status: apiStatus } = state.auth.logout;
  return { apiStatus };
};

export default connect(mapStateToProps)(Logout);
