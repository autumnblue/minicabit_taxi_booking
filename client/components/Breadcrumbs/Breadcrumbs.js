import React, { PropTypes } from 'react';
import { Link } from 'react-router';

export default function Breadcrumbs({ items }) {
  const bcItems = items.map((item) => (
    <li className={item.url && 'active'} key={item.title}>
    {
      item.url ?
        <Link to={item.url}>{item.title}</Link>
      :
        item.title
    }
    </li>
  ));
  return (
    <ol className="breadcrumb">
      {bcItems}
    </ol>
  );
}

Breadcrumbs.propTypes = {
  items: PropTypes.array.isRequired
};
