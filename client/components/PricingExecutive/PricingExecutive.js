import React, { Component, PropTypes } from 'react';

class PricingExecutive extends Component {

  static propTypes = {
    pricingType: PropTypes.string.isRequired,
  }

  render() {
    return (
      <h1>{this.props.pricingType} > Executive</h1> 
    );
  }
}

export default PricingExecutive;
