import React from 'react';
import { Link } from 'react-router';

function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <span className="text">© Minicabit 2016</span>
        <span className="spacer">·</span>
        <Link className="link" to="/">Home</Link>
        <span className="spacer">·</span>
        <Link className="link" to="/privacy">Privacy</Link>
      </div>
    </footer>
  );
}

export default Footer;
