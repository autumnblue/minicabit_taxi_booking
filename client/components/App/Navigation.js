import React from 'react';
import NavigationLink from './NavigationLink';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import Logout from '../Logout';

function Navigation() {
  return (
    <Navbar.Collapse>
      <Nav>
        <li role="presentation" eventKey={1} className="link"><NavigationLink to="/about">About</NavigationLink></li>
        <NavDropdown eventKey={2} className="link" title="Policy" id="policy-nav-dropdown">
          <li role="presentation" eventKey={2.1}><NavigationLink to="/privacy">Privacy</NavigationLink></li>
        </NavDropdown>
      </Nav>

      <Nav pullRight>
        <NavDropdown className="link" title="My Account" id="myaccount-nav-dropdown">
          <li role="presentation"><Logout /></li>
        </NavDropdown>
      </Nav>
    </Navbar.Collapse>
  );
}

export default Navigation;
