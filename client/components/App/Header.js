import React from 'react';
import { Link } from 'react-router';
import Navigation from './Navigation';
import { Navbar } from 'react-bootstrap';

function Header() {
  return (
    <div className="header">
      <Navbar inverse fixedTop fluid>
        <Navbar.Header>
          <Navbar.Brand>
            <Link className="title" to="/">
              <img className="logo"
                   src="https://d2c5ltbybbpf73.cloudfront.net/1460046976368/static/images/logos/minicabit-logo.png"
                   height="30" alt="Minicabit Admin"
              />
              <span className="brand-title">Admin Panel</span>
            </Link>
          </Navbar.Brand>

          <Navbar.Toggle />

        </Navbar.Header>

        <Navigation />

      </Navbar>
    </div>
  );
}

export default Header;
