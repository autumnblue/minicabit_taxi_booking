/* eslint-disable no-undef */
import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class NavigationLink extends Component {
  static propTypes = {
    children: PropTypes.node
  }

  handleClick(event) {
    if (typeof $ !== 'undefined') {
      $(event.target).parents('.dropdown').click();
    }
  }

  render() {
    return (
      <Link {...this.props} onClick={this.handleClick}>
        {this.props.children}
      </Link>
    );
  }
}

export default NavigationLink;
