import React, { Component, PropTypes } from 'react';
import Header from './Header';
import Footer from './Footer';
import MessageBox from '../MessageBox';

// App layout styles at /styles/layout.scss

export default class App extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    children: PropTypes.node
  }

  static childContextTypes = {
    location: PropTypes.object
  }

  getChildContext() {
    return { location: this.props.location };
  }

  render() {
    return (
      <div id="root">
        <Header />
        <div id="content">
          {this.props.children}
        </div>
        <Footer />

        <MessageBox />
      </div>
    );
  }
}
