import React, { Component, PropTypes } from 'react';

class PricingMinicab extends Component {

  static propTypes = {
    pricingType: PropTypes.string.isRequired,
  }

  render() {
    return (
      <h1>{this.props.pricingType} > Minicab</h1> 
    );
  }
}

export default PricingMinicab;
