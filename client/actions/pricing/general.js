/* eslint-disable no-unused-vars */
import { CALL_API } from '../../middleware/api';

export const API_GET_UPLIFT_PRICING_REQUEST = 'API_GET_UPLIFT_PRICING_REQUEST';
export const API_GET_UPLIFT_PRICING_SUCCESS = 'API_GET_UPLIFT_PRICING_SUCCESS';
export const API_GET_UPLIFT_PRICING_FAILURE = 'API_GET_UPLIFT_PRICING_FAILURE';

export const API_POST_GENERAL_PRICING_REQUEST = 'API_POST_GENERAL_PRICING_REQUEST';
export const API_POST_GENERAL_PRICING_SUCCESS = 'API_POST_GENERAL_PRICING_SUCCESS';
export const API_POST_GENERAL_PRICING_FAILURE = 'API_POST_GENERAL_PRICING_FAILURE';

export const UPDATE_MILTIPLIER_TYPE = 'UPDATE_MILTIPLIER_TYPE';

const accessToken = 'D5qDCaUXuEXZy2yaQD41lMgcxEcsQlSBsfUAS7voBwT2DB5YUBicQTuHValiMyY1';

function fetchUpliftPricingPayload(mcoId) {
  return {
    [CALL_API]: {
      types: [API_GET_UPLIFT_PRICING_REQUEST, API_GET_UPLIFT_PRICING_SUCCESS, API_GET_UPLIFT_PRICING_FAILURE],
      endpoint: `/mcos/pricing/${mcoId}/upliftPricing?access_token=${accessToken}`,
      method: 'get',
    }
  };
}

export function getUpliftPricing(mcoId) {
  return (dispatch, getState) => {
    return dispatch(fetchUpliftPricingPayload(mcoId));
  };
}

export function addGeneralPricingToUpdate(index, multiply, multiplyType) {
  return {
    type: UPDATE_MILTIPLIER_TYPE,
    multiply,
    multiplyType,
    index
  };
}

function sendGeneralPricingUpdate(mcoId, data) {
  return {
    [CALL_API]: {
      types: [API_POST_GENERAL_PRICING_REQUEST, API_POST_GENERAL_PRICING_SUCCESS, API_POST_GENERAL_PRICING_FAILURE],
      endpoint: `/mcos/pricing/${mcoId}/upliftPricing?access_token=${accessToken}`,
      method: 'post',
      data
    }
  };
}

export function postGeneralPricing(mcoId, data) {
  return (dispatch, getState) => {
    return dispatch(sendGeneralPricingUpdate(mcoId, data));
  };
}
