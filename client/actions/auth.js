/* eslint-disable no-unused-vars */
import { CALL_API } from '../middleware/api';

// ======================================================================================
/**
 * Signup API action
 */
export const SIGNUP_REQUEST = 'SIGNUP_REQUEST';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_FAILURE = 'SIGNUP_FAILURE';

function fetchSignupUser(form) {
  return {
    [CALL_API]: {
      types: [SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_FAILURE],
      endpoint: '/signup',
      method: 'post',
      data: form
    }
  };
}

export function signupUser(form) {
  return (dispatch, getState) => {
    return dispatch(fetchSignupUser(form));
  };
}


// ======================================================================================
/**
 * Login API action
 */
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

function fetchLoginUser(email, password) {
  return {
    [CALL_API]: {
      types: [LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE],
      endpoint: '/login',
      method: 'post',
      data: { email, password },
      successRedirect: '/'
    }
  };
}

export function loginUser(email, password) {
  return (dispatch, getState) => {
    return dispatch(fetchLoginUser(email, password));
  };
}


// ======================================================================================
/**
 * Logout API action
 */
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';

function fetchLogoutUser() {
  return {
    [CALL_API]: {
      types: [LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE],
      endpoint: '/logout',
      method: 'get',
      successRedirect: '/'
    }
  };
}

export function logoutUser() {
  return (dispatch, getState) => {
    return dispatch(fetchLogoutUser()).then(() => {
      // TODO: show some notification saying `you have been logged out`
      // or this can be done where this logout action is invoked ? needs to check
    });
  };
}

