/**
 * Open/Close message box
 * @param   message   text content of message box
 * @param   style     type/style of message box - 'success', 'error', 'warning' (optional); default is 'warning'
 * @param   title     title of message box (optional)
 */
export const OPEN_MESSAGEBOX = 'OPEN_MESSAGEBOX';
export const CLOSE_MESSAGEBOX = 'CLOSE_MESSAGEBOX';
export function openMessageBox(message, style, title) {
  return {
    type: OPEN_MESSAGEBOX,
    message,
    style,
    title
  };
}
export function closeMessageBox() {
  return {
    type: CLOSE_MESSAGEBOX
  };
}
