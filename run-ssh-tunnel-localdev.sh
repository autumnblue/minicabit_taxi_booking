#!/usr/bin/env bash

#
# usage: ./run-ssh-tunnel-localdev.sh <ssh username>
#

if [ $# -eq 0 ]; then
    echo "usage: run-ssh-tunnel-localdev.sh <ssh username>"
    exit 1
fi

NAME=$1
SSH_CTRL_PATH=~/.ssh-tunnel
DB_HOST=minicabit-node-development-2.crzpkmya5lo8.eu-west-1.rds.amazonaws.com
REDIT_HOST=mc-dev.v6xqbs.0001.euw1.cache.amazonaws.com
BASTION_SERVER=52.50.179.210

echo "Starting db tunnnel to $DB_HOST"
ssh -L 3306:$DB_HOST:3306  -f -N -M -S $SSH_CTRL_PATH.DB $NAME@$BASTION_SERVER

echo "starting redis tunnel to $REDIT_HOST"
ssh -L 6379:$REDIT_HOST:6379  -f -N -M -S $SSH_CTRL_PATH.REDIS $NAME@$BASTION_SERVER

echo "to manually stop"
echo "ssh -S $SSH_CTRL_PATH.DB -O exit $NAME@$BASTION_SERVER"
echo "ssh -S $SSH_CTRL_PATH.REDIS -O exit $NAME@$BASTION_SERVER"

function stop {
   read -n 1 -p "press any key to stop"
   ssh -S $SSH_CTRL_PATH.DB -O exit $NAME@$BASTION_SERVER
   ssh -S $SSH_CTRL_PATH.REDIS -O exit $NAME@$BASTION_SERVER
}

stop