var gulp            = require('gulp');
// var gulpPlugins     = require('gulp-load-plugins')({ lazy: true });
var runSequence     = require('run-sequence');

var env = process.argv.indexOf('--prod') > -1 || process.env.NODE_ENV === 'production' ? 'production' : 'development';

require('require-dir')('./tools/tasks');


gulp.task('default', ['start']);


/**
 * Tasks for local development
 */
gulp.task('start', function(done) {
  return runSequence('env:' + env, 'clean', 'copy:assets', 'nodemon', done);
});

/**
 * Tasks to build the client app for deployment
 */
gulp.task('build', function(done) {
  return runSequence('env:' + env, 'clean', 'copy:assets', 'webpack-build', 'inject', done);
});

/**
 * Tasks for tests and QA of server code
 */
gulp.task('test:server', function(done) {
  return runSequence('env:' + env, 'mocha:server', 'eslint:server', 'eslint:tests:server', done);
});

/**
 * Tasks for tests and QA of client code
 */
gulp.task('test:client', function(done) {
  return runSequence('env:' + env, 'csslint', 'sasslint', 'mocha:client', 'eslint:client', 'eslint:tests:client', done);
});

/**
 * Tasks for tests and QA of the entire project
 */
gulp.task('test', function(done) {
  return runSequence('env:' + env, 'csslint', 'sasslint', 'mocha', 'eslint', done);
});