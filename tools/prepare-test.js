/**
 * Enable ES6, ES7
 */
require('babel-register')({
  presets: ['es2015', 'stage-0'],
  // only: /test\/.*\.test\.js$/
});

/**
 * Include Babel polyfill
 */
require('babel-polyfill');

/**
 * Allow to require by path relative from the root directory
 */
require('rootpath')();