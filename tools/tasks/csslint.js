var gulp            = require('gulp');
var csslint         = require('gulp-csslint');
var sassLint        = require('gulp-sass-lint');

/**
 * Run CSSLint
 */
gulp.task('csslint', function(done) {
  gulp.src(['client/styles/**/*.css'])
    .pipe(csslint('.csslintrc'))
    .pipe(csslint.reporter())
    .on('end', done);
});

// Run SASS Lint
gulp.task('sasslint', function () {
  return gulp.src('client/styles/**/*.s+(a|c)ss')
    .pipe(sassLint({
      rules: {
        'no-css-comments': 0,
        'no-color-literals': 0,
        'hex-length': 0,
        'no-ids': 0,
        'property-sort-order': 0,
        'no-important': 0,
        'force-pseudo-nesting': 0
      }
    }))
    .pipe(sassLint.format());
});
