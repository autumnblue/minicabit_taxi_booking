var gulp            = require('gulp');
var mocha           = require('gulp-mocha');
var runSequence     = require('run-sequence');

/**
 * Run tests for server
 */
gulp.task('mocha:server', function() {

  require('../prepare-test');

  if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development';
  }
  process.env.TESTING = true;

  return gulp.src('test/server/**/*.test.js')
    .pipe(mocha({
      timeout: 15000
    }));
    // .once('error', () => { process.exit(1); })
    // .once('end', () => { process.exit(1); });
});

/**
 * Run tests for client
 */
gulp.task('mocha:client', function() {

  require('../prepare-test');

  if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development';
  }
  process.env.TESTING = true;

  return gulp.src('test/client/**/*.test.js')
    .pipe(mocha({
      timeout: 15000
    }));
});

/**
 * Run tests for both server and client
 */
gulp.task('mocha', function(done) {
  return runSequence('mocha:server', 'mocha:client', done);
});