var gulp            = require('gulp');
var eslint          = require('gulp-eslint');
var runSequence     = require('run-sequence');
var path            = require('path');

/**
 * Run ESLint for server
 */
gulp.task('eslint:server', function() {
  return gulp.src(['server/**/*.js'])
    .pipe(eslint(path.resolve(__dirname, '../../server/.eslintrc')))
    .pipe(eslint.format())
    .pipe(eslint.results(function(results) {
      // done();
    }));
});

/**
 * Run ESLint for client
 */
gulp.task('eslint:client', function() {
  return gulp.src(['client/**/*.js', '!client/assets/**'])
    .pipe(eslint(path.resolve(__dirname, '../../client/.eslintrc')))
    .pipe(eslint.format())
    .pipe(eslint.results(function(results) {
      // done();
    }));
});

/**
 * Run ESLint for tests
 */
gulp.task('eslint:tests', function() {
  return gulp.src(['test/**/*.js'])
    .pipe(eslint(path.resolve(__dirname, '../../test/.eslintrc')))
    .pipe(eslint.format())
    .pipe(eslint.results(function(results) {
      // done();
    }));
});
gulp.task('eslint:tests:server', function() {
  return gulp.src(['test/server/**/*.js'])
    .pipe(eslint(path.resolve(__dirname, '../../test/.eslintrc')))
    .pipe(eslint.format())
    .pipe(eslint.results(function(results) {
      // done();
    }));
});
gulp.task('eslint:tests:client', function() {
  return gulp.src(['test/client/**/*.js'])
    .pipe(eslint(path.resolve(__dirname, '../../test/.eslintrc')))
    .pipe(eslint.format())
    .pipe(eslint.results(function(results) {
      // done();
    }));
});

/**
 * Run ESLint for server, client and tests
 */
gulp.task('eslint', function(done) {
  return runSequence('eslint:server', 'eslint:client', 'eslint:tests', done);
});