'use strict';

var defaultConfig = require('./config');
var _ = require('lodash');

module.exports = _.merge(defaultConfig, {
  EXAMPLE_DEVELOPMENT_CONFIG_ENTITY: 'placeholder'
});
