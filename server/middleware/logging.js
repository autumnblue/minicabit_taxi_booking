var logger = require('../logger');


module.exports = function() {
  return function logging(req, res, next) {
    console.log('Request tracking middleware triggered on %s', req.url);
    next();
  };
};
