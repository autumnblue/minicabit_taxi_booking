/**
 * Created by dejan on 09/08/16.
 */

'use strict';

var mandrill = require('mandrill-api/mandrill');
var logger = require('./../logger');
var config = require('../config');

var mandrillEmail = exports = module.exports = {};

mandrillEmail.sendEmail = function(templateName, templateContent, message, callback) {
  var mandrillClient = new mandrill.Mandrill(config.thirdPartyApis.mandrill.key);

  mandrillClient.messages.sendTemplate({
    'template_name': templateName,
    'template_content': templateContent,
    'message': message,
    'async': true
  },
    function(result) {
      logger.debug('mandrill result', JSON.stringify(result));
      callback(null, result);
    }, function(e) {
      logger.error('A mandrill error occurred: ' + e.name + ' - ' + e.message);
      callback(e);
    });
};
