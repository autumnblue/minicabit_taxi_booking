/**
 * Created by dejan on 09/08/16.
 */

'use strict';

var _ = require('lodash');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');
var utils = require('loopback/lib/utils.js');
var config = require('../../config');
var nm = require('../../helpers/emailContentCreator');

module.exports = function(Notification) {
  Notification.resendEmail = function(data, callback) {
    if (!data.bookingRef)
      return callback('Empty bookingRef');

    if (!data.receiver || !(data.receiver.toUpperCase() === 'PHO' || data.receiver.toUpperCase() === 'CUSTOMER'))
      return callback('Empty or invalid receiver');

    if (!data.type || !(data.type.toUpperCase() === 'CONFIRMATION' || data.type.toUpperCase() === 'CANCELLATION'))
      return callback('Empty or invalid type');

    var bookingModel = server.models.booking;

    bookingModel.findOne({
      where: {bookingLinktext: data.bookingRef},
      include: ['customer', 'pickupAddress', 'destinationAddress', 'mco']}, function(err, result) {
      if (err) {
        logger.error('Error getting booking details [ ' + data.bookingRef + ' ] ' + err);
        return callback(err);
      } else {
        if (result) {
          try {
            var json = JSON.stringify(result);
            var booking = JSON.parse(json);

            switch (data.type.toUpperCase()) {
              case 'CONFIRMATION':
                var bookingData = {
                  bookingOutbound: booking,
                  customer: booking.customer,
                  fromAddress: booking.pickupAddress,
                  toAddress: booking.destinationAddress,
                  payments: {},
                  reqData: {}
                };

                bookingData.payments.type = booking.paymentType;
                if (bookingData.bookingOutbound.type.toUpperCase() === 'OUTBOUND' ||
                  bookingData.bookingOutbound.type.toUpperCase() === 'INBOUND') {
                  bookingData.reqData.tripType = 'SINGLE';
                } else {
                  bookingData.reqData.tripType = bookingData.bookingOutbound.type;
                }

                nm.sendBookingConfirmation(bookingData, data.receiver, function(err, result) {
                  if (err) {
                    return callback(err);
                  } else {
                    return callback(null, result);
                  }
                });
                break;
              case 'CANCELLATION':
                if (booking.bookingStatus.toUpperCase() != 'CANCELLED') {
                  throw new Error('This booking has not been cancelled ' + data.bookingRef);
                }
                nm.sendBookingCancellation(booking, data.receiver, function(err, result) {
                  if (err) {
                    return callback(err);
                  } else {
                    return callback(null, result);
                  }
                });
                break;
              default:
                err = new Error('Invalid type');
                err.status = 400;
                err.name = 'Bad Request';
                return callback(err);
                break;
            }
          } catch (e) {
            return callback('Error resending mail ' + e);
          }
        } else {
          err = new Error('No booking found');
          err.status = 404;
          err.name = 'Not Found';
          return callback(err);
        }
      }
    });
  };

  loopback.remoteMethod(
    Notification.resendEmail, {
      accepts: [
        {
          arg: 'data',
          type: 'object',
          required: true,
          description: 'notification params',
          'http': {source: 'body'}
        }
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/resend', verb: 'post', status: 200, errorStatus: 400},
      description: 'resending mails for mco and customer'
    }
  );
};
