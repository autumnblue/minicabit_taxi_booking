'use strict';
var _ = require('lodash');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');
var utils = require('loopback/lib/utils.js');

module.exports = function(Booking) {

  Booking.return = function(bookingRef, callback) {

    var bookingModel = server.models.booking;
    var addressModel = server.models.address;

    bookingModel.findOne({
      where: {bookingLinktext: bookingRef},
      include: ['customer', 'pickupAddress', 'destinationAddress', 'driver']
    }, function(err, result) {
      if (err) {
        logger.error('Error getting booking details [ ' + bookingRef + ' ] ' + err);
        callback(err);
      } else {
        if (result) {

          var addresses = result['viaAddressIds'].split(',');

          result.addressVias = [];

          var addressProcessed = 0;

          if (addresses.length > 0) {
            _.forEach(addresses, function (addressId) {
              addressProcessed++;
              addressModel.findOne({where: {addressId: addressId}}, function (err, resultAddress) {
                if (err) {
                  return callback(err);
                } else {
                  result.addressVias.push(resultAddress);
                  if (addressProcessed === addresses.length) {
                    logger.debug('Returning booking...', result);
                    return callback(null, result);
                  }
                }
              });
            });
          } else {
            return callback(null, result);
          }
        } else {
          err = new Error('No booking found');
          err.status = 404;
          err.name = 'Not Found';
          return callback(err);
        }
      }
    });
  };

  loopback.remoteMethod(
    Booking.return, {
      accepts: [
        {
          arg: 'bookingRef', type: 'string', required: true, description: 'booking ref no'
        },
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/:bookingRef', verb: 'get', status: 200, errorStatus: 400},
      description: 'get booking details for booking ref no'
    }
  );

  Booking.updateBooking = function(bookingRef, data, cb) {
    var bookingModel = server.models.booking;
    var runAfterUpdate = function() {};

    var d = {};

    if (data['driverNotificationStatus']) {
      d.driverNotificationStatus = data['driverNotificationStatus'];
      runAfterUpdate = function(booking, accountUsername) {
        var driverNotificationAnswers = server.models.driverNotificationAnswers;
        driverNotificationAnswers.create({
          comment: 'Updated via node-admin',
          driverAnswer: booking.driverNotificationStatus,
          statusTimestamp: new Date(),
          mcoId: booking.mcoId,
          bookingLinktext: booking.bookingLinktext,
          author: accountUsername
        }, function(err, result) {
          if (!err) {
            logger.debug('Successfully created driverNotificationStatus record', booking.bookingLinktext, result);
          }
        });
      };
    }

    if (data['mcoId']) {
      d.mcoId = data['mcoId'];
    }

    if (data['bookingStatus']) {
      d.bookingStatus = data['bookingStatus'];
    }

    if (data['bookingCompletedDts']) {
      d.bookingCompletedDts = data['bookingCompletedDts'];
    }

    if (data['additionalInformation']) {
      d.additionalInformation = data['additionalInformation'];
    }

    if (data['pickupEtaDts']) {
      d.pickupEtaDts = data['pickupEtaDts'];
    }

    if (data['mciCheck']) {
      d.mciCheck = data['mciCheck'];
    }

    if (data['flightInfo']) {
      d.flightInfo = data['flightInfo'];
    }

    if (data['driverNotes']) {
      d.driverNotes = data['driverNotes'];
    }

    if (data['notes']) {
      d.notes = data['notes'];
    }

    if (data['isDispute']) {
      d.isDispute = data['isDispute'];
    }

    bookingModel.updateAll({bookingLinktext: bookingRef}, d, function(err, result) {
      if (err) {
        return cb(err);
      } else {
        logger.debug('Booking successfuly updated', bookingRef, result);

        Booking.return(bookingRef, function(err, bookingResult) {
          if (err) {
            return cb(err);
          } else {
            runAfterUpdate(bookingResult, data['accountUsername']);
            return cb(null, bookingResult);
          }
        });
      }
    });
  };

  loopback.remoteMethod(
    Booking.updateBooking, {
      accepts: [
        {
          arg: 'bookingRef', type: 'string', required: true, description: 'booking ref no'
        },
        {
          arg: 'data', type: 'object', required: true, description: 'Booking data', 'http': {source: 'body'}
        },
      ],
      returns: {arg: 'result', type: 'string'},
      http: {path: '/:bookingRef', verb: 'put', status: 200, errorStatus: 400},
      description: 'update booking details for booking ref no'
    }
  );


  Booking.getCustomer = function(bookingRef, cb) {
    var bookingModel = server.models.booking;

    bookingModel.findOne({
      where: {bookingLinktext: bookingRef},
      include: {relation: 'customer',
        scope: {fields: ['customerId', 'title', 'firstname', 'lastname', 'customerMobileNumber']} }
    }, function(err, result) {

      if (err) {
        logger.error('Error returning passengers for booking ref', bookingRef, err);
        return cb(err);
      } else {
        logger.debug('Returning passengers for booking ref', result);
        var c = result.toJSON();
        return cb(null, c.customer);
      }
    });
  }

  loopback.remoteMethod(
    Booking.getCustomer, {
      accepts: [
        {
          arg: 'bookingRef', type: 'string', required: true, description: 'booking ref no'
        }
      ],
      returns: {arg: 'result', type: 'string'},
      http: {path: '/:bookingRef/customer', verb: 'get', status: 200, errorStatus: 400},
      description: 'get passengers for booking with ref no'
    }
  );

  Booking.addCustomer = function(bookingRef, customerData, callback) {

    Booking.beginTransaction({isolationLevel: Booking.Transaction.READ_COMMITTED}, function(err, tx) {

      Booking.findOne({where: {bookingLinktext: bookingRef}}, function(err, booking) {

        if (err) {
          logger.error('Error adding passenger - booking find', bookingRef, err);
          tx.rollback(function(te) {

            if (te) {
              logger.error('Error rolling back transaction', te);
            }

            return callback(err);
          });
        } else {

          var customerModel = server.models.customer;

          customerModel.create(customerData, {transaction: tx}, function(err, customer) {

            if (err) {
              logger.error('Error creating customer for booking', bookingRef, customer, err);
              tx.rollback(function(te) {

                if (te) {
                  logger.error('Error rolling back transaction', te);
                }

                return callback(err);
              });
            } else {

              booking.updateAttributes({customerId: customer.customerId}, {transaction: tx}, function(err, b) {

                if (err) {
                  logger.error('Error adding passenger - updating booking', bookingRef, err);
                  tx.rollback(function(te) {

                    if (te) {
                      logger.error('Error rolling back transaction', te);
                    }

                    return callback(err);
                  });

                } else {

                  tx.commit(function(te) {
                    if (te) {
                      logger.error('Error rolling back transaction', te);
                      return callback(te);
                    } else {
                      logger.debug('Customer added to booking', customer);
                      return callback(null, customer);
                    }
                  });
                }
              });
            }
          });
        }
      });
    });
  }


  loopback.remoteMethod(
    Booking.addCustomer, {
      accepts: [
        {
          arg: 'bookingRef', type: 'string', required: true, description: 'booking ref no'
        },
        {
          arg: 'data', type: 'object', required: true, description: 'Customer data', 'http': {source: 'body'}
        }
      ],
      returns: {arg: 'result', type: 'string'},
      http: {path: '/:bookingRef/customer', verb: 'post', status: 202, errorStatus: 400},
      description: 'get passengers for booking with ref no'
    }
  );


};
