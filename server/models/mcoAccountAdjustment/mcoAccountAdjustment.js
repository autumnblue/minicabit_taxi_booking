module.exports = function(McoAccountAdjustment) {
  McoAccountAdjustment.observe('before save', function filterProperties(ctx, next) {
    ctx.data.lastUpdated = new Date();
    next();
  });
};

