/**
 * Created by dejan on 11/08/16.
 */

'use strict';

var _ = require('lodash');
var loopback = require('loopback');
var async = require('async');
var moment = require('moment');
var server = require('./../../server');
var logger = require('./../../logger');
var utils = require('loopback/lib/utils.js');
var generateId = require('../../helpers/uniqIdsCreator');
var fs = require('fs');
var json2csv = require('json2csv');
var nm = require('../../helpers/emailContentCreator');

module.exports = function(Statement) {

  function getGlobalSettings() {
    var globalSettingsModel = server.models.globalSettings;
    return new Promise(function(resolve, reject) {
      globalSettingsModel.findOne({where: {settingName: 'statements_start_time'}}, function(err, time) {
        if (err) {
          logger.error('Error with getting statementsStartTime from global settings ' + err);
          reject();
        } else {
          resolve(time);
        }
      });
    });
  }

  function getParentMco(mcoId) {
    var mcoModel = server.models.mco;
    return new Promise(function(resolve, reject) {
      mcoModel.findOne({where: {mcoId: mcoId}, fields: ['parentMcoId']}, function(err, result) {
        if (err) {
          logger.error('Error retrieving data from mco table for mcoId [ ' + mcoId + ' ] ' + err);
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  function getBookings(whereStatement, include, limit, offset) {
    var bookingModel = server.models.booking;
    return new Promise(function(resolve, reject) {
      bookingModel.find({
        where: whereStatement,
        include: include,
        order: 'bookingDate DESC',
        limit: limit,
        offset: offset
      }, function(err, result) {
        if (err) {
          logger.error('Error retrieving data from bookingModel');
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  function getMcos(whereStatement, include) {
    var mcoModel = server.models.mco;
    return new Promise(function(resolve, reject) {
      mcoModel.find({
        where: whereStatement,
        include: include
      }, function(err, result) {
        if (err) {
          logger.error('Error retrieving data from mco model');
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  function getInvoices(whereStatement, include, limit, offset) {
    var invoiceModel = server.models.mcoInvoice;
    return new Promise(function(resolve, reject) {
      invoiceModel.find({
        where: whereStatement,
        include: include,
        order: 'invoiceTimestamp DESC DESC',
        limit: limit,
        offset: offset
      }, function(err, result) {
        if (err) {
          logger.error('Error retrieving data from mcoInvoice');
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  function getAccountAdjustments(whereStatement, include) {
    var mcoAccountAdjustmentModel = server.models.mcoAccountAdjustment;
    return new Promise(function(resolve, reject) {
      mcoAccountAdjustmentModel.find({
        where: whereStatement,
        include: include
      }, function(err, result) {
        if (err) {
          logger.error('Error retrieving data from mcoAccountAdjustment model');
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  function getBookingPriceAdjustments(whereStatement, include) {
    var bookingPriceAdjustmentsModel = server.models.bookingPriceAdjustments;
    return new Promise(function(resolve, reject) {
      bookingPriceAdjustmentsModel.find({
        where: whereStatement,
        include: include
      }, function(err, result) {
        if (err) {
          logger.error('Error retrieving data from bookingPriceAdjustments model');
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  function writeCsvFile(input, fields, preview) {
    return new Promise(function(resolve, reject) {
      var fileName = 'payruns/' + preview + new Date().toDateString();
      fs.writeFile(fileName + '.csv', input, function(err) {
        if (err) {
          reject(err);
        } else {
          resolve(input);
        }
      });
    });
  }

  function transactionPromise(mcoPayrun, mcoReconcile, mcoInvoices, bookingIds, mcoAccountAdjustmentIds, mcoBpaIds) {
    var mcoReconcileModel = server.models.mcoReconcile;
    var mcoInvoiceModel = server.models.mcoInvoice;
    var bookingModel = server.models.booking;
    var mcoAccountAdjustmentModel = server.models.mcoAccountAdjustment;
    var mcoPayrunModel = server.models.mcoPayrun;
    var bookingPriceAdjustments = server.models.bookingPriceAdjustments;
    var mcoPayrunId = null;

    return new Promise(function(resolve, reject) {
      mcoReconcileModel.beginTransaction({
        isolationLevel: Statement.Transaction.READ_COMMITTED,
        timeout: 45000
      }, function(err, tx) {
        async.waterfall([
          function(callback) {
            mcoPayrunModel.create(mcoPayrun, {transaction: tx}, function(err, result) {
              if (err) {
                logger.error('Error saving mco payrun data to mcoPayrun table: ' + err);
                callback(err);
              } else {
                mcoPayrunId = result.mcoPayrunId;
                callback(err, result);
              }
            });
          },
          function(err, callback) {
            mcoReconcileModel.create(mcoReconcile, {transaction: tx}, function(err, result) {
              if (err) {
                logger.error('Error saving mco reconcile data to mcoReconcile table: ' + err);
                callback(err);
              } else {
                callback(err, result);
              }
            });
          },
          function(err, callback) {
            for (var i = 0; i < mcoInvoices.length; i++) {
              mcoInvoices[i].mcoPayrunId = mcoPayrunId;
            }
            mcoInvoiceModel.create(mcoInvoices, {transaction: tx}, function(err, result) {
              if (err) {
                logger.error('Error saving mco invoices data to mcoInvoice table: ' + err);
                callback(err);
              } else {
                callback(err, result);
              }
            });
          },
          function(err, callback) {
            if (bookingIds && bookingIds.or.length !== 0) {
              bookingModel.updateAll(bookingIds, {paid: 1}, {transaction: tx}, function(err, result) {
                if (err) {
                  logger.error('Error updating bookings to paid = 1 : ' + err);
                  callback(err);
                } else {
                  callback(err, result);
                }
              });
            } else {
              callback(null, 0);
            }
          },
          function(err, callback) {
            for (var i = 0; i < mcoAccountAdjustmentIds.length; i++) {
              mcoAccountAdjustmentModel.updateAll(
                  mcoAccountAdjustmentIds[i].mcoAccountAdjustmentWhere,
                  {invoiceId: mcoAccountAdjustmentIds[i].invoiceId}, {transaction: tx}, function(err, result) {
                    if  (err) {
                      logger.error('Error updating mcoAccountAdjustmentModel: ' + err);
                      return callback(err);
                    }
                  });
            }
            callback(null, 'success');
          },
          function(err, callback) {
            if (mcoBpaIds.length) {
              bookingPriceAdjustments.updateAll({ or: mcoBpaIds }, { paid: 1 },
              { transaction: tx }, function(err, result) {
                if (err) {
                  logger.error('Error updating reallocated booking adjustments to paid=1', err);
                  return callback(err);
                } else if (result.count === mcoBpaIds.length) {
                  return callback(null, result);
                } else {
                  return callback(new Error('Not all booking price adjusments updated'));
                }
              });
            } else {
              callback(null, 0);
            }
          }
        ], function finish(finErr, result) {
          if (finErr) {
            logger.error('Error rollbacking transaction for saving data: ' + finErr);
            tx.rollback(function(err) {
              if (err) {
                logger.error('Error rollbacking transaction for saving data: ' + err);
                return reject(err);
              } else {
                return reject(finErr);
              }
            });
          } else {
            tx.commit(function(err) {
              if (err) {
                logger.error('Error when commiting transaction for saving data: ' + err);
                return reject(err);
              } else {
                return resolve(mcoPayrunId);
              }
            });
          }
        });
      });
    });
  }

  Statement.returnPendingBooking = function(mcoId, limit, offset, callback) {
    var statementsStartTime = null;

    getGlobalSettings()
      .then(function(time) {
        if (time) {
          statementsStartTime = new Date(time.value);
        }
        return getParentMco(mcoId);
      })
      .then(function(parentMcoId) {
        var whereStatement = '';
        if (!parentMcoId || parentMcoId.parentMcoId === 0) {
          whereStatement = {
            mcoId: mcoId,
            and: [
              {driverNotificationStatus: {neq: 'pob_confirmed_4'}},
              {driverNotificationStatus: {neq: 'driver_not_show_4'}},
              {driverNotificationStatus: {neq: 'other_4'}},
              {bookingStatus: {neq: 'cancelled'}},
              {pickupEtaDts: {lt: Date.now() + 10 * 60 * 1000}},
              {destinationEtaDts: {gt: statementsStartTime}}
            ]
          };
        } else {
          whereStatement = {
            or: [{mcoId: mcoId}, {subMcoId: parentMcoId}],
            and: [
              {driverNotificationStatus: {neq: 'pob_confirmed_4'}},
              {driverNotificationStatus: {neq: 'driver_not_show_4'}},
              {driverNotificationStatus: {neq: 'other_4'}},
              {bookingStatus: {neq: 'cancelled'}},
              {pickupEtaDts: {lt: Date.now() + 10 * 60 * 1000}},
              {destinationEtaDts: {gt: statementsStartTime}}
            ]
          };
        }

        var include = ['customer', 'pickupAddress', 'destinationAddress', 'driver', 'bookingPriceAdjustments'];

        return getBookings(whereStatement, include, limit, offset);
      }).then(function(pendingBookings) {
        logger.debug('Getting data for pending bookings');
        return callback(null, pendingBookings);
      }).catch(function(error) {
        logger.error('Error with receiving mco data');
        return callback(error);
      });
  };

  Statement.returnDisputedBooking = function(mcoId, limit, offset, callback) {
    var statementsStartTime = null;

    getGlobalSettings()
      .then(function(time) {
        if (time) {
          statementsStartTime = new Date(time.value);
        }
        return getParentMco(mcoId);
      })
      .then(function(parentMcoId) {
        var whereStatement = '';
        if (!parentMcoId || parentMcoId.parentMcoId === 0) {
          whereStatement = {
            mcoId: mcoId,
            and: [{isDispute: 1},
              {bookingStatus: {neq: 'cancelled'}},
              {pickupEtaDts: {lt: Date.now() + 10 * 60 * 1000}},
              {destinationEtaDts: {gt: statementsStartTime}}
            ]
          };
        } else {
          whereStatement = {
            or: [{mcoId: mcoId}, {subMcoId: parentMcoId}],
            and: [{isDispute: 1},
              {bookingStatus: {neq: 'cancelled'}},
              {pickupEtaDts: {lt: Date.now() + 10 * 60 * 1000}},
              {destinationEtaDts: {gt: statementsStartTime}}
            ]
          };
        }

        var include = ['customer', 'pickupAddress', 'destinationAddress', 'driver', 'bookingPriceAdjustments'];

        return getBookings(whereStatement, include, limit, offset);
      }).then(function(disputedBookings) {
        logger.debug('Getting data for disputed bookings');
        return callback(null, disputedBookings);
      }).catch(function(error) {
        logger.error('Error with receiving mco data');
        return callback(error);
      });
  };

  Statement.returnOutstandingBooking = function(mcoId, fromDate, toDate, callback) {
    var whereStatement = '';
    var include = ['customer', 'pickupAddress', 'destinationAddress', 'driver', 'bookingAccounting',
      {
        relation: 'bookingPriceAdjustments',
        scope: {
          where: {mcoId: mcoId}
        }
      }
    ];
    var bookings, accountAdjustments;

    fromDate = (fromDate) ? new Date(fromDate) : null;
    var toDateBak = toDate;
    toDate = (toDate) ? new Date(toDate) : new Date();
    if (toDateBak && typeof toDateBak === 'string' && toDateBak.length === 10) {
      toDate = moment(toDate).endOf('day').toDate();
    }

    getGlobalSettings()
      .then(function(time) {
        // we need to check if statementsStartTime exist and compare it with from date
        if (time) {
          var statementsStartTime = new Date(time.value);

          if ((!fromDate) || (statementsStartTime.getTime() > fromDate.getTime())) {
            fromDate = statementsStartTime;
          }
        }
        return getParentMco(mcoId);
      })
      .then(function(parentMcoId) {
        var dateRange = null;

        if (fromDate && toDate) {
          dateRange = {bookingCompletedDts: {between: [fromDate, toDate]}};
        }

        if (!parentMcoId || parentMcoId.parentMcoId === 0) {
          whereStatement = {
            mcoId: mcoId,
            and: [dateRange, {isDispute: 0}, {paid: 0},
              {bookingStatus: {neq: 'cancelled'}}, {or: [
              {driverNotificationStatus: 'pob_confirmed_3'},
              {driverNotificationStatus: 'pob_confirmed_4'},
              {driverNotificationStatus: 'passenger_not_show_4'},
              {driverNotificationStatus: 'other_4'}]}]
          };
        } else {
          whereStatement = {
            or: [{mcoId: mcoId}, {subMcoId: parentMcoId}],
            and: [dateRange, {isDispute: 0}, {paid: 0},
              {bookingStatus: {neq: 'cancelled'}}, {or: [
              {driverNotificationStatus: 'pob_confirmed_3'},
              {driverNotificationStatus: 'pob_confirmed_4'},
              {driverNotificationStatus: 'passenger_not_show_4'},
              {driverNotificationStatus: 'other_4'}]}]
          };
        }

        return getBookings(whereStatement, include);
      }).then(function(outstandingBookings) {
        var tmp = JSON.stringify(outstandingBookings);
        bookings = JSON.parse(tmp);
        var listOfPromises = [];
        var bookingAccounting = server.models.bookingAccounting;

        // if bookingAccounting not exist we need to insert it
        for (var i = 0; i < bookings.length; i++) {
          if (!bookings[i].bookingAccounting) {
            var p = new Promise(function(resolve, reject) {
              bookingAccounting.insertInto(bookings[i].vendortxcode, function(err, result) {
                if (err) {
                  reject(err);
                } else {
                  resolve(result);
                }
              });
            });

            listOfPromises.push(p);
          }
        }
        return Promise.all(listOfPromises);
      })
      .then(function() {
        return getAccountAdjustments({
          mcoId: mcoId,
          created: { between: [fromDate, toDate] },
          invoiceId: null
        });
      })
      .then(function(mcoAccountAdjustments) {
        var tmp = JSON.stringify(mcoAccountAdjustments);
        accountAdjustments = JSON.parse(tmp);

        // ticket api-1571

        var bookingPriceAdjustmentsQueryWhere = {
          and: [{mcoId: mcoId}, {deleted: 0}, {paid: 0}]
        };
        var bookingPriceAdjustmentsQueryInclude = {
          relation: 'booking',
          scope: {
            filter: {
              where: {
                and: [
                  { bookingCompletedDts: {between: [fromDate, toDate]} },
                  { mcoId: { neq: mcoId } }
                ]
              }
            }
          }
        };
        return getBookingPriceAdjustments(bookingPriceAdjustmentsQueryWhere, bookingPriceAdjustmentsQueryInclude);
      }).then(function(bpaResults) {
        var results = JSON.stringify(bpaResults);
        var bpaParsedResult = JSON.parse(results);
        var bpaFinalResult = bpaParsedResult.filter(function(bpa) {
          return bpa.booking && !!bpa.booking.bookingCompletedDts;
        })
        .filter(function(bpa) {
          return bpa.booking.mcoId !== bpa.mcoId;
        });
        var additionalBookingPriceAdjustments = [];
        // object for total summary
        var totalSummary = {
          outstandingAmount: null,
          dueToPHO: null,
          fines: null,
          topUps: null,
          accountAdjustments: 0
        };

        // calculate summary for each booking
        for (var i = 0; i < bookings.length; i++) {
          // object for booking summary
          var bookingSummary = {
            outstandingAmount: null,
            dueToPHO: null,
            fines: null,
            topUps: null
          };

          // check paymentType (cash or card) different calculations
          // ___________________________________________CASH_CASE_______________________________________________________
          if (bookings[i].paymentType === 'cash') {
            // we need to make sure that we have bookingAccountingData
            if (bookings[i].bookingAccounting) {
              bookingSummary.outstandingAmount = (parseFloat(bookings[i].bookingAccounting.income) * (-1));
              bookingSummary.dueToPHO = (parseFloat(bookings[i].bookingAccounting.income) * (-1));
              // we need to make sure that we have bookingPriceAdjustmentsData
              if (bookings[i].bookingPriceAdjustments && bookings[i].bookingPriceAdjustments.length != 0) {
                for (var j = 0; j < bookings[i].bookingPriceAdjustments.length; j++) {
                  bookingSummary.outstandingAmount += parseFloat(bookings[i].bookingPriceAdjustments[j].amount);
                }
              }
            }
          // ___________________________________________CARD_CASE_______________________________________________________
          } else if (bookings[i].paymentType === 'card') {
            // we need to make sure that we have bookingAccountingData
            if (bookings[i].bookingAccounting) {
              bookingSummary.outstandingAmount = parseFloat(bookings[i].bookingAccounting.phoPay);
              bookingSummary.dueToPHO = parseFloat(bookings[i].bookingAccounting.phoPay);
              // we need to make sure that we have bookingPriceAdjustmentsData
              if (bookings[i].bookingPriceAdjustments && bookings[i].bookingPriceAdjustments.length != 0) {
                for (var j = 0; j < bookings[i].bookingPriceAdjustments.length; j++) {
                  bookingSummary.outstandingAmount += parseFloat(bookings[i].bookingPriceAdjustments[j].amount);
                }
              }
            }
          }
          // ___________________________________________________________________________________________________________

          // ________________________________________ADJUSTMENTS________________________________________________________
          if (bookings[i].bookingPriceAdjustments && bookings[i].bookingPriceAdjustments.length != 0) {
            for (var j = 0; j < bookings[i].bookingPriceAdjustments.length; j ++) {
              if (bookings[i].bookingPriceAdjustments[j].amount >= 0) {
                bookingSummary.topUps += parseFloat(bookings[i].bookingPriceAdjustments[j].amount);
              } else {
                bookingSummary.fines += parseFloat(bookings[i].bookingPriceAdjustments[j].amount);
              }
            }
          }
          // ___________________________________________________________________________________________________________
          bookings[i].summary = bookingSummary;

          // we also need to calculate total summary for all bookings
          totalSummary.outstandingAmount += bookingSummary.outstandingAmount;
          totalSummary.dueToPHO += bookingSummary.dueToPHO;
          totalSummary.fines += bookingSummary.fines;
          totalSummary.topUps += bookingSummary.topUps;
        }

        bpaFinalResult.forEach(function(bpa) {
          if (bpa.mcoId === mcoId) {
            if (bpa.amount > 0) {
              totalSummary.topUps += parseFloat(bpa.amount);
            } else {
              totalSummary.fines += parseFloat(bpa.amount);
            }
          }
          var tempBPA = _.clone(bpa);
          delete tempBPA['booking'];
          additionalBookingPriceAdjustments.push(tempBPA);
        });
        // ________________________________________ACCOUNT ADJUSTMENTS__________________________________________________
        for (i = 0; i < accountAdjustments.length; i++) {
          totalSummary.accountAdjustments += parseFloat(accountAdjustments[i].amount);
        }
        if (totalSummary.outstandingAmount) {
          // Don't add account adjustments sum into the outstanding amount when no bookings
          totalSummary.outstandingAmount += totalSummary.accountAdjustments;
        }

        if (totalSummary.fines) {
          totalSummary.fines = totalSummary.fines.toFixed(2);
          totalSummary.outstandingAmount += parseFloat(totalSummary.fines);
        }
        if (totalSummary.topUps) {
          totalSummary.topUps = totalSummary.topUps.toFixed(2);
          totalSummary.outstandingAmount += parseFloat(totalSummary.topUps);
        }
        if (totalSummary.outstandingAmount) totalSummary.outstandingAmount = totalSummary.outstandingAmount.toFixed(2);
        if (totalSummary.dueToPHO) totalSummary.dueToPHO = totalSummary.dueToPHO.toFixed(2);
        if (totalSummary.accountAdjustments) {
          totalSummary.accountAdjustments = totalSummary.accountAdjustments.toFixed(2);
        }


        var result = {
          bookings: bookings,
          accountAdjustments: accountAdjustments,
          additionalBookingPriceAdjustments: additionalBookingPriceAdjustments,
          totalSummary: totalSummary
        };

        logger.debug('Getting data for outstanding bookings');
        return callback(null, result);
      }).catch(function(error) {
        logger.error('Error with receiving mco data');
        return callback(error);
      });
  };

  Statement.payRun = function(data, callback) {
    if (!(data.toDate || data.fromDate)) {
      return callback('Either a "to" or "from" date must be provided');
    }
    if (data.markAsPaid === undefined || (data.markAsPaid != 0 && data.markAsPaid != 1)) {
      return callback('Missing or invalid markAsPaid param');
    }
    if (data.excludeNegativeValues === undefined ||
      (data.excludeNegativeValues != 0 && data.excludeNegativeValues != 1)) {
      return callback('Missing or invalid excludeNegativeValues param');
    }
    if (data.preview === undefined || (data.preview != 0 && data.preview != 1)) {
      return callback('Missing or invalid preview param');
    }


    var toDate = (data.toDate) ? new Date(data.toDate) : new Date();
    if (data.toDate && typeof data.toDate === 'string' && data.toDate.length === 10) {
      toDate = moment(toDate).endOf('day').toDate();
    }
    var fromDate = (data.fromDate) ? new Date(data.fromDate) : null;
    var markAsPaid = data.markAsPaid;
    var excludeNegativeValues = data.excludeNegativeValues;
    var excludeMco = data.excludeMco || null;
    var preview = data.preview;
    var settleSingleMCO = data.settleSingleMCOs || null;
    var sendEmail = data.sendEmail;
    var includeStatus = data.includeStatus || {};
    var includeFilter = Object.keys(includeStatus).filter(function(key) {
      return includeStatus[key];
    })
    .map(function(key) {
      return key.toLowerCase().replace('include', '');
    });

    var mcoCsvData = [];

    var weeklyStatementEmailData = {
      fromDate: fromDate,
      toDate: toDate,
      statements: []
    };

    // variables
    var mcoWithBookings;
    var mcoReconcile = [];
    var mcoInvoices = [];
    var mcoAccountAdjustmentIds = [];
    var mcoBpaIds = []; // adjustments that are not connected to this MCO's booking
    var bookingIds = [];
    var bookingAccounting = server.models.bookingAccounting;

    getGlobalSettings()
      .then(function(time) {

        // we need to check if statementsStartTime exist and compare it with fromDate
        if (time) {
          var statementsStartTime = new Date(time.value);

          if ((!fromDate) || (statementsStartTime.getTime() > fromDate.getTime())) {
            fromDate = statementsStartTime;
          }
        }

        // setting date range of bookings that user want
        var dateRange = {bookingCompletedDts: {between: [fromDate, toDate]}};

        // create mysql conditional for bookings
        var where = {
          and: [dateRange, {isDispute: 0}, {paid: 0},
            {bookingStatus: {neq: 'cancelled'}}, {or: [
              {driverNotificationStatus: 'pob_confirmed_3'},
              {driverNotificationStatus: 'pob_confirmed_4'},
              {driverNotificationStatus: 'passenger_not_show_4'},
              {driverNotificationStatus: 'other_4'}]}]
        };

        // create mysql conditional for mco-s
        var whereForMco = {
          and: [
            {accountType: {neq: 'test'}}
          ]
        };

        // add exclude mco-s to mysql conditional for mco-s
        if (excludeMco) {
          for (var i = 0; i < excludeMco.length; i++) {
            whereForMco.and.push({mcoId: {neq: excludeMco[i]}});
          }
        }

        if (settleSingleMCO) {
          var mcos = [];
          for (var i = 0; i < settleSingleMCO.length; i++) {
            mcos.push({mcoId: settleSingleMCO[i]});
          }
          whereForMco.and.push({or: mcos});
        }

        if (includeFilter.length) {
          whereForMco.and.push({
            mcoStatus: {
              inq: includeFilter
            }
          });
        }

        // include all tables from db we need
        var include = ['address', {
          relation: 'bookings',
          scope: {
            primaryKey: 'mcoId',
            foreignKey: 'mcoId',
            where: where,
            include: ['bookingAccounting', 'bookingPriceAdjustments']
          }
        }, {
          relation: 'mcoAccountAdjustments',
          scope: {
            where: {created: {between: [fromDate, toDate]}, invoiceId: null}
          }
        }];

        // get data from getMcos promise
        return getMcos(whereForMco, include);
      })
      .then(function(mcos) {
        var m = JSON.stringify(mcos);
        mcoWithBookings = JSON.parse(m);

        // ticket api-1571
        var mcoIds = [];
        for (var i = 0; i < mcoWithBookings.length; i++) {
          if (mcoWithBookings[i].bookings && mcoWithBookings[i].bookings.length != 0) {
            mcoIds.push(mcoWithBookings[i].mcoId);
          }
        }

        var bookingPriceAdjustmentsQueryWhere = {
          and: [{ mcoId: {inq: mcoIds}}, {deleted: 0}, {paid: 0}]
        };

        var bookingPriceAdjustmentsQueryInclude = [
          { relation: 'booking',
            scope: {
              primaryKey: 'booking_linktext',
              foreignKey: 'booking_linktext',
              where: { bookingCompletedDts: { between: [fromDate, toDate]}}
            }
          }
        ];

        return getBookingPriceAdjustments(bookingPriceAdjustmentsQueryWhere, bookingPriceAdjustmentsQueryInclude);
      }).then(function(bpaResult) {
        var results = JSON.stringify(bpaResult);
        var bpaParsedResult = JSON.parse(results);
        var bpaFinalResult = bpaParsedResult.filter(function(bpa) {
          return bpa.booking && !!bpa.booking.bookingCompletedDts;
        })
        .filter(function(bpa) {
          return bpa.booking.mcoId !== bpa.mcoId;
        });

        // for statement for mco
        // we get back mco-s with and without bookings; we only need mco-s with bookings
        // because of that we need to filter mco-s with if statement
        //START FOR i___________________________________________________________________________________________________
        for (var i = 0; i < mcoWithBookings.length; i++) {
          if (mcoWithBookings[i].bookings && mcoWithBookings[i].bookings.length != 0) {
            var tmpInvoiceId = generateId.generateInvoiceId(mcoWithBookings[i].mcoId);

            var tmp = {
              totalFares: null,
              minicabitCommission: null,
              vatOnCommission: null,
              fines: null,
              topUps: null,
              totalAccountAdjustments: null,
              minicabitNetCommission: null,
              totalPhoPay: null
            };

            //____________________ API-1571___________________________________________________________________________
            bpaFinalResult.filter(function(bpa) {
              return bpa.mcoId === mcoWithBookings[i].mcoId;
            }).forEach(function(bpa) {
              if (bpa.amount < 0) {
                tmp.fines += parseFloat(bpa.amount);
              } else {
                tmp.topUps += parseFloat(bpa.amount);
              }
              tmp.totalPhoPay += parseFloat(bpa.amount);
              var tmpMcoReconcile = {
                bookingId: bpa.booking.bookingId,
                bookingLinktext: bpa.bookingLinktext,
                mcoId: bpa.mcoId,
                invoiceId: tmpInvoiceId,
                phoPay: 0,
                topUps: bpa.amount > 0 ? bpa.amount : 0,
                fines: bpa.amount < 0 ? bpa.amount : 0,
                totalDue: bpa.amount,
                paymentType: null
              };
              mcoBpaIds.push({ id: bpa.id });
              mcoReconcile.push(tmpMcoReconcile);
            });
            // for statement for bookings
            // when we find mco with bookings, we need to loop through each booking
            //START FOR j_______________________________________________________________________________________________
            for (var j = 0; j < mcoWithBookings[i].bookings.length; j++) {
              var tmpTopUps = null;
              var tmpFines = null;
              var tmpPhoPay = null;
              var tmpPaymentType = null;
              var tmpTotalDue = null;

              // for calculating top ups and fines from bookingAdjustments
              //________________________________________________________________________________________________________
              if (mcoWithBookings[i].bookings[j].bookingPriceAdjustments &&
                mcoWithBookings[i].bookings[j].bookingPriceAdjustments.length != 0) {
                for (var k = 0; k < mcoWithBookings[i].bookings[j].bookingPriceAdjustments.length; k ++) {
                  var bpa = mcoWithBookings[i].bookings[j].bookingPriceAdjustments[k];
                  if (bpa.mcoId == mcoWithBookings[i].mcoId) {
                    if (bpa.amount > 0) {
                      tmpTopUps += parseFloat(bpa.amount);
                    } else {
                      tmpFines += parseFloat(bpa.amount);
                    }
                  }
                  mcoBpaIds.push({ id: bpa.id });
                }
              }
              //________________________________________________________________________________________________________

              // check for booking accounting table and make calculations
              //________________________________________________________________________________________________________
              if (mcoWithBookings[i].bookings[j].bookingAccounting) {
                tmp.totalFares += parseFloat(mcoWithBookings[i].bookings[j].bookingAccounting.tripPrice);
                tmp.minicabitCommission += parseFloat(mcoWithBookings[i].bookings[j].bookingAccounting.commission);
                tmp.minicabitNetCommission +=
                  parseFloat(mcoWithBookings[i].bookings[j].bookingAccounting.netCommission);

                if (mcoWithBookings[i].bookings[j].bookingAccounting.vatOnCommission) {
                  tmp.vatOnCommission += parseFloat(mcoWithBookings[i].bookings[j].bookingAccounting.vatOnCommission);
                } else {
                  tmp.vatOnCommission += parseFloat(mcoWithBookings[i].bookings[j].bookingAccounting.tripPrice *
                    (mcoWithBookings[i].bookings[j].bookingAccounting.commissionRate *
                    mcoWithBookings[i].bookings[j].bookingAccounting.vatRate) / 100);
                }

                tmpPaymentType = mcoWithBookings[i].bookings[j].bookingAccounting.paymentType;

                if (tmpPaymentType && tmpPaymentType.toUpperCase() === 'CASH') {
                  tmpPhoPay = (parseFloat(mcoWithBookings[i].bookings[j].bookingAccounting.income) * (-1));
                } else {
                  tmpPhoPay = parseFloat(mcoWithBookings[i].bookings[j].bookingAccounting.phoPay);
                }
              // if booking accounting table is missing we need to insert data into
              } else {
                bookingAccounting.insertInto(mcoWithBookings[i].bookings[j].vendortxcode, function(err, result) {
                  if (err) {
                    logger.error('Error with inserting bookingAccounting');
                  } else {
                    logger.debug('Data inserted into bookingAccounting ' + result);
                  }
                });
              }
              //________________________________________________________________________________________________________
              tmp.fines += tmpFines;
              tmp.topUps += tmpTopUps;
              tmpTotalDue = tmpPhoPay + tmpTopUps + tmpFines;
              tmp.totalPhoPay += tmpTotalDue;

              // temporary object for mco reconcile table
              var tmpDataReconcile = {
                bookingId: mcoWithBookings[i].bookings[j].bookingId,
                bookingLinktext: mcoWithBookings[i].bookings[j].bookingLinktext,
                mcoId: mcoWithBookings[i].bookings[j].mcoId,
                invoiceId: tmpInvoiceId,
                phoPay: tmpPhoPay,
                topUps: tmpTopUps,
                fines: tmpFines,
                totalDue: tmpTotalDue,
                paymentType: tmpPaymentType
              };

              // save object for later
              mcoReconcile.push(tmpDataReconcile);
              bookingIds.push({
                mcoId: mcoWithBookings[i].bookings[j].mcoId,
                bookingLinktext: mcoWithBookings[i].bookings[j].bookingLinktext
              });
            }
            //END FOR j_________________________________________________________________________________________________

            // check if we have mcoAccountAdjustments data
            //__________________________________________________________________________________________________________
            if (mcoWithBookings[i].mcoAccountAdjustments && mcoWithBookings[i].mcoAccountAdjustments.length != 0) {
              var tmpMcoAccountAdjustmentIds = [];
              for (var j = 0; j < mcoWithBookings[i].mcoAccountAdjustments.length; j++) {
                // if (!mcoWithBookings[i].mcoAccountAdjustments[j].invoiceId) // filtering in query above
                {
                  // active/unpaid/unsettled account adjustments have invoice_id = null
                  tmp.totalAccountAdjustments += parseFloat(mcoWithBookings[i].mcoAccountAdjustments[j].amount);
                  tmpMcoAccountAdjustmentIds.push({
                    mcoAccountAdjustmentsId: mcoWithBookings[i].mcoAccountAdjustments[j].mcoAccountAdjustmentsId
                  });
                }
              }
              if (tmpMcoAccountAdjustmentIds.length) {
                mcoAccountAdjustmentIds.push({
                  mcoAccountAdjustmentWhere: {or: tmpMcoAccountAdjustmentIds},
                  invoiceId: tmpInvoiceId
                });
              }
            }
            //__________________________________________________________________________________________________________

            // temporary object for mco invoice table
            var tmpDataInvoice = {
              invoiceId: tmpInvoiceId,
              mcoId: mcoWithBookings[i].mcoId,
              invoiceTimestamp: new Date,
              totalFares: tmp.totalFares,
              minicabitCommission: tmp.minicabitCommission,
              vatOnCommission: tmp.vatOnCommission,
              fines: tmp.fines,
              topups: tmp.topUps,
              totalAccountAdjustments: tmp.totalAccountAdjustments,
              total: tmp.totalFares + tmp.topUps + tmp.fines + tmp.totalAccountAdjustments,
              totalPhoPay: tmp.totalPhoPay + tmp.totalAccountAdjustments,
              minicabitNetCommission: tmp.minicabitNetCommission
            };

            // remove all negative values if excludeNegativeValues is set to 1
            //__________________________________________________________________________________________________________
            if (excludeNegativeValues === 1 && tmpDataInvoice.totalPhoPay < 0) {
              _.remove(mcoReconcile, {mcoId: mcoWithBookings[i].mcoId});
              _.remove(bookingIds, {mcoId: mcoWithBookings[i].mcoId});
              _.remove(mcoAccountAdjustmentIds, {invoiceId: tmpInvoiceId});
            } else {
              mcoInvoices.push(tmpDataInvoice);

              var tmpMcoCsvData = {
                sortCode: mcoWithBookings[i].bankSortcode,
                bankAccountName: mcoWithBookings[i].bankAccountName,
                bankAccountNumber: mcoWithBookings[i].bankAccountNumber,
                valueOutstanding: tmpDataInvoice.totalPhoPay.toFixed(2),
                date: 'JOBS TO ' + toDate,
                bankCode: 99,
                email: mcoWithBookings[i].officeEmail,
                accountStatus: mcoWithBookings[i].mcoStatus,
                mcoId: mcoWithBookings[i].mcoId,
                registeredName: mcoWithBookings[i].registeredName,
                registeredAddressTown: mcoWithBookings[i].address.town,
                registeredAddressPostcode: mcoWithBookings[i].address.postcode,
                contactPerson: mcoWithBookings[i].contactPersonDay
              };
              mcoCsvData.push(tmpMcoCsvData);

              var tmpWeeklyStatement = {
                mcoOfficeEmail: mcoWithBookings[i].officeEmail,
                mcoRegisteredName: mcoWithBookings[i].registeredName,
                totalPhoPay: tmpDataInvoice.totalPhoPay,
                vatOnCommission: tmpDataInvoice.vatOnCommission
              };
              weeklyStatementEmailData.statements.push(tmpWeeklyStatement);
            }
            // console.log(JSON.stringify({mcoAccountAdjustmentIds: mcoAccountAdjustmentIds,
            //   'tmp.totalAccountAdjustments': tmp.totalAccountAdjustments,
            //   tmp: tmp,
            //   tmpDataInvoice: tmpDataInvoice
            // }));
            //__________________________________________________________________________________________________________
          }
        }
        //END FOR i_____________________________________________________________________________________________________
        var bookingWhere = {or: []};
        for (var i = 0; i < bookingIds.length; i++) {
          bookingWhere.or.push({and: [
            {mcoId: bookingIds[i].mcoId},
            {bookingLinktext: bookingIds[i].bookingLinktext}
          ]});
        }
        if (preview) {
          return true;
        } else {
          if (markAsPaid) {
            var mcoPayrun = {
              paymentTimestamp: null,
              dateFrom: fromDate,
              dateTo: toDate
            };

            return transactionPromise(mcoPayrun, mcoReconcile, mcoInvoices, bookingWhere, mcoAccountAdjustmentIds,
              mcoBpaIds);
          } else {
            return true;
          }
        }
      })
      .then(function(mcoPayrunId) {
        if (!sendEmail) {
          return [mcoPayrunId];
        } else {
          return Promise.all([
            mcoPayrunId,
            new Promise(function(resolve, reject) {
              nm.sendWeeklyStatement(weeklyStatementEmailData, function(err, result) {
                resolve(result);
              });
            })
          ]);
        }
      })
      .then(function(responses) {
        var mcoPayrunId = responses[0];
        console.log('mcoPayrunId');
        console.log(mcoPayrunId);
        var fields = ['sortCode', 'bankAccountName', 'bankAccountNumber', 'valueOutstanding', 'date', 'email',
          'accountStatus', 'mcoId', 'registeredName', 'registeredAddressTown',
          'registeredAddressPostcode', 'contactPerson'];

        if (mcoCsvData && mcoCsvData.length !== 0) {
          var csv = json2csv({ data: mcoCsvData, hasCSVColumnTitle: false});
          // if (preview)
          //   return writeCsvFile(csv, fields, 'preview/');
          // else
          //   return writeCsvFile(csv, fields, '');
          return [csv, mcoPayrunId];
        } else {
          return ['No bookings', mcoPayrunId];
        }
      })
      .then(function(results) {
        var result = results[0];
        var mcoPayrunId = results[1];
        console.log('results', results);
        var payrunId = !isNaN(parseInt(mcoPayrunId, 10)) ? mcoPayrunId : 'Preview';
        callback(null, result, 'text/csv', 'attachment; filename="Payrun ID ' + payrunId + '.csv"');
      })
      .catch(function(err) {
        return callback(err);
      });
  };

  Statement.returnHistory = function(mcoId, limit, offset, callback) {
    var where = {
      mcoId: mcoId,
    };

    var bookingWhere = {
      mcoId: mcoId
    };

    var include = [
      {
        relation: 'mcoReconcile',
        scope: {
          include: {
            relation: 'booking',
            scope: {
              where: bookingWhere,
              include: ['customer', 'pickupAddress', 'destinationAddress']
            }
          }
        }
      },
      'mcoPayrun',
      'mcoAccountAdjustments'
    ];

    getInvoices(where, include, limit, offset)
      .then(function(result) {
        logger.debug('Getting data for history bookings');
        return callback(null, result);
      })
      .catch(function(err) {
        logger.debug('Error getting data for history bookings ' + err);
        return callback(err);
      });
  };

  loopback.remoteMethod(
    Statement.returnPendingBooking, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {arg: 'limit', type: 'number', required: false, description: 'limit'},
        {arg: 'offset', type: 'number', required: false, description: 'offset'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/pendingBookings/:mcoId', verb: 'get', status: 200, errorStatus: 400},
      description: 'get pending bookings'
    }
  );

  loopback.remoteMethod(
    Statement.returnDisputedBooking, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {arg: 'limit', type: 'number', required: false, description: 'limit'},
        {arg: 'offset', type: 'number', required: false, description: 'offset'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/disputedBookings/:mcoId', verb: 'get', status: 200, errorStatus: 400},
      description: 'get disputed bookings'
    }
  );

  loopback.remoteMethod(
    Statement.returnOutstandingBooking, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {arg: 'fromDate', type: 'string', description: 'from date'},
        {arg: 'toDate', type: 'string', description: 'to date'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/outstandingBookings/:mcoId', verb: 'get', status: 200, errorStatus: 400},
      description: 'get outstanding bookings'
    }
  );

  loopback.remoteMethod(
    Statement.payRun, {
      accepts: [
        {
          arg: 'data',
          type: 'object',
          required: true,
          description: 'payRun data',
          'http': {source: 'body'}
        }
      ],
      returns: [
        { arg: 'body', type: 'file', root: true },
        { arg: 'Content-Type', type: 'string', http: { target: 'header' } },
        { arg: 'Content-Disposition', type: 'string', http: { target: 'header' } }
      ],
      http: {path: '/payRun', verb: 'post', status: 200, errorStatus: 400},
      description: 'make payRun'
    }
  );

  loopback.remoteMethod(
    Statement.returnHistory, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {arg: 'limit', type: 'number', required: false, description: 'limit'},
        {arg: 'offset', type: 'number', required: false, description: 'offset'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/history/:mcoId', verb: 'get', status: 200, errorStatus: 400},
      description: 'get history of all the bookings and payments that have been settled ' +
      'in the history of the booking, paginated by invoices.'
    }
  );
};

