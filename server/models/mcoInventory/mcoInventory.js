'use strict';

var _ = require('lodash');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');

module.exports = function(McoInventory) {


  McoInventory.getInventory = function(mcoId, callback) {

    var mcoInventory = server.models.mcoInventory;

    var inventory = [];

    var carTypes = ['4seater', '4eseater', '6seater', '7seater', '8seater', '9seater',
      '14seater', '16seater'];

    mcoInventory.find({where: {mcoId: mcoId}}, function(err, result) {
      if (err) {
        logger.error('Error getting mco Inventory data', mcoId, err);
        callback(err, null);
      } else {
        _.each(carTypes, function(ct) {
          var obCarSize = {};

          var ob = {};
          _.each(result, function(r) {
            var m = r['inventoryDay'];
            ob[m] = r[ct + 'Count'];
          });

          obCarSize[ct] = ob;

          inventory.push(obCarSize);
        });
        logger.debug('Inventory found', inventory);
        callback(null, inventory);
      }
    });
  };

  McoInventory.updateInventory = function(mcoId, carType, inventoryDay, numberOfCars, callback) {

    var mcoInventory = server.models.mcoInventory;

    var inventoryDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    if (_.indexOf(inventoryDays, inventoryDay) === -1) {
      return callback('Inventory day does not match valid inventory days');
    }

    mcoInventory.findOne({where: {
      mcoId: mcoId,
      inventoryDay: inventoryDay
    }}, function(err, result) {
      if (!result) {
        callback('Inventory data for mcoId and inventoryDay not exist');
      } else {
        result.updateAttribute(carType + 'Count', numberOfCars, function(err, inventory) {
          if (err) {
            return callback(err);
          } else {
            return callback(null, inventory);
          }
        });
      }
    });
  };

  loopback.remoteMethod(
    McoInventory.getInventory, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'}
      ],
      returns: {arg: 'mcoInventory', type: 'object'},
      http: {path: '/getInventory/:mcoId', verb: 'get', status: 200, errorStatus: 500},
      description: 'Get inventory pricing for MCO'
    }
  );

  loopback.remoteMethod(
    McoInventory.updateInventory, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {arg: 'carType', type: 'string', required: true, description: 'car type'},
        {arg: 'inventoryDay', type: 'string', required: true, description: 'inventory date (ex: Monday, Tuesday,...)'},
        {arg: 'numberOfCars', type: 'number', required: true, description: 'number of cars for specified mcoId, carSize and inventoryDate'}
      ],
      returns: {arg: 'mcoInventory', type: 'object'},
      http: {path: '/updateInventory', verb: 'post', status: 200, errorStatus: 500},
      description: 'Update inventory for mcoId, inventoryDate, carSize'
    }
  );

};
