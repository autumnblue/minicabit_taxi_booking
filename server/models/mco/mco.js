'use strict';
var _ = require('lodash');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');
var utils = require('loopback/lib/utils.js');

module.exports = function(Mco) {


  Mco.search = function(query, limit, callback) {
    logger.debug('Serching mcos', query);

    var mcoModel = server.models.mco;

    mcoModel.find({
      where: {
        or: [
          { registeredName: { like: '%' + query + '%' } },
          { mcoId: { like: query + '%' } },
        ]
      },
      limit: limit,
      fields: {mcoId: true, registeredName: true}
    },
    function(err, result) {
      if (err) {
        logger.error('Error searching mcos', err);
        callback(err);
      } else {
        if (result.length === 0) {
          logger.debug('There is no MCO ID matching the search');
          callback(null, 'There is no MCO ID matching that search');
        } else {
          logger.debug('Mco search results', result);
          callback(null, result);
        }
      }
    });
  };

  loopback.remoteMethod(
    Mco.search, {
      accepts: [
        {arg: 'query', type: 'string', required: true, description: 'mco id'},
        {arg: 'limit', type: 'number', required: false, description: 'limit'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/search', verb: 'GET', status: 200, errorStatus: 400},
      description: 'Searching mco with filter'
    }
  );
};
