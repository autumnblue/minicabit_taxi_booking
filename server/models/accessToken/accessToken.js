'use strict';

module.exports = function(AccessToken) {

  AccessToken.observe('before save', function updateTimestamp(ctx, next) {
    var modelInstance = ctx.instance || ctx.data;

    if (!modelInstance.created) {

      modelInstance.created = new Date();
    }
    next();
  });
};
