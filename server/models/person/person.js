'use strict';

var _ = require('lodash');
var async = require('async');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');
var base64 = require('base-64');
var utils = require('loopback/lib/utils.js');

module.exports = function(Person) {

  function getPasswordHash(password) {
    //return require('crypto').createHash('md5').update(password + 'minicabit_tibacinim').digest("hex");
    //Temporary base64 pass encoding. Should be replaced with real password hashing ASAP.
    return base64.encode(password);
  }

  function passwordCompare(plain, password, cb) {
    if (getPasswordHash(plain) === password) {
      cb(null, true);
    } else {
      cb(null, false);
    }
  }

  Person.hashPassword = function(plain) {
    return getPasswordHash(plain);
  };

  Person.observe('before save', function updateTimestamp(ctx, next) {
    var modelInstance = ctx.instance || ctx.data;

    if (!modelInstance.created) {
      modelInstance.created = new Date();
    }
    if (modelInstance) {
      modelInstance.modified = new Date();
    }
    next();
  });

  Person.mcoRegister = function(data, cb) {
    logger.debug('Person.mcoRegister', 'data', data);
    var mcoModel = server.models.mco;
    var roleMapping = server.models.roleMapping;

    //personData should only have the correct fields
    var personData = _.pick(data, [
      'username',
      'password',
      'email'
    ]);

    var mcoData = _.pick(data, [
      'username',
      'password',
      'email',
      'registeredName',
      'dispatchSystem',
      'tradingName',
      'website',
      'operatorLicenceNumber',
      'licenceExpiryDate',
      'localAuthority',
      'fleetLabel',
      'fleetSize',
      'officeContactTel',
      'mcoMobileNumber',
      'contactPersonDay',
      'contactPersonNight',
      'howHeardAboutus'
    ]);

    mcoData['password'] = getPasswordHash(personData['password']);
    personData['email'] = personData['email'];

    Person.beginTransaction({
      isolationLevel: Person.Transaction.READ_COMMITTED,
      timeout: 15000
    }, function(err, tx) {
      async.waterfall([
        function(callback) {
          Person.create(personData,  { transaction: tx }, function(err, result) {
            if (err) {
              logger.error('Error saving MCO to Person table: ' +  err);
              callback(err);
            } else {
              callback(err, result);
            }
          });
        },
        function(personResult, callback) {
          var roleMappingData = {
            principalType: 'ROLE',
            principalId: personResult.id,
            roleId: 3
          };
          roleMapping.create(roleMappingData,  { transaction: tx }, function(err, result) {
            if (err) {
              logger.error('Error saving User to RoleMapping table: ' + err);
              callback(err);
            } else {
              callback(err, personResult);
            }
          });
        },
        function(personResult, callback) {
          mcoModel.create(mcoData,  { transaction: tx }, function(err, result) {
            if (err) {
              logger.error('Error saving MCO to Mco table: ' + err);
              callback(err);
            } else {
              callback(err, {mco: result, person: personResult});
            }
          });
        },
        function(result, callback) {
          result.person.updateAttribute('mcoId', result.mco.mcoId, { transaction: tx }, function(err, result) {
            if (err) {
              logger.error('Error updating mcoId: ' + err);
              callback(err);
            } else {
              callback(err, result);
            }
          });
        }
      ], function finish(finErr, result) {
        if (finErr) {
          logger.error('Error rollbacking transaction for saving MCO: ' + finErr);
          tx.rollback(function(err) {
            if (err) {
              logger.error('Error rollbacking transaction for saving MCO: ' + err);
              return cb(err);
            } else {
              return cb(finErr);
            }
          });
        } else {
          tx.commit(function(err) {
            if (err) {
              logger.error('Error when commiting transaction for saving MCO: ' + err);
              return cb(err);
            } else {
              return cb(err, result);
            }
          });
        }
      });
    });
  };

  Person.prototype.hasPassword = function(plain, fn) {
    fn = fn || utils.createPromiseCallback();
    if (this.password && plain) {
      passwordCompare(plain, this.password, function(err, isMatch) {
        if (err) return fn(err);
        fn(null, isMatch);
      });
    } else {
      fn(null, false);
    }
    return fn.promise;
  };

  loopback.remoteMethod(
    Person.mcoRegister,
    {
      accepts: [
        {arg: 'data', type: 'object', required: true, description: 'MCO registration data', 'http': { source: 'body' }}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/mco/register', verb: 'post'},
      description: 'Registration of MCO'
    }
  );
};
