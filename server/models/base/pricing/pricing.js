'use strict';

var _ = require('lodash');
var loopback = require('loopback');
var server = require('./../../../server');
var logger = require('./../../../logger');
var utils = require('loopback/lib/utils.js');

/*

 GET pricing/mileage/calculator/:mcoId - get mileage pricing for mco
 PUT pricing/mileage/rate/:mcoId - update MCO graduated pricing tier rate
 GET pricing/mileage/:mcoId - get graduated pricing fro MCO
 PUT pricing/mileage/ceiling/:mcoId - update graduated pricing tier ceiling for MCO
 PUT pricing/general/uplift/:mcoId - set uplift pricing fro MCO
 GET pricing/general/uplift/:mcoId - get uplift pricing for MCO
 POST pricing/headline - Insert headline pricing for MCO
 GET pricing/postcode/cheapestPrice - when a MCO is searching, the cheapest price for that particular postcode will be returned.
 */

module.exports = function(Pricing) {

  Pricing.returnMileagePricing = function(mcoId, mileage, callback) {
    var graduatedPricing = server.models.graduatedPricing;
    var mco = server.models.mco;
    var tmpPrice;

    graduatedPricing.find({where: {mcoId: mcoId, numPassengers: 4}}, function(err, resultPricing) {
      if (err) {
        logger.error('Error retrieving data from graduated pricing table for mcoId [ ' + mcoId + ' ] ' + err);
        return callback(err, null);
      } else if (!resultPricing[0]) {
        return callback('No result for mcoId [ ' + mcoId + ' ]', null);
      } else {
        // lets calculate mileage pricing
        if (mileage > resultPricing[0].tier5Ceiling) {
          return callback(null, 'This mileage is too far out of the MCOs mileage range');
        } else if (mileage > resultPricing[0].tier4Ceiling && mileage <= resultPricing[0].tier5Ceiling) {
          tmpPrice = ((resultPricing[0].tier1Ceiling * resultPricing[0].tier1Rate) + (
          (resultPricing[0].tier2Ceiling - resultPricing[0].tier1Ceiling) * resultPricing[0].tier2Rate) + (
          (resultPricing[0].tier3Ceiling - resultPricing[0].tier2Ceiling) * resultPricing[0].tier3Rate) + (
          (resultPricing[0].tier4Ceiling - resultPricing[0].tier3Ceiling) * resultPricing[0].tier4Rate) + (
          (mileage - resultPricing[0].tier4Ceiling) * resultPricing[0].tier5Rate));

        } else if (mileage > resultPricing[0].tier3Ceiling && mileage <= resultPricing[0].tier4Ceiling) {
          tmpPrice = ((resultPricing[0].tier1Ceiling * resultPricing[0].tier1Rate) + (
          (resultPricing[0].tier2Ceiling - resultPricing[0].tier1Ceiling) * resultPricing[0].tier2Rate) + (
          (resultPricing[0].tier3Ceiling - resultPricing[0].tier2Ceiling) * resultPricing[0].tier3Rate) + (
          (mileage - resultPricing[0].tier3Ceiling) * resultPricing[0].tier4Rate));

        } else if (mileage > resultPricing[0].tier2Ceiling && mileage <= resultPricing[0].tier3Ceiling) {
          tmpPrice = ((resultPricing[0].tier1Ceiling * resultPricing[0].tier1Rate) + (
          (resultPricing[0].tier2Ceiling - resultPricing[0].tier1Ceiling) * resultPricing[0].tier2Rate) + (
          (mileage - resultPricing[0].tier2Ceiling) * resultPricing[0].tier3Rate));

        } else if (mileage > resultPricing[0].tier1Ceiling && mileage <= resultPricing[0].tier2Ceiling) {
          tmpPrice = ((resultPricing[0].tier1Ceiling * resultPricing[0].tier1Rate) + (
          (mileage - resultPricing[0].tier1Ceiling) * resultPricing[0].tier2Rate));

        } else if (mileage <= resultPricing[0].tier1Ceiling) {
          tmpPrice = (mileage * resultPricing[0].tier1Rate);
        }

        mco.find({where: {mcoId: mcoId}}, function(err, mcoResult) {
          if (err) {
            logger.error('Error retrieving data from mco table for mcoId [ ' + mcoId + ' ] ' + err);
            return callback(err, null);
          } else if (!mcoResult[0]) {
            return callback('No result for mcoId [ ' + mcoId + ' ]', null);
          } else {
            return callback(null, calculateMileagePricing(tmpPrice, mcoResult[0]));
          }
        });
      }
    });
  };

  loopback.remoteMethod(
    Pricing.returnMileagePricing, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {arg: 'mileage', type: 'number', required: true, description: 'mileage'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/mileage/calculator/:mcoId', verb: 'get', status: 200, errorStatus: 400},
      description: 'get mileage pricing for MCO'
    }
  );


  Pricing.updateTierRate = function(mcoId, data, callback) {
    var listOfPromises = [];
    var graduatedPricing = server.models.graduatedPricing;

    getMcoPriceMultiply(mcoId)
      .then(function(mcoData) {
        var prices = calculateGraduatedPricing(data, mcoData);

        _.forEach(prices, function(element, el) {

          var numPassengers;
          if (el === '1-4') {
            numPassengers = 4;
          } else if (el === '1-4Estate') {
            numPassengers = 100; // TODO change this when will get info of row in DB
          } else if (el === '5-6') {
            numPassengers = 6;
          } else if (el === '7') {
            numPassengers = 7;
          } else if (el === '8') {
            numPassengers = 8;
          } else if (el === '9') {
            numPassengers = 9;
          } else if (el === '10-14') {
            numPassengers = 14;
          } else if (el === '15-16') {
            numPassengers = 16;
          }

          var p = new Promise(function (resolve, reject) {
            graduatedPricing.updateAll({mcoId: mcoId, numPassengers: numPassengers}, element, function (err, result) {
              if (err) {
                logger.error('Error inserting tier rate into graduated pricing table [ ' + err + ' ]');
                reject(err);
              } else {
                logger.debug('Updating graduated pricing table with tier rate [ ' + result + ' ]');
                resolve(result);
              }
            });
          });

          listOfPromises.push(p);
        });
        return Promise.all(listOfPromises);
      })
      .then(function() {
        Pricing.getGraduatedPricing(mcoId, function(err, result) {
          if (err) {
            logger.error('Error getting data from graduated pricing table [ ' + err + ' ]');
            throw new Error('Error getting data from graduated pricing table ' + err);
          } else {
            logger.debug('Getting data from graduated pricing table [ ' + result + ' ]');
            callback(err, result);
          }
        });
      })
      .catch(function(err) {
        logger.error('Error updateTierRate [ ' + err + ' ]');
        callback(err, null);
      });
  };

  loopback.remoteMethod(
    Pricing.updateTierRate, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {
          arg: 'data',
          type: 'object',
          required: true,
          description: 'tier rate data',
          'http': {source: 'body'}
        }
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/graduate/rate/:mcoId', verb: 'put', status: 201},
      description: 'update MCO graduated pricing tier rate'
    }
  );


  Pricing.updateTierCeiling = function(mcoId, data, callback) {
    var graduatedPricing = server.models.graduatedPricing;
    graduatedPricing.updateAll({mcoId: mcoId}, data, function(err) {
      if (err) {
        logger.error('Error updating graduated pricing for mcoId [ ' + mcoId + ' ] ' + err);
        callback(err, null);
      } else {
        Pricing.getGraduatedPricing(mcoId, function(err, result) {
          if (err) {
            logger.error('Error getting data from graduated pricing table [ ' + err + ' ]');
            callback(err, null);
          } else {
            logger.debug('Getting data from graduated pricing table [ ' + result + ' ]');
            callback(err, result);
          }
        });
      }
    });
  };

  loopback.remoteMethod(
    Pricing.updateTierCeiling, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {
          arg: 'data',
          type: 'object',
          required: true,
          description: 'tier veiling data',
          'http': {source: 'body'}
        }
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/mileage/ceiling/:mcoId', verb: 'put', status: 201},
      description: 'update graduated pricing tier ceiling for MCO'
    }
  );


  Pricing.updateUplift = function(mcoId, data, callback) {
    var pricingMco = server.models.pricingMco;
    var updateDb = prepareData(data);

    if (updateDb.err) {
      logger.error('Error updating uplift pricing for mcoId [ ' + mcoId + ' ] ' + updateDb.err);
      callback(updateDb.err, null);
    } else {
      pricingMco.updateAll({mcoId: mcoId}, updateDb.data, function(err) {
        if (err) {
          logger.error('Error updating uplift pricing for mcoId [ ' + mcoId + ' ] ' + err);
          callback(err, null);
        } else {
          logger.debug('Updating uplift pricing success. McoId [ ' + mcoId + ' ]');
          Pricing.getUpliftPricing(mcoId, function(err, result) {
            if (err) {
              callback(err, null);
            } else {
              callback(null, result);
            }
          });
        }
      });
    }
  };

  loopback.remoteMethod(
    Pricing.updateUplift, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {
          arg: 'data',
          type: 'object',
          required: true,
          description: 'pricing data',
          'http': {source: 'body'}
        }
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/general/uplift/:mcoId', verb: 'put', status: 201, errorStatus: 400},
      description: 'set uplift pricing fro MCO'
    }
  );

  Pricing.getGraduatedPricing = function(mcoId, callback) {
    var graduatedPricing = server.models.graduatedPricing;
    graduatedPricing.find({where: {mcoId: mcoId}}, function(err, result) {
      if (err) {
        logger.error('Error retrieving data from graduated pricing table for mcoId [ ' + mcoId + ' ] ' + err);
        callback(err, null);
      } else {
        logger.debug('Getting graduated pricing for [ ' + mcoId + ' ] : [ ' + result + ' ]');
        callback(null, result);
      }
    });
  };

  loopback.remoteMethod(
    Pricing.getGraduatedPricing, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/mileage/:mcoId', verb: 'get'},
      description: 'get graduate pricing fro MCO'
    }
  );

  Pricing.getUpliftPricing = function(mcoId, callback) {
    var pricingMco = server.models.pricingMco;

    pricingMco.find({where: {mcoId: mcoId}}, function(err, result) {
      if (err) {
        logger.error('Error getting uplift pricing for [ ' + mcoId + ' ] ' + err);
        callback(err, null);
      } else {
        logger.debug('Getting uplift pricing for [ ' + mcoId + ' ] : [ ' + result + ' ]');
        callback(null, result[0]);
      }
    });
  };

  loopback.remoteMethod(
    Pricing.getUpliftPricing, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/general/uplift/:mcoId', verb: 'get'},
      description: 'get uplift pricing for MCO'
    }
  );

  Pricing.insertHeadlinePricing = function(data, callback) {
    var fn;
    var hp = server.models.headlinePricing;
    var pricingMco = server.models.pricingMco;


    var h = {
      mcoId: data['mcoId'],
      startPostcode: data['startPostcode'],
      startLatitude: data['startLatitude'],
      startLongitude: data['startLongitude'],
      startRadiusMiles: data['startRadiusMiles'],
      finishPostcode: data['finishPostcode'],
      finishLatitude: data['finishLatitude'],
      finishLongitude: data['finishLongitude'],
      finishRadiusMiles: data['finishRadiusMiles'],
      journeyPrice4Seater: data['journeyPrice4Seater'],
      returnJourney: data['returnJourney']
    };

    function insertPricingForCarType(carSize, headlinePrice, multipliers) {

      return new Promise(function(resolve, reject) {

        var multiplier = 1;
        var multiplierType = 1;

        if (carSize === 4) {
          multiplier = 1;
          multiplierType = 1;
        } else if (carSize === -4) {
          multiplier = multipliers['estateLuggageMultiplayer'];
          multiplierType = multipliers['estateLuggageMultiplayerType'];
        } else {
          multiplier = multipliers['multiplayer' + carSize];
          multiplierType = multipliers['multiplayer' + carSize];
        }

        logger.debug('mutiplier', carSize, multiplier);
        logger.debug('mutliplier type', carSize, multiplierType);

        var singlePrice = h.journeyPrice4Seater;

        //percent
        if (multiplierType === 1) {
          singlePrice = singlePrice * (1 + multiplier / 100);
        } else {
          singlePrice = singlePrice + multiplier;
        }

        headlinePrice.carSize = carSize;
        headlinePrice.singlePrice = singlePrice;
        headlinePrice.returnPrice = singlePrice;

        hp.create(headlinePrice, function(err, result) {
          if (err) {
            logger.error('Error inserting headline pricing', err);
            reject(err);
          } else {
            logger.debug('Headline pricing inserted', h);
            resolve();
          }
        });
      });
    };


    //get mco pricing data

    pricingMco.find({where: {mcoId: h.mcoId}}, function(err, result) {
      if (err) {
        logger.error('Error getting mco pricing data [ ' + h.mcoId + ' ] ' + err);
        callback(err, null);
      } else {

        var carSizes = [4, -4, 6, 7, 8, 9, 14, 16];

        fn = carSizes.map(function(c) {
          insertPricingForCarType(c, h, result[0]);
        });

        if (h.returnJourney === 1) {
          logger.debug('return journey');

          var hr = {
            mcoId: h.mcoId,
            startPostcode: h.finishPostcode,
            startLatitude: h.finishLatitude,
            startLongitude: h.finishLongitude,
            startRadiusMiles: h.finishRadiusMiles,
            finishPostcode: h.startPostcode,
            finishLatitude: h.startLatitude,
            finishLongitude: h.startLongitude,
            finishRadiusMiles: h.startRadiusMiles,
            journeyPrice4Seater: h.journeyPrice4Seater
          };

          var fnr = carSizes.map(function (c) {
            insertPricingForCarType(c, hr, result[0]);
          });

          fn = _.concat(fn, fnr);
        }

        Promise.all(fn)
          .then(function(data) {
            return callback(null, 'Headline pricing data inserted');
          })
          .catch(function(err) {
            return callback(err);
            logger.error('Error inserting headline pricing', err);
          });

      }
    });
  };

  loopback.remoteMethod(
    Pricing.insertHeadlinePricing, {
      accepts: [
        {
          arg: 'data', type: 'object', required: true, description: 'pricing data', 'http': {source: 'body'}
        }
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/headline/', verb: 'POST', status: 202, errorStatus: 400},
      description: 'Insert headline pricing for MCO'
    }
  );


  Pricing.getCheapestPostcodePrice = function(mcoId, pickupPostcode, dropoffPostcode, callback) {
    var mcoPostcodePricing = server.models.mcoPostcodePricing;

    mcoPostcodePricing.find({'where': {'mcoId': mcoId, 'pickupPostcode': pickupPostcode, 'dropoffPostcode': dropoffPostcode},
      'order': 'price14 DESC', 'limit': 1}, function(err, result) {
        if (err) {
          callback(err);
        } else  {
          callback(null, result);
        }
    });
  };

  loopback.remoteMethod(
    Pricing.getCheapestPostcodePrice, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {arg: 'pickupPostcode', type: 'string', required: true, description: 'pickup postcode'},
        {arg: 'dropoffPostcode', type: 'string', required: true, description: 'dropoff postcode'},
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/postcode/cheapestprice', verb: 'GET', status: 200, errorStatus: 400},
      description: 'Insert headline pricing for MCO'
    }
  );


  Pricing.insertPostcodePrice = function(mcoId, clusterId, pickupPostCode, dropoffPostcode, price, callback) {
    var mcoPostcodePricingModel = server.models.mcoPostcodePricing;
    var mcoModel = server.models.mco;

    var pc = {
      //id: null,
      mcoId: mcoId,
      clusterId: clusterId,
      pickupPostcode: pickupPostCode,
      dropoffPostcode: dropoffPostcode,
      price1_4: price,
      lastUpdate: Date.now()
    }

    mcoModel.findById(mcoId, function(err, mcoResult) {

      if (err) {
        callback(err);
      } else {

        if (mcoResult.multiplayerType6 === 1) {
          pc.price56 = price * (1 + mcoResult.multiplayer6 / 100);
        } else {
          pc.price56 = price + mcoResult.multiplayer6;
        }

        if (mcoResult.multiplayerType7 === 1) {
          pc.price7 = price + (1 + mcoResult.multiplayer7 / 100);
        } else {
          pc.price7 = price + mcoResult.multiplayer7;
        }

        if (mcoResult.multiplayerType8 === 1) {
          pc.price8 = price + (1 + mcoResult.multiplayer8 / 100);
        } else {
          pc.price8 = price + mcoResult.multiplayer8;
        }

        if (mcoResult.multiplayerType9 === 1) {
          pc.price9 = price + (1 + mcoResult.multiplayer9 / 100);
        } else {
          pc.price9 = price + mcoResult.multiplayer9;
        }

        if (mcoResult.multiplayerType14 === 1) {
          pc.price14 = price + (1 + mcoResult.multiplayer14 / 100);
        } else {
          pc.price14 = price + mcoResult.multiplayer14;
        }

        if (mcoResult.multiplayerType16 === 1) {
          pc.price16 = price + (1 + mcoResult.multiplayer16 / 100);
        } else {
          pc.price16 = price + mcoResult.multiplayer16;
        }

        logger.debug('Inserting postcode price', pc);

        mcoPostcodePricingModel.create(pc, function(err, result) {

          if (err) {
            logger.error('Error inserting postcode price', pc, err);
            callback(err);
          } else {
            logger.debug('Postcode price inserted', pc);
            return callback(null, result);
          }
        });
      }
    });
  };

  loopback.remoteMethod(
    Pricing.insertPostcodePrice, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'},
        {arg: 'clusterId', type: 'number', required: true, description: 'cluster id'},
        {arg: 'pickupPostcodes', type: 'string', required: true, description: 'pickup postcode'},
        {arg: 'dropoffPostcodes', type: 'string', required: true, description: 'dropoff postcode'},
        {arg: 'price', type: 'number', required: true, description: 'price for 1-4'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/postcode', verb: 'POST', status: 202, errorStatus: 400},
      description: 'Insert postcode pricing for MCO'
    }
  );

  Pricing.getPostCodePrice = function(mcoId, callback) {
    var mcoPostcodePricing = server.models.mcoPostcodePricing;

    mcoPostcodePricing.find({'where': {'mcoId': mcoId}}, function(err, result) {

      if (err) {
        logger.error('Error getting postcode pricing for mco', mcoId);
        return callback(err);
      } else {
        logger.debug('Returning postcode pricing for mco', mcoId);
        return callback(null, result);
      }
    });
  };


  loopback.remoteMethod(
    Pricing.getPostCodePrice, {
      accepts: [
        {arg: 'mcoId', type: 'number', required: true, description: 'mco id'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/postcode', verb: 'GET', status: 200, errorStatus: 400},
      description: 'Returns postcode pricings for MCO'
    }
  );

  function getMcoPriceMultiply(mcoId) {
    return new Promise(function(resolve, reject) {
      var mcoModel = server.models.pricingMco;
      mcoModel.find({where: {mcoId: mcoId}}, function(err, result) {
        if (err) {
          logger.error('Error getting mco price multiply for mcoId [ ' + mcoId + ' ] ' + err);
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  function prepareData(pricing) {
    var dataForDB = {};
    var err = null;
    _.forIn(pricing, function (element, el) {
      if (!element.multiply || !element.multiplyType)
        err = 'Invalid request data';
      else {
        if (el === '1-4Estate') {
          dataForDB.estateLuggageMultiplayer = element.multiply;
          dataForDB.estateLuggageMultiplayerType = element.multiplyType;
        } else if (el === '5-6') {
          dataForDB.multiplayer6 = element.multiply;
          dataForDB.multiplayerType6 = element.multiplyType;
        } else if (el === '7') {
          dataForDB.multiplayer7 = element.multiply;
          dataForDB.multiplayerType7 = element.multiplyType;
        } else if (el === '8') {
          dataForDB.multiplayer8 = element.multiply;
          dataForDB.multiplayerType8 = element.multiplyType;
        } else if (el === '9') {
          dataForDB.multiplayer9 = element.multiply;
          dataForDB.multiplayerType9 = element.multiplyType;
        } else if (el === '10-14') {
          dataForDB.multiplayer14 = element.multiply;
          dataForDB.multiplayerType14 = element.multiplyType;
        } else if (el === '15-16') {
          dataForDB.multiplayer16 = element.multiply;
          dataForDB.multiplayerType16 = element.multiplyType;
        } else {
          err = 'Invalid request data';
        }
      }
    });

    return {err: err, data: dataForDB};
  }

  function calculateGraduatedPricing(data, mcoData) {

    var tmpResponse = {
      '1-4': {},
      '1-4Estate': {},
      '5-6': {},
      '7': {},
      '8': {},
      '9': {},
      '10-14': {},
      '15-16': {}
    };

    // calculation for tier1Rate
    if (data['1-4'].tier1Rate) {
      tmpResponse['1-4'].tier1Rate = data['1-4'].tier1Rate;
      if (mcoData[0].estateLuggageMultiplayerType === 1) {
        tmpResponse['1-4Estate'].tier1Rate = data['1-4'].tier1Rate + (data['1-4'].tier1Rate * (
          mcoData[0].estateLuggageMultiplayer / 100));
      } else if (mcoData[0].estateLuggageMultiplayerType === 2) {
        tmpResponse['1-4Estate'].tier1Rate = data['1-4'].tier1Rate + mcoData[0].estateLuggageMultiplayer;
      }
      if (mcoData[0].multiplayerType6 === 1) {
        tmpResponse['5-6'].tier1Rate = data['1-4'].tier1Rate + (data['1-4'].tier1Rate * (
          mcoData[0].multiplayer6 / 100));
      } else if (mcoData[0].multiplayerType6 === 2) {
        tmpResponse['5-6'].tier1Rate = data['1-4'].tier1Rate + mcoData[0].multiplayer6;
      }
      if (mcoData[0].multiplayerType7 === 1) {
        tmpResponse['7'].tier1Rate = data['1-4'].tier1Rate + (data['1-4'].tier1Rate * (
          mcoData[0].multiplayer7 / 100));
      } else if (mcoData[0].multiplayerType7 === 2) {
        tmpResponse['7'].tier1Rate = data['1-4'].tier1Rate + mcoData[0].multiplayer7;
      }
      if (mcoData[0].multiplayerType8 === 1) {
        tmpResponse['8'].tier1Rate = data['1-4'].tier1Rate + (data['1-4'].tier1Rate * (
          mcoData[0].multiplayer8 / 100));
      } else if (mcoData[0].multiplayerType8 === 2) {
        tmpResponse['8'].tier1Rate = data['1-4'].tier1Rate + mcoData[0].multiplayer8;
      }
      if (mcoData[0].multiplayerType9 === 1) {
        tmpResponse['9'].tier1Rate = data['1-4'].tier1Rate + (data['1-4'].tier1Rate * (
          mcoData[0].multiplayer9 / 100));
      } else if (mcoData[0].multiplayerType9 === 2) {
        tmpResponse['9'].tier1Rate = data['1-4'].tier1Rate + mcoData[0].multiplayer9;
      }
      if (mcoData[0].multiplayerType14 === 1) {
        tmpResponse['10-14'].tier1Rate = data['1-4'].tier1Rate + (data['1-4'].tier1Rate * (
          mcoData[0].multiplayer14 / 100));
      } else if (mcoData[0].multiplayerType14 === 2) {
        tmpResponse['10-14'].tier1Rate = data['1-4'].tier1Rate + mcoData[0].multiplayer14;
      }
      if (mcoData[0].multiplayerType16 === 1) {
        tmpResponse['15-16'].tier1Rate = data['1-4'].tier1Rate + (data['1-4'].tier1Rate * (
          mcoData[0].multiplayer16 / 100));
      } else if (mcoData[0].multiplayerType16 === 2) {
        tmpResponse['15-16'].tier1Rate = data['1-4'].tier1Rate + mcoData[0].multiplayer16;
      }
    }

    // calculation for tier2Rate
    if (data['1-4'].tier2Rate) {
      tmpResponse['1-4'].tier2Rate = data['1-4'].tier2Rate;
      if (mcoData[0].estateLuggageMultiplayerType === 1) {
        tmpResponse['1-4Estate'].tier2Rate = data['1-4'].tier2Rate + (data['1-4'].tier2Rate * (
          mcoData[0].estateLuggageMultiplayer / 100));
      } else if (mcoData[0].estateLuggageMultiplayerType === 2) {
        tmpResponse['1-4Estate'].tier2Rate = data['1-4'].tier2Rate + mcoData[0].estateLuggageMultiplayer;
      }
      if (mcoData[0].multiplayerType6 === 1) {
        tmpResponse['5-6'].tier2Rate = data['1-4'].tier2Rate + (data['1-4'].tier2Rate * (
          mcoData[0].multiplayer6 / 100));
      } else if (mcoData[0].multiplayerType6 === 2) {
        tmpResponse['5-6'].tier2Rate = data['1-4'].tier2Rate + mcoData[0].multiplayer6;
      }
      if (mcoData[0].multiplayerType7 === 1) {
        tmpResponse['7'].tier2Rate = data['1-4'].tier2Rate + (data['1-4'].tier2Rate * (
          mcoData[0].multiplayer7 / 100));
      } else if (mcoData[0].multiplayerType7 === 2) {
        tmpResponse['7'].tier2Rate = data['1-4'].tier2Rate + mcoData[0].multiplayer7;
      }
      if (mcoData[0].multiplayerType8 === 1) {
        tmpResponse['8'].tier2Rate = data['1-4'].tier2Rate + (data['1-4'].tier2Rate * (
          mcoData[0].multiplayer8 / 100));
      } else if (mcoData[0].multiplayerType8 === 2) {
        tmpResponse['8'].tier2Rate = data['1-4'].tier2Rate + mcoData[0].multiplayer8;
      }
      if (mcoData[0].multiplayerType9 === 1) {
        tmpResponse['9'].tier2Rate = data['1-4'].tier2Rate + (data['1-4'].tier2Rate * (
          mcoData[0].multiplayer9 / 100));
      } else if (mcoData[0].multiplayerType9 === 2) {
        tmpResponse['9'].tier2Rate = data['1-4'].tier2Rate + mcoData[0].multiplayer9;
      }
      if (mcoData[0].multiplayerType14 === 1) {
        tmpResponse['10-14'].tier2Rate = data['1-4'].tier2Rate + (data['1-4'].tier2Rate * (
          mcoData[0].multiplayer14 / 100));
      } else if (mcoData[0].multiplayerType14 === 2) {
        tmpResponse['10-14'].tier2Rate = data['1-4'].tier2Rate + mcoData[0].multiplayer14;
      }
      if (mcoData[0].multiplayerType16 === 1) {
        tmpResponse['15-16'].tier2Rate = data['1-4'].tier2Rate + (data['1-4'].tier2Rate * (
          mcoData[0].multiplayer16 / 100));
      } else if (mcoData[0].multiplayerType16 === 2) {
        tmpResponse['15-16'].tier2Rate = data['1-4'].tier2Rate + mcoData[0].multiplayer16;
      }
    }

    // calculation for tier3Rate
    if (data['1-4'].tier3Rate) {
      tmpResponse['1-4'].tier3Rate = data['1-4'].tier3Rate;
      if (mcoData[0].estateLuggageMultiplayerType === 1) {
        tmpResponse['1-4Estate'].tier3Rate = data['1-4'].tier3Rate + (data['1-4'].tier3Rate * (
          mcoData[0].estateLuggageMultiplayer / 100));
      } else if (mcoData[0].estateLuggageMultiplayerType === 2) {
        tmpResponse['1-4Estate'].tier3Rate = data['1-4'].tier3Rate + mcoData[0].estateLuggageMultiplayer;
      }
      if (mcoData[0].multiplayerType6 === 1) {
        tmpResponse['5-6'].tier3Rate = data['1-4'].tier3Rate + (data['1-4'].tier3Rate * (
          mcoData[0].multiplayer6 / 100));
      } else if (mcoData[0].multiplayerType6 === 2) {
        tmpResponse['5-6'].tier3Rate = data['1-4'].tier3Rate + mcoData[0].multiplayer6;
      }
      if (mcoData[0].multiplayerType7 === 1) {
        tmpResponse['7'].tier3Rate = data['1-4'].tier3Rate + (data['1-4'].tier3Rate * (
          mcoData[0].multiplayer7 / 100));
      } else if (mcoData[0].multiplayerType7 === 2) {
        tmpResponse['7'].tier3Rate = data['1-4'].tier3Rate + mcoData[0].multiplayer7;
      }
      if (mcoData[0].multiplayerType8 === 1) {
        tmpResponse['8'].tier3Rate = data['1-4'].tier3Rate + (data['1-4'].tier3Rate * (
          mcoData[0].multiplayer8 / 100));
      } else if (mcoData[0].multiplayerType8 === 2) {
        tmpResponse['8'].tier3Rate = data['1-4'].tier3Rate + mcoData[0].multiplayer8;
      }
      if (mcoData[0].multiplayerType9 === 1) {
        tmpResponse['9'].tier3Rate = data['1-4'].tier3Rate + (data['1-4'].tier3Rate * (
          mcoData[0].multiplayer9 / 100));
      } else if (mcoData[0].multiplayerType9 === 2) {
        tmpResponse['9'].tier3Rate = data['1-4'].tier3Rate + mcoData[0].multiplayer9;
      }
      if (mcoData[0].multiplayerType14 === 1) {
        tmpResponse['10-14'].tier3Rate = data['1-4'].tier3Rate + (data['1-4'].tier3Rate * (
          mcoData[0].multiplayer14 / 100));
      } else if (mcoData[0].multiplayerType14 === 2) {
        tmpResponse['10-14'].tier3Rate = data['1-4'].tier3Rate + mcoData[0].multiplayer14;
      }
      if (mcoData[0].multiplayerType16 === 1) {
        tmpResponse['15-16'].tier3Rate = data['1-4'].tier3Rate + (data['1-4'].tier3Rate * (
          mcoData[0].multiplayer16 / 100));
      } else if (mcoData[0].multiplayerType16 === 2) {
        tmpResponse['15-16'].tier3Rate = data['1-4'].tier3Rate + mcoData[0].multiplayer16;
      }
    }

    // calculation for tier4Rate
    if (data['1-4'].tier4Rate) {
      tmpResponse['1-4'].tier4Rate = data['1-4'].tier4Rate;
      if (mcoData[0].estateLuggageMultiplayerType === 1) {
        tmpResponse['1-4Estate'].tier4Rate = data['1-4'].tier4Rate + (data['1-4'].tier4Rate * (
          mcoData[0].estateLuggageMultiplayer / 100));
      } else if (mcoData[0].estateLuggageMultiplayerType === 2) {
        tmpResponse['1-4Estate'].tier4Rate = data['1-4'].tier4Rate + mcoData[0].estateLuggageMultiplayer;
      }
      if (mcoData[0].multiplayerType6 === 1) {
        tmpResponse['5-6'].tier4Rate = data['1-4'].tier4Rate + (data['1-4'].tier4Rate * (
          mcoData[0].multiplayer6 / 100));
      } else if (mcoData[0].multiplayerType6 === 2) {
        tmpResponse['5-6'].tier4Rate = data['1-4'].tier4Rate + mcoData[0].multiplayer6;
      }
      if (mcoData[0].multiplayerType7 === 1) {
        tmpResponse['7'].tier4Rate = data['1-4'].tier4Rate + (data['1-4'].tier4Rate * (
          mcoData[0].multiplayer7 / 100));
      } else if (mcoData[0].multiplayerType7 === 2) {
        tmpResponse['7'].tier4Rate = data['1-4'].tier4Rate + mcoData[0].multiplayer7;
      }
      if (mcoData[0].multiplayerType8 === 1) {
        tmpResponse['8'].tier4Rate = data['1-4'].tier4Rate + (data['1-4'].tier4Rate * (
          mcoData[0].multiplayer8 / 100));
      } else if (mcoData[0].multiplayerType8 === 2) {
        tmpResponse['8'].tier4Rate = data['1-4'].tier4Rate + mcoData[0].multiplayer8;
      }
      if (mcoData[0].multiplayerType9 === 1) {
        tmpResponse['9'].tier4Rate = data['1-4'].tier4Rate + (data['1-4'].tier4Rate * (
          mcoData[0].multiplayer9 / 100));
      } else if (mcoData[0].multiplayerType9 === 2) {
        tmpResponse['9'].tier4Rate = data['1-4'].tier4Rate + mcoData[0].multiplayer9;
      }
      if (mcoData[0].multiplayerType14 === 1) {
        tmpResponse['10-14'].tier4Rate = data['1-4'].tier4Rate + (data['1-4'].tier4Rate * (
          mcoData[0].multiplayer14 / 100));
      } else if (mcoData[0].multiplayerType14 === 2) {
        tmpResponse['10-14'].tier4Rate = data['1-4'].tier4Rate + mcoData[0].multiplayer14;
      }
      if (mcoData[0].multiplayerType16 === 1) {
        tmpResponse['15-16'].tier4Rate = data['1-4'].tier4Rate + (data['1-4'].tier4Rate * (
          mcoData[0].multiplayer16 / 100));
      } else if (mcoData[0].multiplayerType16 === 2) {
        tmpResponse['15-16'].tier4Rate = data['1-4'].tier4Rate + mcoData[0].multiplayer16;
      }
    }

    // calculation for tier5Rate
    if (data['1-4'].tier5Rate) {
      tmpResponse['1-4'].tier5Rate = data['1-4'].tier5Rate;
      if (mcoData[0].estateLuggageMultiplayerType === 1) {
        tmpResponse['1-4Estate'].tier5Rate = data['1-4'].tier5Rate + (data['1-4'].tier5Rate * (
          mcoData[0].estateLuggageMultiplayer / 100));
      } else if (mcoData[0].estateLuggageMultiplayerType === 2) {
        tmpResponse['1-4Estate'].tier5Rate = data['1-4'].tier5Rate + mcoData[0].estateLuggageMultiplayer;
      }
      if (mcoData[0].multiplayerType6 === 1) {
        tmpResponse['5-6'].tier5Rate = data['1-4'].tier5Rate + (data['1-4'].tier5Rate * (
          mcoData[0].multiplayer6 / 100));
      } else if (mcoData[0].multiplayerType6 === 2) {
        tmpResponse['5-6'].tier5Rate = data['1-4'].tier5Rate + mcoData[0].multiplayer6;
      }
      if (mcoData[0].multiplayerType7 === 1) {
        tmpResponse['7'].tier5Rate = data['1-4'].tier5Rate + (data['1-4'].tier5Rate * (
          mcoData[0].multiplayer7 / 100));
      } else if (mcoData[0].multiplayerType7 === 2) {
        tmpResponse['7'].tier5Rate = data['1-4'].tier5Rate + mcoData[0].multiplayer7;
      }
      if (mcoData[0].multiplayerType8 === 1) {
        tmpResponse['8'].tier5Rate = data['1-4'].tier5Rate + (data['1-4'].tier5Rate * (
          mcoData[0].multiplayer8 / 100));
      } else if (mcoData[0].multiplayerType8 === 2) {
        tmpResponse['8'].tier5Rate = data['1-4'].tier5Rate + mcoData[0].multiplayer8;
      }
      if (mcoData[0].multiplayerType9 === 1) {
        tmpResponse['9'].tier5Rate = data['1-4'].tier5Rate + (data['1-4'].tier5Rate * (
          mcoData[0].multiplayer9 / 100));
      } else if (mcoData[0].multiplayerType9 === 2) {
        tmpResponse['9'].tier5Rate = data['1-4'].tier5Rate + mcoData[0].multiplayer9;
      }
      if (mcoData[0].multiplayerType14 === 1) {
        tmpResponse['10-14'].tier5Rate = data['1-4'].tier5Rate + (data['1-4'].tier5Rate * (
          mcoData[0].multiplayer14 / 100));
      } else if (mcoData[0].multiplayerType14 === 2) {
        tmpResponse['10-14'].tier5Rate = data['1-4'].tier5Rate + mcoData[0].multiplayer14;
      }
      if (mcoData[0].multiplayerType16 === 1) {
        tmpResponse['15-16'].tier5Rate = data['1-4'].tier5Rate + (data['1-4'].tier5Rate * (
          mcoData[0].multiplayer16 / 100));
      } else if (mcoData[0].multiplayerType16 === 2) {
        tmpResponse['15-16'].tier5Rate = data['1-4'].tier5Rate + mcoData[0].multiplayer16;
      }
    }

    return tmpResponse;
  }

  function calculateMileagePricing(tmpPrice, mcoData) {
    var calculatedPrices = {
      '1-4': {price: null},
      '1-4Estate': {price: null},
      '5-6': {price: null},
      '7': {price: null},
      '8': {price: null},
      '9': {price: null},
      '10-14': {price: null},
      '15-16': {price: null}
    };
    var tmp;

    // set 1-4 car type price
    if (tmpPrice < mcoData.minFare4)
      calculatedPrices['1-4'].price = mcoData.minFare4;
    else
      calculatedPrices['1-4'].price = tmpPrice;

    // set 1-4Estate car type price
    if (mcoData.estateLuggageMultiplayerType === 1) {
      tmp = tmpPrice + tmpPrice * (mcoData.estateLuggageMultiplayer / 100);
      if (tmp < mcoData.minFare4E)
        calculatedPrices['1-4Estate'].price = mcoData.minFare4E;
      else
        calculatedPrices['1-4Estate'].price = tmp;
    } else if (mcoData.estateLuggageMultiplayerType === 2) {
      tmp = tmpPrice + mcoData.estateLuggageMultiplayer;
      if (tmp < mcoData.minFare4E)
        calculatedPrices['1-4Estate'].price = mcoData.minFare4E;
      else
        calculatedPrices['1-4Estate'].price = tmp;
    }

    // set 5-6 car type price
    if (mcoData.multiplayerType6 === 1) {
      tmp = tmpPrice + tmpPrice * (mcoData.multiplayer6 / 100);
      if (tmp < mcoData.minFare6)
        calculatedPrices['5-6'].price = mcoData.minFare6;
      else
        calculatedPrices['5-6'].price = tmp;
    } else if (mcoData.multiplayerType6 === 2) {
      tmp = tmpPrice + mcoData.multiplayer6;
      if (tmp < mcoData.minFare6)
        calculatedPrices['5-6'].price = mcoData.minFare6;
      else
        calculatedPrices['5-6'].price = tmp;
    }

    // set 7 car type price
    if (mcoData.multiplayerType7 === 1) {
      tmp = tmpPrice + tmpPrice * (mcoData.multiplayer7 / 100);
      if (tmp < mcoData.minFare7)
        calculatedPrices['7'].price = mcoData.minFare7;
      else
        calculatedPrices['7'].price = tmp;
    } else if (mcoData.multiplayerType7 === 2) {
      tmp = tmpPrice + mcoData.multiplayer7;
      if (tmp < mcoData.minFare7)
        calculatedPrices['7'].price = mcoData.minFare7;
      else
        calculatedPrices['7'].price = tmp;
    }

    // set 8 car type price
    if (mcoData.multiplayerType8 === 1) {
      tmp = tmpPrice + tmpPrice * (mcoData.multiplayer8 / 100);
      if (tmp < mcoData.minFare8)
        calculatedPrices['8'].price = mcoData.minFare8;
      else
        calculatedPrices['8'].price = tmp;
    } else if (mcoData.multiplayerType8 === 2) {
      tmp = tmpPrice + mcoData.multiplayer8;
      if (tmp < mcoData.minFare8)
        calculatedPrices['8'].price = mcoData.minFare8;
      else
        calculatedPrices['8'].price = tmp;
    }

    // set 9 car type price
    if (mcoData.multiplayerType9 === 1) {
      tmp = tmpPrice + tmpPrice * (mcoData.multiplayer9 / 100);
      if (tmp < mcoData.minFare9)
        calculatedPrices['9'].price = mcoData.minFare9;
      else
        calculatedPrices['9'].price = tmp;
    } else if (mcoData.multiplayerType9 === 2) {
      tmp = tmpPrice + mcoData.multiplayer9;
      if (tmp < mcoData.minFare9)
        calculatedPrices['9'].price = mcoData.minFare9;
      else
        calculatedPrices['9'].price = tmp;
    }

    // set 10-14 car type price
    if (mcoData.multiplayerType14 === 1) {
      tmp = tmpPrice + tmpPrice * (mcoData.multiplayer14 / 100);
      if (tmp < mcoData.minFare14)
        calculatedPrices['10-14'].price = mcoData.minFare14;
      else
        calculatedPrices['10-14'].price = tmp;
    } else if (mcoData.multiplayerType14 === 2) {
      tmp = tmpPrice + mcoData.multiplayer14;
      if (tmp < mcoData.minFare14)
        calculatedPrices['10-14'].price = mcoData.minFare14;
      else
        calculatedPrices['10-14'].price = tmp;
    }

    // set 15-16 car type price
    if (mcoData.multiplayerType16 === 1) {
      tmp = tmpPrice + tmpPrice * (mcoData.multiplayer16 / 100);
      if (tmp < mcoData.minFare16)
        calculatedPrices['15-16'].price = mcoData.minFare16;
      else
        calculatedPrices['15-16'].price = tmp;
    } else if (mcoData.multiplayerType16 === 2) {
      tmp = tmpPrice + mcoData.multiplayer16;
      if (tmp < mcoData.minFare16)
        calculatedPrices['15-16'].price = mcoData.minFare16;
      else
        calculatedPrices['15-16'].price = tmp;
    }

    return calculatedPrices;
  }
};
