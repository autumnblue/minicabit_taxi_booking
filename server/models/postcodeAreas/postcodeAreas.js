'use strict';
var _ = require('lodash');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');
var utils = require('loopback/lib/utils.js');

module.exports = function(PostcodeAreas) {

  PostcodeAreas.observe('access', function logQuery(ctx, next) {

    ctx.query.limit = 100;
    if (ctx.query.where.outcode.like.slice(-1) !== '%') {
      ctx.query.where.outcode.like += '%';
    }

    if (ctx.query.where.outcode.like.length > 2) {
      logger.debug('Serching postcode areas with query', ctx.query);
      next();
    } else {
      next('At least 2 characters should be specified');
    }
  });

};
