'use strict';
var _ = require('lodash');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');
var utils = require('loopback/lib/utils.js');


module.exports = function(Address) {

  Address.return = function(addressIds, callback) {

    var addressModel = server.models.address;
    var response = [];

    var addressIds = addressIds.split(',');
    var addressProcessed = 0;

    _.forEach(addressIds, function(id) {
      logger.debug('address id', id);
      addressModel.findOne({where: {addressId: id}}, function(err, result) {
        addressProcessed++;
        if (err) {
          callback(err);
        } else {
          if (result) {
            var returnAdress = {
              id: result['addressId'],
              formattedAddress: result['formattedAddress'],
              postcode: result['postcode'],
              longitude: result['longitude'],
              latitude: result['latitude']
            };

            response.push(returnAdress);
          } else {
            logger.debug('Address with id not found', id);
          }
          if (addressProcessed === addressIds.length) {
            return callback(null, response);
          }
        }
      });
    });
  };

  loopback.remoteMethod(
    Address.return, {
      accepts: [
        {
          arg: 'addressIds', type: 'string', required: true, description: 'address ids'
        },
      ],
      returns: {arg: 'addresses', type: 'object'},
      http: {path: '/:addressIds', verb: 'get', status: 200, errorStatus: 400},
      description: 'get address details for address ids'
    }
  );

  Address.updateAddress = function(addressId, data, callback) {

    var addressModel = server.models.address;

    addressModel.findById(addressId, function(err, result) {
      if (err) {
        logger.error('Error getting address by id', err);
        callback(err);
      } else {
        logger.debug(result);

        if (data['formattedAddress']) {
          result.formattedAddres = data['formattedAddress'];
        };

        if (data['longitude']) {
          result.longitude = data['longitude'];
        };

        if (data['latitude']) {
          result.latitude = data['latitude'];
        }

        result.save(null, function(err, savedAddress) {
          if (err) {
            logger.error('Error saving address', err);
            callback(err);
          } else {
            logger.debug('Saved address', savedAddress);
            callback(null, savedAddress);
          }
        });
      }
    });
  };

  loopback.remoteMethod(
    Address.updateAddress, {
      accepts: [
        {
          arg: 'addressId', type: 'string', required: true, description: 'address id'
        },
        {
          arg: 'data', type: 'object', required: true, description: 'address update data', 'http': { source: 'body' }
        }
      ],
      returns: {arg: 'addresses', type: 'object'},
      http: {path: '/:addressId', verb: 'put', status: 200, errorStatus: 400},
      description: 'update address details for address ids'
    }
  );
};
