/**
 * Created by dejan on 29/08/16.
 */

'use strict';
var _ = require('lodash');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');
var pricesHelper = require('../../helpers/pricesCalculations');

module.exports = function(BookingAccounting) {

  BookingAccounting.insertInto = function(vendorTxCode, callback) {
    var bookingModel = server.models.booking;
    var globalSettingsModel = server.models.globalSettings;

    bookingModel.find({where: {vendortxcode: vendorTxCode}, include: ['promotionalCode']}, function(err, result) {
      if (err) {
        logger.error('Error with selecting booking from booking table for vendorTxCode: [ ' + vendorTxCode + ' ] ' +
          err);
        return callback(err);
      } else {
        if (!result || result.length === 0) {
          return callback('No results for vendorTxCode [ ' + vendorTxCode + ' ]');
        } else {
          globalSettingsModel.findOne({where: {settingName: 'vatRate'}}, function(err, vatRate) {
            if (err) {
              logger.error('Error with getting vat rate from global settings ' + err);
              return callback(err);
            } else {
              var b = JSON.stringify(result);
              var booking = JSON.parse(b);
              var bookingOutbound = null;
              var bookingInbound = null;

              if (booking.length === 1) {
                bookingOutbound = booking[0];
              } else if (booking[0].type.toUpperCase() === 'OUTBOUND') {
                bookingOutbound = booking[0];
                bookingInbound = booking[1];
              } else {
                bookingOutbound = booking[1];
                bookingInbound = booking[0];
              }

              var bookingAccountingData =
                pricesHelper.calculateAllPrices(bookingOutbound, bookingInbound, vatRate.value);

              var dataForInsert = [];
              if (bookingAccountingData.bookingInbound) {
                dataForInsert.push(bookingAccountingData.bookingOutbound);
                dataForInsert.push(bookingAccountingData.bookingInbound);
              } else {
                dataForInsert.push(bookingAccountingData.bookingOutbound);
              }

              BookingAccounting.create(dataForInsert, function(err, result) {
                if (err) {
                  logger.error('Error with inserting to booking accounting table ' + err);
                  return callback(err);
                } else {
                  return callback(null, result);
                }
              });
            }
          });
        }
      }
    });
  };
  loopback.remoteMethod(
    BookingAccounting.insertInto, {
      accepts: [
        {
          arg: 'vendorTxCode', type: 'string', required: true, description: 'vendor tx code'
        }
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/insert/:vendorTxCode', verb: 'post', status: 201, errorStatus: 400},
      description: 'insert booking details for vendor tx code'
    }
  );
};
