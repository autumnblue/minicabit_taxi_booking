'use strict';

var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');


module.exports = function(McoPayrun) {

  function getInvoices(payrunId) {
    var mcoPayrunModel = server.models.mcoPayrun;
    return new Promise(function(resolve, reject) {
      mcoPayrunModel.findOne({
        where: {mcoPayrunId: payrunId},
        include: {
          relation: 'payrunInvoices',
          scope: {
            include: {
              relation: 'mcoInvoice',
              scope: {
                fields: ['registeredName', 'bankName', 'officeEmail', 'contactPersonDay']
              }
            }
          }
        }
      }, function(err, result) {
        if (err) {
          logger.error('Error retrieving data from mcoInvoid');
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  McoPayrun.getPayrunDetails = function(payrunId, callback) {
    console.log(payrunId);
    getInvoices(payrunId)
      .then(function(invoices) {
        return callback(null, invoices);
      })
      .catch(function(err) {
        logger.error('Error with retreiving payrun details');
        logger.error(err);
        return callback(err);
      });
  };

  loopback.remoteMethod(
    McoPayrun.getPayrunDetails, {
      accepts: [
        {arg: 'payrunId', type: 'number', required: true, description: 'Payrun ID'}
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/:payrunId', verb: 'get', status: 200, errorStatus: 400},
      description: 'Get the details of a payrun'
    }
  );
};
