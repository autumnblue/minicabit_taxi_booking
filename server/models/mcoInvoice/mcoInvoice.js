/**
 * Created by dejan on 23/08/16.
 */

'use strict';
var _ = require('lodash');
var PDF = require('pdfkit');
var loopback = require('loopback');
var server = require('./../../server');
var logger = require('./../../logger');
var utils = require('loopback/lib/utils.js');
var moment = require('moment')

module.exports = function(McoInvoice) {

  McoInvoice.on('dataSourceAttached', function() {

    McoInvoice.findById = function(id, filter, callback) {

      McoInvoice.findOne({where: {invoiceId: id}}, function(err, resultInvoice) {
        if (err) {
          logger.error('Error getting mco invoice details [ ' + id + ' ] ' + err);
          callback(err);
        } else {
          if (!resultInvoice) {
            return callback('This invoice ID does not exist');
          } else {

            var mcoReconcileModel = server.models.mcoReconcile;

            mcoReconcileModel.findOne({where: {invoiceId: resultInvoice.invoiceId}, include: {booking: {mco: 'address'}}}, function(err, result) {
              if (err) {
                logger.error('Error getting mco reconcile model', result.inoviceId);
                return callback(err);
              } else {
                var r = result.toJSON();
                r.invoice = resultInvoice.toJSON();
                logger.debug('mco inovice result for invoice id', r.invoiceId, r);
                return callback(null, r);
              }
            });
          }
        }
      });
    };
  });

  McoInvoice.printInvoice = function(res, invoiceId, callback) {

    var mcoInvoiceModel = server.models.mcoInvoice;
    var total = 0;

    var logoUrl = 'http://d3prcfwi67dnm2.cloudfront.net/MINICABIT_LOGO_FULL_LOCKUP_RGB_WITH_LOGO.jpg';

    function getLogo(cb)  {
      var request = require('request').defaults({ encoding: null });
      request.get(logoUrl, function (err, res, body) {
        if (err) {
          cb(err);
        } else {
          var buf = new Buffer(body, 'binary');
          cb(null, buf);
        }
      });
    }

    mcoInvoiceModel.findById(invoiceId, null, function(err, i) {

      if (err) {
        callback(err);
      } else {

        var doc = new PDF();
        res.type('application/pdf');
        doc.pipe(res);

        doc
          .font('Helvetica')
          .fontSize(20);

        doc.text('Invoice no.' + i.invoiceId, 50, 120);

        doc
          .font('Helvetica')
          .fontSize(12);

        doc.text(i.booking.mco.registeredName, 50, 150);
        doc.text('PHO ID:' + i.booking.mco.mcoId, 50, 167);
        doc.text(i.booking.mco.address.town + ',', 50, 184);
        doc.text(i.booking.mco.address.street + ',', 50, 201);
        doc.text(i.booking.mco.address.locality, 50, 218);


        doc.text('minicabit Limited. 80-83,', 300, 150, {width: 270, align: 'right'});
        doc.text('Long Lane, London', 300, 167, {width: 270, align: 'right'});
        doc.text('EC1A 9ET', 300, 184, {width: 270, align: 'right'});
        doc.text('minicabit VAT Reg. No.106 5117 49', 300, 201, {width: 270, align: 'right'});
        doc.text('Invoice no. ' + i.invoiceId, 300, 218, {width: 270, align: 'right'});

        doc.text('Date: ' + moment(i.booking.bookingDate).format('Do MMMM YYYY'), 50, 280, {width: 270});

        doc.text('Commission on jobs included in payment:', 50, 320, {width: 270});
        doc.text('£' + i.invoice.minicabitNetCommission, 500, 320, {width:60, align: 'right'});
        doc.text('VAT:', 50, 339, {width: 270});
        doc.text('£' + i.invoice.vatOnCommission, 500, 339, {width:60, align: 'right'})
        doc.text('Total', 50, 360, {width: 270});

        // total = parseFloat(i.invoice.minicabitCommission) + parseFloat(i.invoice.vatOnCommission);

        doc.text('£' + i.invoice.minicabitCommission, 500, 360, {width:60, align: 'right'});

        doc.moveTo(50, 318)
          .lineTo(580, 318)
          .stroke();

        doc.moveTo(50, 335)
          .lineTo(580, 335)
          .stroke();

        doc.moveTo(50, 350)
          .lineTo(580, 350)
          .stroke();

        getLogo(function(err, image) {
          if (!err) {
            try {
              doc.image(image, 50, 50, {scale:0.4});
            } catch(e) {
              logger.error('Error inserting image', err);
            }
          }

          doc.end();
        });

        //callback(null, doc);
      }
    });
  }

  loopback.remoteMethod(
    McoInvoice.printInvoice, {
      accepts: [
        {
          arg: 'res', type: 'object', 'http': {source: 'res'}
        },
        {
          arg: 'invoiceId', type: 'string', required: true, description: 'invoice id'
        }
      ],
      returns: {arg: 'data', type: 'object'},
      http: {path: '/:invoiceId/print', verb: 'get', status: 200, errorStatus: 400},
      description: 'Print PDF invoice for invoice id'
    }
  );

}
