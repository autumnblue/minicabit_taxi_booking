'use strict';

module.exports = {
  db: {
    'name': 'db',
    'connector': process.env.DB_DIALECT || 'mysql',
    'host': process.env.DB_HOST || 'localhost',
    'port': process.env.DB_PORT || 3306,
    'database': process.env.DATABASE || 'cogotrip',
    'user': process.env.DB_USERNAME || 'minicabit',
    'password': process.env.DB_PASSWORD || 'minicabit888',
  },
  redis: {
    'name': 'redis',
    'connector': 'redis',
    'host': process.env.REDIS_ADDRESS || 'localhost',
    'port': process.env.REDIS_PORT || 6379
  }
};
