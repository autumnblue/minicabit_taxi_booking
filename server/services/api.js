'use strict';
var request = require('request');

function IsJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

module.exports = {
  get: function(url, data, apiKey, callback) {
    if (callback === undefined) {
      callback = apiKey;
      apiKey = undefined;
    }

    var options = {
      method: 'GET',
      url: url,
      qs: data,
      headers: {
        'accept': 'application/json'
      }
    };

    if (apiKey) {
      options.headers[apiKey.name] = apiKey.value;
    }

    this.request(options, callback);
  },
  post: function(url, data, isJson, apiKey, callback) {
    if (callback === undefined) {
      callback = apiKey;
      apiKey = undefined;
    }

    var options = {
      method: 'POST',
      url: url,
      headers: {
        'accept': 'application/json'
      }
    };

    if (apiKey) {
      options.headers[apiKey.name] = apiKey.value;
    }

    if (isJson) {
      options.body = data;
      options.json = true;
    } else {
      options.form = data;
    }

    this.request(options, apiKey, callback);
  },
  put: function(url, data, isJson, apiKey, callback) {
    if (callback === undefined) {
      callback = apiKey;
      apiKey = undefined;
    }

    var options = {
      method: 'PUT',
      url: url,
      headers: {
        'accept': 'application/json'
      }
    };

    if (apiKey) {
      options.headers[apiKey.name] = apiKey.value;
    }

    if (isJson) {
      options.body = data;
      options.json = true;
    } else {
      options.form = data;
    }

    this.request(options, callback);
  },
  delete: function(url, data, apiKey, callback) {
    if (callback === undefined) {
      callback = apiKey;
      apiKey = undefined;
    }

    var options = {
      method: 'DELETE',
      url: url,
      qs: data,
      headers: {
        'accept': 'application/json'
      }
    };

    if (apiKey) {
      options.headers[apiKey.name] = apiKey.value;
    }

    this.request(options, callback);
  },
  request: function(options, callback) {
    console.log('Custom API request', options.method, options.url, ', qs: ', options.qs ? options.qs : '');

    request(options, function(error, response, body) {
      if (error) {
        console.error(error);
        return callback(error);
      }
      // if not status code is in 200 range throw an error
      if (response.statusCode < 200 || response.statusCode > 299) {
        var errObj = {
          statusCode: response.statusCode,
          statusMessage: response.statusMessage,
          message: body.errorMessage
        };
        if (body.errorList) {
          errObj.errorList = body.errorList;
        }

        console.error('Error!', errObj);
        return callback(errObj);
      }
      // return ok response
      if (IsJsonString(body)) {
        callback(null, JSON.parse(body), response);
      } else {
        callback(null, body, response);
      }

    });
  }
};
