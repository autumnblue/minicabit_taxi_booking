/**
 * Created by Dejan on 10. 08. 2016.
 */

'use strict';

var config = require('../config');
var mandrillEmail = require('../middleware/mandrill');
var logger = require('./../logger');
var moment = require('moment');
var _ = require('lodash');
var server = require('./../server');

var nm = exports = module.exports = {};

function getMcoDetailsByIdPromise(mcoId) {
  var mcoModel = server.models.mco;

  return new Promise(function(resolve, reject) {
    mcoModel.findOne({
      where: {mcoId: mcoId}, include: ['address']}, function(err, result) {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
}

nm.sendBookingCancellation = function(booking, receiver, callback) {
  var nowDateString = new Date();
  var bookingCancellationDataCustomer = {
    templateName: '',
    bookingRef: booking.bookingLinktext,
    requestDate: nowDateString,
    reason: '',
    source: '',
    contactEmail: 'info@minicabit.com',
    message: '',
    dateOfTrip: new Date(booking.pickupEtaDts).toDateString(),
    numbOfPassengers: booking.numberOfPassengers,
    totalCost: booking.amount,
    pickUpTime: new Date(booking.pickupEtaDts).toLocaleTimeString(),
    destinationTime: new Date(booking.destinationEtaDts).toLocaleTimeString(),
    additionalInfo: booking.additionalInformation,
    fromAddress: booking.pickupAddress.property,
    toAddress: booking.destinationAddress.property,
    refund: booking.amount - booking.ccCharges,
    paymentMethod: booking.paymentType,
    chauffeurit: booking.chauffeurit,
    year: new Date().getFullYear(),
    sendTo: {
      firstName: booking.customer.firstname,
      lastName: booking.customer.lastname,
      email: booking.customer.customerEmail
    },
    customer: {
      firstName: booking.customer.firstname,
      lastName: booking.customer.lastname,
      email: booking.customer.customerEmail
    }
  };
  var bookingCancellationDataPHO = {
    templateName: '',
    bookingRef: booking.bookingLinktext,
    requestDate: nowDateString,
    reason: '',
    source: '',
    contactEmail: 'info@minicabit.com',
    message: '',
    pickUpTime: new Date(booking.pickupEtaDts).toLocaleTimeString(),
    dateOfTrip: new Date(booking.pickupEtaDts).toDateString(),
    numbOfPassengers: booking.numberOfPassengers,
    additionalInfo: booking.additionalInformation,
    tripCharges: booking.tripCharges,
    fromAddress: booking.pickupAddress.property,
    toAddress: booking.destinationAddress.property,
    paymentMethod: booking.paymentType,
    chauffeurit: booking.chauffeurit,
    mcoPay: booking.amount / 2,
    year: new Date().getFullYear(),
    sendTo: {
      firstName: booking.mco.registeredName,
      lastName: '',
      email: booking.mco.officeEmail
    },
    customer: {
      firstName: booking.customer.firstname,
      lastName: booking.customer.lastname,
      email: booking.customer.customerEmail
    }
  };

  if (bookingCancellationDataCustomer.chauffeurit === 1) {
    if (nowDateString < booking.earlyCancellationTs) {
      // early cancellation
      bookingCancellationDataCustomer.templateName =
        config.thirdPartyApis.mandrill.templates.chauffeuritEarlyBookingCancellationCustomer;
      bookingCancellationDataPHO.templateName =
        config.thirdPartyApis.mandrill.templates.chauffeuritEarlyBookingCancellationPHO;
    } else {
      // late cancellation
      bookingCancellationDataCustomer.templateName =
        config.thirdPartyApis.mandrill.templates.chauffeuritLateBookingCancellationCustomer;
      bookingCancellationDataPHO.templateName =
        config.thirdPartyApis.mandrill.templates.chauffeuritLateBookingCancellationPHO;
    }
  } else {
    if (nowDateString < booking.earlyCancellationTs) {
      // early cancellation
      bookingCancellationDataCustomer.templateName =
        config.thirdPartyApis.mandrill.templates.earlyBookingCancellationCustomer;
      bookingCancellationDataPHO.templateName =
        config.thirdPartyApis.mandrill.templates.earlyBookingCancellationPHO;
    } else {
      // late cancellation
      bookingCancellationDataCustomer.templateName =
        config.thirdPartyApis.mandrill.templates.lateBookingCancellationCustomer;
      bookingCancellationDataPHO.templateName =
        config.thirdPartyApis.mandrill.templates.lateBookingCancellationPHO;
    }
  }

  if (receiver.toUpperCase() === 'CUSTOMER') {
    var result = createEmailCancellationContent(bookingCancellationDataCustomer);
    if (result.err) {
      return callback(result.err);
    } else {
      mandrillEmail.sendEmail(bookingCancellationDataCustomer.templateName, result.templateContent,
      result.message, function(err) {
        if (!err) {
          logger.info('Email cancellation sent to customer', result.message.to[0].email);
          return callback(null, 'Cancellation email send to ' + receiver);
        } else {
          logger.error('Error sending cancellation email to customer', err);
          return callback(err);
        }
      });
    }
  } else if (receiver.toUpperCase() === 'PHO') {
    var result = createEmailCancellationContent(bookingCancellationDataPHO);
    if (result.err) {
      return callback(result.err);
    } else {
      mandrillEmail.sendEmail(bookingCancellationDataPHO.templateName, result.templateContent,
      result.message, function(err) {
        if (!err) {
          logger.info('Email cancellation sent to customer', result.message.to[0].email);
          return callback(null, 'Cancellation email send to ' + receiver);
        } else {
          logger.error('Error sending cancellation email to customer', err);
          return callback(err);
        }
      });
    }
  }
};

function createEmailCancellationContent(bookingData) {
  if (process.env.NODE_ENV !== 'production') {
    bookingData.sendTo.email = 'james.inglis@minicabit.com';
  }
  try {
    var message = {
      'to': [{
        'email': bookingData.sendTo.email,
        'name': bookingData.sendTo.firstName + ' ' + bookingData.sendTo.lastName,
        'type': 'to'
      },
        {
          'email': 'info@minicabit.com',
          'name': 'info@minicabit.com',
          'type': 'bcc'
        }],
      'global_merge_vars': [
        {
          'name': 'booking_linktext',
          'content': bookingData.bookingRef
        },
        {
          'name': 'date_of_trip',
          'content': bookingData.dateOfTrip
        },
        {
          'name': 'passenger_name',
          'content': bookingData.customer.firstName + ' ' + bookingData.customer.lastName
        },
        {
          'name': 'number_of_passengers',
          'content': bookingData.numbOfPassengers
        },
        {
          'name': 'booking_amount',
          'content': bookingData.totalCost
        },
        {
          'name': 'mco_pay',
          'content': bookingData.mcoPay
        },
        {
          'name': 'trip_charges',
          'content': bookingData.tripCharges
        },
        {
          'name': 'pickup_time',
          'content': bookingData.pickUpTime
        },
        {
          'name': 'destination_time',
          'content': bookingData.destinationTime
        },
        {
          'name': 'additional_information',
          'content': bookingData.additionalInfo
        },
        {
          'name': 'pickup_location',
          'content': bookingData.fromAddress
        },
        {
          'name': 'destination_location',
          'content': bookingData.toAddress
        },
        {
          'name': 'refund',
          'content': bookingData.refund
        },
        {
          'name': 'year',
          'content': bookingData.year
        },
        {
          'name': 'nature_of_contact',
          'content': bookingData.reason
        },
        {
          'name': 'yourmessage',
          'content': bookingData.message
        },
        {
          'name': 'payment_method',
          'content': bookingData.paymentMethod
        },
        {
          'name': 'early_cancellation_time',
          'content': bookingData.earlyCancellationDate
        }
      ]
    };
    return ({err: null, templateContent: [], message: message});
  } catch (e) {
    logger.error('Error sending cancellation email to customer', e);
    return ({err: e});
  }
}

nm.sendBookingConfirmation = function(bookingData, receiver, callback) {
  var response = getBookingConfirmationNotificationDetails(bookingData);

  if (response.err) {
    logger.error('Error getting booking confirmation', response.err);
    return callback(response.err);
  } else {
    var bookingDetails = response.result;
    bookingDetails.subject = 'Updated confirmation';

    getMcoDetailsByIdPromise(bookingDetails.booking.bookingOutbound.mcoId)
      .then(function(mcoData) {
        var json = JSON.stringify(mcoData);
        var mcoDataJson = JSON.parse(json);

        bookingDetails.cabOperator = {
          name: mcoDataJson.registeredName,
          phone: mcoDataJson.officeContactTel,
          username: mcoDataJson.username,
          email: mcoDataJson.officeEmail,
          addressStreet: mcoDataJson.address.street,
          addressProperty: mcoDataJson.address.property,
          addressTown: mcoDataJson.address.town,
          addressPostcode: mcoDataJson.address.postcode,
          fleetLabel: mcoDataJson.fleetLabel
        };

        bookingDetails.sendTo.email = bookingData.customer.customerEmail;
        bookingDetails.sendTo.name = bookingData.customer.firstname + ' ' + bookingData.customer.lastname;

        var emailContent = createEmailConfirmationContent(bookingDetails);
        if (emailContent.err) {
          return callback(emailContent.err);
        } else {
          if (receiver.toUpperCase() === 'CUSTOMER') {
            mandrillEmail.sendEmail(bookingDetails.emailTemplate, emailContent.templateContent,
              emailContent.message, function(err) {
                if (!err) {
                  logger.info('Email confirmation sent to customer');
                } else {
                  logger.error('Error sending email confirmation to customer', err);
                }
              });
          } else if (receiver.toUpperCase() === 'PHO') {
            bookingDetails.sendTo.email = bookingDetails.cabOperator.email;
            bookingDetails.sendTo.name = bookingDetails.cabOperator.name;

            emailContent = createEmailConfirmationContent(bookingDetails);
            mandrillEmail.sendEmail(bookingDetails.emailTemplateMco, emailContent.templateContent,
              emailContent.message, function(err) {
                if (!err) {
                  logger.info('Email confirmation sent to mco');
                } else {
                  logger.error('Error sending email confirmation to mco', err);
                }
              });
          }
          callback(false, 'Confirmation mail send');
        }
      }).catch(function(err) {
        callback('Error with sending email ' + err);
      });
  }
};

function createEmailConfirmationContent(booking) {
  if (process.env.NODE_ENV !== 'production') {
    booking.sendTo.email = 'james.inglis@minicabit.com';
  }
  try {
    var templateContent = [];
    var message = {
      'to': [{
        'email': booking.sendTo.email,
        'name': booking.sendTo.name,
        'type': 'to'
      },
        {
          'email': 'info@minicabit.com',
          'name': 'info@minicabit.com',
          'type': 'bcc'
        }],
      'global_merge_vars': [
        {
          'name': 'booking_linktext',
          'content': booking.booking.bookingOutbound.bookingRef
        },
        {
          'name': 'md5_booking_linktext',
          'content': booking.booking.bookingOutbound.bookingRefMD5
        },
        {
          'name': 'booking_gross_amount',
          'content': booking.booking.grossAmount
        },
        {
          'name': 'customer_name',
          'content': booking.customer.firstname + ' ' + booking.customer.lastname
        },
        {
          'name': 'customer_details_customer_email',
          'content': booking.customer.customerEmail
        },
        {
          'name': 'customer_details_customer_mobile_number',
          'content': booking.customer.customerMobileNumber || booking.customer.customerTel
        },
        {
          'name': 'additional_information',
          'content': booking.booking.additionalInfo
        },
        {
          'name': 'mco_details_registered_name',
          'content': booking.cabOperator.name
        },
        {
          'name': 'mco_address',
          'content': booking.cabOperator.addressStreet + ', ' + booking.cabOperator.addressTown
        },
        {
          'name': 'mco_details_office_contact_tel',
          'content': booking.cabOperator.phone
        },
        {
          'name': 'mco_details_office_email',
          'content': booking.cabOperator.email
        },
        {
          'name': 'date_of_trip',
          'content': booking.booking.bookingOutbound.pickUpDate
        },
        {
          'name': 'pickup_time',
          'content': booking.booking.bookingOutbound.pickUpTime
        },
        {
          'name': 'number_of_passengers',
          'content': booking.booking.numbOfPassengers
        },
        {
          'name': 'booking_car_type',
          'content': booking.booking.carType
        },
        {
          'name': 'latest_cancellation_time',
          'content': booking.booking.bookingOutbound.pickUpDate + ' ' + booking.booking.bookingOutbound.pickUpTime
        },
        {
          'name': 'txt_alert_txt',
          'content': 'yes'
        },
        {
          'name': 'year',
          'content': new Date().getFullYear()
        },
        {
          'name': 'booking_per_passenger_amount',
          'content': booking.booking.perPassenger
        },
        {
          'name': 'mco_details_fleet_label',
          'content': booking.cabOperator.fleetLabel
        },
        {
          'name': 'trip_charges',
          'content': booking.booking.bookingOutbound.tripCharges
        },
        {
          'name': 'pay_commition_to_minicabit',
          'content': booking.booking.bookingOutbound.payCommission
        },
        {
          'name': 'pickup_location',
          'content': booking.booking.bookingOutbound.from
        },
        {
          'name': 'destination_location',
          'content': booking.booking.bookingOutbound.to
        },
        {
          'name': 'mco_account_id',
          'content': booking.cabOperator.username
        },
        {
          'name': 'booking_price_type',
          'content': booking.booking.priceType || ''
        },
        {
          'name': 'pre_body',
          'content': booking.booking.preBody
        },
        {
          'name': 'pre_link',
          'content': 'CLICK HERE TO SET BOOKING LIVE STATUS'
        },
        {
          'name': 'vat',
          'content': booking.booking.bookingOutbound.vat
        },
        {
          'name': 'you_retain',
          'content': booking.booking.bookingOutbound.youRetain
        },
        {
          'name': 'collection_time',
          'content': booking.booking.collectionTime
        },
        {
          'name': 'arrival_time',
          'content': booking.booking.collectionTime
        },
        {
          'name': 'waiting_minutes',
          'content': booking.booking.waitingMinutes
        },
        {
          'name': 'waiting_hrs',
          'content': booking.booking.waitingHrs
        },
        {
          'name': 'via',
          'content': booking.booking.via
        },
        {
          'name': 're_vias',
          'content': booking.booking.reVia
        },
        {
          'name': 'return_time',
          'content': booking.booking.bookingOutbound.returnTime
        },
        {
          'name': 'return_label',
          'content': 'Trip type'
        },
        {
          'name': 'return_line',
          'content': booking.booking.line
        },
        {
          'name': 'payment_method',
          'content': booking.booking.paymentMethod
        },
        {
          'name': 'payment_label',
          'content': booking.booking.paymentMethod
        },
        {
          'name': 'booking_is_pingit',
          'content': booking.booking.isPingit
        },
        {
          'name': 'booking_luggage_json',
          'content': booking.booking.luggage
        },
        {
          'name': 'booking_type',
          'content': booking.booking.tripType
        },
        {
          'name': 'customerSupportNumber',
          'content': booking.booking.customerSupportNumber
        }
      ]
    };

    if (booking.subject)
      message.subject = booking.subject;

    if (booking.booking.paymentMethod === 'CARD') {
      message.global_merge_vars.push({
        'name': 'Billingfirstnames',
        'content': booking.booking.billingFirstname || ''
      });
      message.global_merge_vars.push({
        'name': 'BillingSurname',
        'content': booking.booking.billingSurname || ''
      });
      message.global_merge_vars.push({
        'name': 'BillingAddress1',
        'content': booking.booking.billingAddress || ''
      });
      message.global_merge_vars.push({
        'name': 'BillingCity',
        'content': booking.booking.billingCity || ''
      });
      message.global_merge_vars.push({
        'name': 'BillingPostCode',
        'content': booking.booking.billingPostCode || ''
      });
      message.global_merge_vars.push({
        'name': 'BillingCountry',
        'content': booking.booking.billingCountry || ''
      });
    }
    logger.debug('TCONTENT', templateContent);
    return ({err: null, templateContent: templateContent, message: message});
  } catch (e) {
    return ({err: e});
  }
}

function getBookingConfirmationNotificationDetails(bookingData) {
  try {
    var bookingDetails = {
      smsTemplate: '',
      emailTemplate: '',
      emailTemplateMco: '',
      booking: {},
      sendTo: {}
    };

    bookingDetails.booking.bookingOutbound = {};


    var UKTime = moment.utc(bookingData.bookingOutbound.pickupEtaDts);
    bookingDetails.booking.bookingOutbound.pickUpTime = UKTime.format('HH:mm');
    bookingDetails.booking.bookingOutbound.pickUpDate =
      new Date(bookingData.bookingOutbound.pickupEtaDts).toDateString();

    bookingDetails.booking.bookingOutbound.from =
      bookingData.fromAddress.property + ', ' + bookingData.fromAddress.postcode;
    bookingDetails.booking.bookingOutbound.to =
      bookingData.toAddress.property + ', ' + bookingData.toAddress.postcode;

    bookingDetails.booking.bookingOutbound.bookingRef = bookingData.bookingOutbound.bookingLinktext;
    bookingDetails.booking.bookingOutbound.bookingRefMD5 =
      require('crypto').createHash('md5').update(bookingData.bookingOutbound.bookingLinktext).digest('hex');
    bookingDetails.booking.bookingOutbound.mcoId = bookingData.bookingOutbound.mcoId;
    bookingDetails.booking.bookingOutbound.earylCancellation = bookingData.bookingOutbound.earlyCancellationTs;

    bookingDetails.booking.bookingOutbound.tripCharges = bookingData.bookingOutbound.tripCharges;
    bookingDetails.booking.bookingOutbound.payCommission =
      bookingData.bookingOutbound.tripCharges * bookingData.bookingOutbound.minicabCommission;
    bookingDetails.booking.bookingOutbound.vat = bookingDetails.booking.bookingOutbound.payCommission * 0.2;
    bookingDetails.booking.bookingOutbound.youRetain = bookingDetails.booking.bookingOutbound.tripCharges -
      bookingDetails.booking.bookingOutbound.payCommission -
      bookingDetails.booking.bookingOutbound.vat;


    bookingDetails.booking.carType = bookingData.bookingOutbound.carType;
    bookingDetails.booking.priceType = bookingData.bookingOutbound.priceType;
    bookingDetails.booking.additionalInfo = bookingData.bookingOutbound.additionalInformation;
    bookingDetails.booking.grossAmount = bookingData.bookingOutbound.grossAmount;
    bookingDetails.booking.numbOfPassengers = bookingData.bookingOutbound.numberOfPassengers;
    bookingDetails.booking.perPassenger = bookingData.bookingOutbound.grossAmount /
      bookingData.bookingOutbound.numberOfPassengers;

    bookingDetails.customer = bookingData.customer;
    bookingDetails.chauffeurit = bookingData.bookingOutbound.chauffeurit;

    var vias = '';
    var reVias = '';
    if (bookingData.viasAddresses) {
      for (var i = 0; i < bookingData.viasAddresses.length; ++i) {
        vias += bookingData.viasAddresses[i].property + ', ' + bookingData.viasAddresses[i].postCode + '; ';
      }

      var reVia = _.reverse(bookingData.viasAddresses);
      for (var i = 0; i < reVia.length; ++i) {
        reVias += reVia[i].property + ', ' + reVia[i].postCode + '; ';
      }
    }
    bookingDetails.booking.via = vias;
    bookingDetails.booking.reVia = reVias;

    var luggageAll = '';
    if (bookingData.bookingOutbound.luggageJson) {
      var luggage = JSON.parse(bookingData.bookingOutbound.luggageJson);

      for (var lugg in luggage) {
        if (luggage.hasOwnProperty(lugg)) {
          luggageAll += luggage[lugg].count + 'x ' + lugg + ' ';
        }
      }
    }
    bookingDetails.booking.luggage = luggageAll;


    if (bookingData.bookingOutbound.chauffeurit === 0) {
      bookingDetails.booking.preBody = 'You have a new booking on minicabit';
    } else if (bookingData.bookingOutbound.chauffeurit === 1) {
      bookingDetails.booking.preBody = 'You have a new booking on chauffeurit';
    }

    if (bookingData.bookingOutbound.returnRtaDts) {
      var UKTime = moment.utc(bookingData.bookingOutbound.returnEtaDt);
      bookingDetails.booking.bookingOutbound.returnTime = UKTime.format('HH:mm');
    }

    if (bookingData.bookingOutbound.waitingTime && bookingData.bookingOutbound.waitingTime != 0) {
      bookingDetails.booking.waitingMinutes = bookingData.bookingOutbound.waitingTime % 60;
      bookingDetails.booking.waitingHrs = Math.floor(bookingData.bookingOutbound.waitingTime / 60);

      var UKTime = moment.utc(bookingData.bookingOutbound.collectEtaDts);
      bookingDetails.booking.collectionTime = UKTime.format('HH:mm');
    }

    bookingDetails.booking.customerSupportNumber = '+44 (0) 1234 868 568';

    if (bookingData.payments && bookingData.payments.cardData && bookingData.payments.type.toUpperCase() === 'CARD') {
      bookingDetails.booking.billingFirstname = bookingData.payments.cardData.transaction.billing.firstName;
      bookingDetails.booking.billingSurname = bookingData.payments.cardData.transaction.billing.lastName;
      bookingDetails.booking.billingAddress = bookingData.payments.cardData.transaction.billing.streetAddress;
      bookingDetails.booking.billingCity = bookingData.payments.cardData.transaction.billing.region;
      bookingDetails.booking.billingPostCode = bookingData.payments.cardData.transaction.billing.postalCode;
      bookingDetails.booking.billingCountry = bookingData.payments.cardData.transaction.billing.countryName;
    }


    bookingDetails.booking.isPingit = bookingData.bookingOutbound.isPingit;
    bookingDetails.booking.tripType = bookingData.reqData.tripType.toUpperCase();
    bookingDetails.booking.paymentMethod = bookingData.payments.type.toUpperCase();

    if (bookingData.reqData.tripType.toUpperCase() === 'SINGLE' && bookingData.payments.type.toUpperCase() === 'CASH') {
      bookingDetails.booking.line = 'SINGLE';
      if (bookingDetails.chauffeurit === 1) {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.chauffeuritBookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.chauffeuritSingleTripConfirmationMco;
      } else {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.bookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.singleTripConfirmationMco;
      }
    } else if (bookingData.reqData.tripType.toUpperCase() === 'SINGLE' &&
      bookingData.payments.type.toUpperCase() === 'CARD') {
      bookingDetails.booking.line = 'SINGLE';
      if (bookingDetails.chauffeurit === 1) {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.chauffeuritBookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.chauffeuritSingleTripConfirmationMco;
      } else {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.bookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.singleTripConfirmationMcoCard;
      }
    } else if (bookingData.reqData.tripType.toUpperCase() === 'RETURN' &&
      bookingData.payments.type.toUpperCase() === 'CASH') {
      bookingDetails.booking.line = 'RETURN';
      if (bookingDetails.chauffeurit === 1) {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.chauffeuritBookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.chauffeuritReturnTripConfirmationMco;
      } else {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.bookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.returnTripConfirmationMco;
      }
    } else if (bookingData.reqData.tripType.toUpperCase() === 'RETURN' &&
      bookingData.payments.type.toUpperCase() === 'CARD') {
      bookingDetails.booking.line = 'RETURN';
      if (bookingDetails.chauffeurit === 1) {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.chauffeuritBookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.chauffeuritReturnTripConfirmationMco;
      } else {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.bookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.returnTripConfirmationMcoCard;
      }
    } else if (bookingData.reqData.tripType.toUpperCase() === 'SPLIT' &&
      bookingData.payments.type.toUpperCase() === 'CASH') {
      bookingDetails.booking.line = 'SPLIT';
      if (bookingDetails.chauffeurit === 1) {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.chauffeuritBookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.chauffeuritSingleTripConfirmationMco;
      } else {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.bookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.singleTripConfirmationMco;
      }
    } else if (bookingData.reqData.tripType.toUpperCase() === 'SPLIT' &&
      bookingData.payments.type.toUpperCase() === 'CARD') {
      bookingDetails.booking.line = 'SPLIT';
      if (bookingDetails.chauffeurit === 1) {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.chauffeuritBookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.chauffeuritSingleTripConfirmationMco;
      } else {
        bookingDetails.emailTemplate = config.thirdPartyApis.mandrill.templates.bookingConfirmationCustomer;
        bookingDetails.emailTemplateMco = config.thirdPartyApis.mandrill.templates.singleTripConfirmationMcoCard;
      }
    } else {
      logger.error('Unknown booking type ' + bookingData.reqData.tripType);
      return ({err: 'Unknown booking type', result: null});
    }

    return ({err: null, result: bookingDetails});
  } catch (e) {
    return ({err: e, result: null});
  }
}

nm.sendWeeklyStatement = function(data, callback) {
  var templateContent = [];
  var fromMoment = moment(data.fromDate);
  var toMoment = moment(data.toDate);
  var message = {
    'to': [],
    'global_merge_vars': [
      {
        'name': 'from_date',
        'content': fromMoment.format(fromMoment.year() === toMoment.year() ? 'DD MMMM' : 'DD MMMM YYYY')
      },
      {
        'name': 'to_date',
        'content': toMoment.format('DD MMMM YYYY')
      },
      {
        'name': 'domainNameB2B',
        'content': 'https://admin.minicabit.com'
      }
    ],
    'merge_vars': []
  };
  _.each(data.statements, function(statement, i) {
    var toEmail = statement.mcoOfficeEmail;
    // if (process.env.NODE_ENV !== 'production' && i === 0) {
    //   toEmail = 'knightang11@gmail.com';
    // }
    message.to.push({
      'email': toEmail,
      'name': statement.mcoRegisteredName,
      'type': 'to'
    });
    message.merge_vars.push({
      'rcpt': toEmail,
      'vars': [
        {
          'name': 'week_total',
          'content': statement.totalPhoPay ? statement.totalPhoPay.toFixed(2) : '0.00'
        },
        {
          'name': 'vat',
          'content': statement.vatOnCommission ? statement.vatOnCommission.toFixed(2) : '0.00'
        },
        {
          'name': 'total',
          'content': statement.totalPhoPay ? statement.totalPhoPay.toFixed(2) : '0.00'
        }
      ]
    });
  });

  mandrillEmail.sendEmail(config.thirdPartyApis.mandrill.templates.weeklyStatement,
    templateContent, message, function(err, result) {
      if (!err) {
        logger.info('Weekly statement sent to Mcos');
      } else {
        logger.error('Failed to send weekly statement');
      }
      callback(err, result);
    });
};
