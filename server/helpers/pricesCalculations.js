/**
 * Created by dejan on 29/08/16.
 */

'use strict';

var sap = exports = module.exports = {};

sap.calculateAllPrices = function(outboundBooking, inboundBooking, vatRate) {
  var bookingAccountingData = {
    bookingOutbound: {
      bookingId: null,
      bookingLinktext: null,
      paymentType: null,
      transactionValue: null,
      tripPrice: null,
      bookingFee: null,
      tripPriceLessPromo: null,
      commissionRate: null,
      vatRate: null,
      commission: null,
      phoPay: null,
      promoDeduction: null,
      transactionCharge: null,
      income: null,
      netCommission: null,
      netBookingFee: null,
      netTransactionCharge: null,
      netIncome: null,
      netIncomeLessPromo: null
    },
    bookingInbound: {
      bookingId: null,
      bookingLinktext: null,
      paymentType: null,
      transactionValue: null,
      tripPrice: null,
      bookingFee: null,
      tripPriceLessPromo: null,
      commissionRate: null,
      vatRate: null,
      commission: null,
      phoPay: null,
      promoDeduction: null,
      transactionCharge: null,
      income: null,
      netCommission: null,
      netBookingFee: null,
      netTransactionCharge: null,
      netIncome: null,
      netIncomeLessPromo: null
    }
  };

  if (outboundBooking) {
    bookingAccountingData.bookingOutbound.bookingId = outboundBooking.bookingId;
    bookingAccountingData.bookingOutbound.bookingLinktext = outboundBooking.bookingLinktext;
    bookingAccountingData.bookingOutbound.transactionValue = parseFloat(outboundBooking.grossAmount);
    bookingAccountingData.bookingOutbound.tripPrice = parseFloat(outboundBooking.tripCharges);
    bookingAccountingData.bookingOutbound.bookingFee = parseFloat(outboundBooking.bookingFees);
    bookingAccountingData.bookingOutbound.commissionRate = parseFloat(outboundBooking.minicabCommission);
    bookingAccountingData.bookingOutbound.transactionCharge = parseFloat(outboundBooking.ccCharges);
    bookingAccountingData.bookingOutbound.vatRate = parseFloat(vatRate);
    bookingAccountingData.bookingOutbound.commission = (bookingAccountingData.bookingOutbound.tripPrice *
      (bookingAccountingData.bookingOutbound.commissionRate)) *
      (1 + (bookingAccountingData.bookingOutbound.vatRate / 100));
    bookingAccountingData.bookingOutbound.phoPay = bookingAccountingData.bookingOutbound.tripPrice -
      bookingAccountingData.bookingOutbound.commission;
    bookingAccountingData.bookingOutbound.income = bookingAccountingData.bookingOutbound.commission +
      bookingAccountingData.bookingOutbound.bookingFee + bookingAccountingData.bookingOutbound.transactionCharge;
    bookingAccountingData.bookingOutbound.netCommission = bookingAccountingData.bookingOutbound.commission /
      (1 + (bookingAccountingData.bookingOutbound.vatRate / 100));
    bookingAccountingData.bookingOutbound.netBookingFee = bookingAccountingData.bookingOutbound.bookingFee /
      (1 + (bookingAccountingData.bookingOutbound.vatRate / 100));
    bookingAccountingData.bookingOutbound.netTransactionCharge =
      bookingAccountingData.bookingOutbound.transactionCharge /
      (1 + (bookingAccountingData.bookingOutbound.vatRate / 100));
    bookingAccountingData.bookingOutbound.netIncome = bookingAccountingData.bookingOutbound.income /
      (1 + (bookingAccountingData.bookingOutbound.vatRate / 100));
  }

  if (inboundBooking) {
    bookingAccountingData.bookingInbound.bookingId = inboundBooking.bookingId;
    bookingAccountingData.bookingInbound.bookingLinktext = inboundBooking.bookingLinktext;
    bookingAccountingData.bookingInbound.transactionValue = parseFloat(inboundBooking.grossAmount);
    bookingAccountingData.bookingInbound.tripPrice = parseFloat(inboundBooking.tripCharges);
    bookingAccountingData.bookingInbound.bookingFee = parseFloat(inboundBooking.bookingFees);
    bookingAccountingData.bookingInbound.commissionRate = parseFloat(inboundBooking.minicabCommission);
    bookingAccountingData.bookingInbound.transactionCharge = parseFloat(inboundBooking.ccCharges);
    bookingAccountingData.bookingInbound.vatRate = parseFloat(vatRate);
    bookingAccountingData.bookingInbound.commission = (bookingAccountingData.bookingInbound.tripPrice *
      (bookingAccountingData.bookingInbound.commissionRate)) *
      (1 + (bookingAccountingData.bookingInbound.vatRate / 100));
    bookingAccountingData.bookingInbound.phoPay = bookingAccountingData.bookingInbound.tripPrice -
      bookingAccountingData.bookingInbound.commission;
    bookingAccountingData.bookingInbound.income = bookingAccountingData.bookingInbound.commission +
      bookingAccountingData.bookingInbound.bookingFee + bookingAccountingData.bookingInbound.transactionCharge;
    bookingAccountingData.bookingInbound.netCommission = bookingAccountingData.bookingInbound.commission /
      (1 + (bookingAccountingData.bookingInbound.vatRate / 100));
    bookingAccountingData.bookingInbound.netBookingFee = bookingAccountingData.bookingInbound.bookingFee /
      (1 + (bookingAccountingData.bookingInbound.vatRate / 100));
    bookingAccountingData.bookingInbound.netTransactionCharge =
      bookingAccountingData.bookingInbound.transactionCharge /
      (1 + (bookingAccountingData.bookingInbound.vatRate / 100));
    bookingAccountingData.bookingInbound.netIncome = bookingAccountingData.bookingInbound.income /
      (1 + (bookingAccountingData.bookingInbound.vatRate / 100));
  } else {
    bookingAccountingData.bookingInbound = null;
  }


  if (outboundBooking.isPaypal) {
    bookingAccountingData.bookingOutbound.paymentType = 'paypal';
    if (inboundBooking) {
      bookingAccountingData.bookingInbound.paymentType = 'paypal';
    }
  } else if (outboundBooking.isPingit) {
    bookingAccountingData.bookingOutbound.paymentType = 'pingit';
    if (inboundBooking) {
      bookingAccountingData.bookingInbound.paymentType = 'pingit';
    }
  } else {
    bookingAccountingData.bookingOutbound.paymentType = outboundBooking.paymentType;
    if (inboundBooking) {
      bookingAccountingData.bookingInbound.paymentType = outboundBooking.paymentType;
    }
  }

  if (outboundBooking.promotionalCode && outboundBooking.promotionalCode.discount) {
    if (inboundBooking) {
      bookingAccountingData.bookingInbound.promoDeduction = outboundBooking.promotionalCode.discount / 2;
      bookingAccountingData.bookingOutbound.promoDeduction = outboundBooking.promotionalCode.discount / 2;
      bookingAccountingData.bookingOutbound.tripPriceLessPromo = parseFloat(outboundBooking.tripCharges) -
        outboundBooking.promotionalCode.discount * 0.5;
      bookingAccountingData.bookingInbound.tripPriceLessPromo = parseFloat(inboundBooking.tripCharges) -
        outboundBooking.promotionalCode.discount * 0.5;
    } else {
      bookingAccountingData.bookingOutbound.promoDeduction = outboundBooking.promotionalCode.discount;
      bookingAccountingData.bookingOutbound.tripPriceLessPromo = parseFloat(outboundBooking.tripCharges) -
        outboundBooking.promotionalCode.discount;
    }
  } else if (outboundBooking.promotionalCode && outboundBooking.promotionalCode.percent) {
    var tmpAmount = null;
    if (inboundBooking) {
      tmpAmount = (parseFloat(outboundBooking.tripCharges) + parseFloat(inboundBooking.tripCharges)) *
        (outboundBooking.promotionalCode.percent / 100);
      bookingAccountingData.bookingInbound.promoDeduction = tmpAmount / 2;
      bookingAccountingData.bookingOutbound.promoDeduction = tmpAmount / 2;
      bookingAccountingData.bookingOutbound.tripPriceLessPromo = parseFloat(outboundBooking.tripCharges) -
        tmpAmount * 0.5;
      bookingAccountingData.bookingInbound.tripPriceLessPromo = parseFloat(inboundBooking.tripCharges) -
        tmpAmount * 0.5;
    } else {
      tmpAmount = outboundBooking.tripCharges * (outboundBooking.promotionalCode.percent / 100);
      bookingAccountingData.bookingOutbound.promoDeduction = tmpAmount;
      bookingAccountingData.bookingOutbound.tripPriceLessPromo = parseFloat(outboundBooking.tripCharges) - tmpAmount;
    }
  } else {
    bookingAccountingData.bookingOutbound.tripPriceLessPromo = parseFloat(outboundBooking.tripCharges);
    if (inboundBooking) {
      bookingAccountingData.bookingInbound.tripPriceLessPromo = parseFloat(inboundBooking.tripCharges);
    }
  }

  bookingAccountingData.bookingOutbound.netIncomeLessPromo = bookingAccountingData.bookingOutbound.netIncome -
    bookingAccountingData.bookingOutbound.promoDeduction;

  if (inboundBooking) {
    bookingAccountingData.bookingInbound.netIncomeLessPromo = bookingAccountingData.bookingInbound.netIncome -
      bookingAccountingData.bookingInbound.promoDeduction;
  }

  return bookingAccountingData;
};

