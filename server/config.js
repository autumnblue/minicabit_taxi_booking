'use strict';

var p = require('../package.json');
var version = p.version.split('.').shift();

module.exports = {
  restApiRoot: '/api' + (version > 0 ? '/v' + version : ''),
  host: process.env.HOST || 'localhost',
  port: process.env.PORT || 3000,
  loggingLevel: process.env.LOGLEVEL || 'debug',

  customApiRoot: '/api/custom',

  thirdPartyApis: {
    minicabit: {
      host: process.env.MINICABIT_API_ENDPOINT || 'https://api-dev.minicabit.com/v1',
      key: {
        name: process.env.MINICABIT_API_KEY_VALUE || 'x-minicabit-apikey-id',
        value: process.env.MINICABIT_API_KEY_VALUE || '42a87a9d-fd6c-11e5-b0d9-22000b0166cf'
      }
    },
    mandrill: {
      key: process.env.MANDRILL_API_KEY || 'fx8hDUsyRO9b0_2ouQ6YNA', //'z55nKoaWZMQ5DgcxzhMasA',
      templates: {
        emailVerification: 'email_verification',
        earlyBookingCancellationCustomer: 'email-customer-early-cancellation',
        lateBookingCancellationCustomer: 'email-customer-late-cancellation',
        earlyBookingCancellationPHO: 'email-pho-early-cancellation',
        lateBookingCancellationPHO: 'email-pho-late-cancellation',
        chauffeuritEarlyBookingCancellationCustomer: 'chauffeurit_email_customer_early_cancellation',
        chauffeuritLateBookingCancellationCustomer: 'chauffeurit_email_customer_late_cancellation',
        chauffeuritEarlyBookingCancellationPHO: 'email-pho-early-cancellation', //the same template like for minicabit
        chauffeuritLateBookingCancellationPHO: 'email-pho-late-cancellation', //the same template like for minicabit
        chauffeuritBookingConfirmationCustomer: 'chauffeurit_new_trip_confirmation',
        chauffeuritSingleTripConfirmationMco: 'chauffeurit_single_trip_confirmation_mco',
        chauffeuritReturnTripConfirmationMco: 'chauffeurit_return_trip_confirmation_mco',
        bookingConfirmationCustomer: 'new_trip_confirmation',
        singleTripConfirmationMco: 'single_trip_confirmation_mco',
        returnTripConfirmationMco: 'return_trip_confirmation_mco',
        singleTripConfirmationMcoCard: 'single_trip_confirmation_mco_card',
        returnTripConfirmationMcoCard: 'return_trip_confirmation_mco_card',
        emailQuote: 'email_quote',
        emailForgottenPassword: 'password_reset_email',
        weeklyStatement: 'weekly_statement'
      }
    }
  }
};
