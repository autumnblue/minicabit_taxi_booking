'use strict';

var winston = require('winston');
var config = require('./config.' + process.env.NODE_ENV + '.js');

var logger = new (winston.Logger)({
  transports: [
    //new (winston.transports.DailyRotateFile)({ // roate daily
    new (winston.transports.File)({ // normal size rotation
      level: config.loggingLevel,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, //5MB
      maxFiles: 20,
      colorize: false,
      filename: 'server/api.log.json'
    }),
    new winston.transports.Console({
      level: config.loggingLevel,
      handleExceptions: true,
      timestamp: true,
      json: false,
      colorize: true
    })
  ]
});

module.exports = logger;
