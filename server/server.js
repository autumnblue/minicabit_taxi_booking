'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname);


// Serve client app
var serveClient = process.argv.indexOf('--serve-client') > -1;
var apiOnly = process.argv.indexOf('--api-only') > -1;

if (serveClient && !apiOnly && !process.env.TESTING) {
  // Set up webpack-dev-middleware and webpack-hot-middleware
  var webpack = require('webpack');
  var webpackDevMiddleware = require('webpack-dev-middleware');
  var webpackHotMiddleware = require('webpack-hot-middleware');
  var webpackConfig = require('../tools/webpack.config');
  var compiler = webpack(webpackConfig);

  app.use(webpackDevMiddleware(compiler, {
    noInfo: false,
    publicPath: webpackConfig.output.publicPath,
    stats: webpackConfig.stats
  }));
  app.use(webpackHotMiddleware(compiler));

  // serve static files for client app
  app.use(loopback.static(__dirname + '/../.build'));

  // serve index.html for client app page view request
  app.all('*', function(req, res, next) {
    res.sendFile('index.html', { root: __dirname + '/../.build/' });
  });
}


app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};


// start the server if `$ node server.js`
if (require.main === module) {
  app.start();
}
