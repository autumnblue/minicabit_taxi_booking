'use strict';

module.exports = function(app) {
  // find and register custom routes in `routes` directory recursively
  var customRoutes = require('require-dir')('./routes', { recurse: true });

  Object.keys(customRoutes).forEach(function(_key) {
    if (typeof customRoutes[_key] === 'function') {
      customRoutes[_key](app);
    }
  });
};
