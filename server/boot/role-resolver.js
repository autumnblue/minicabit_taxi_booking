'use strict';

module.exports = function(server) {

  /**
   * Our 5 roles and role-user mappings are stored in the database. They are not defined dynamically at run-time.
   * Don't be confused here. Permissions they have are dynamic.
   * However, the roles themselves are not dynamic, but static ones.
   * Thus, we do not need custom role handlers.
   */

  // not deleted just for future reference
  /*var Role = server.models.role;
   var RoleMapping = server.models.roleMapping;

   Role.registerResolver('Administrator', function(role, context, cb) {
   function reject() {
   process.nextTick(function() {
   cb(null, false);
   });
   }

   //If anonymous, reject
   if (!context.accessToken.id) {
   return reject();
   }

   var userId = context.accessToken.userId;

   if (!userId) {
   return reject();
   }

   Role.findOne({ where: { name: role } }, function(err, r) {
   if (err) {
   return cb(err);
   } else {
   RoleMapping.find({ where: { and: [{ principalId: userId }, { roleId: r.id }] } }, function(err, result) {
   if (err) {
   return cb(err);
   } else {
   if (result.length > 0) {
   return cb(null, true);
   } else {
   return reject();
   }
   }
   });
   }
   });
   });

   Role.registerResolver('Operater', function(role, context, cb) {
   function reject() {
   process.nextTick(function() {
   cb(null, false);
   });
   }

   //If anonymous, reject
   if (!context.accessToken.id) {
   return reject();
   }

   var userId = context.accessToken.userId;
   if (!userId) {
   return reject();
   }

   Role.findOne({ where: { name: role } }, function(err, r) {
   if (err) {
   return cb(err);
   } else {
   RoleMapping.find({ where: { and: [{ principalId: userId }, { roleId: r.id }] } }, function(err, result) {
   if (err) {
   return cb(err);
   } else {
   if (result.length > 0) {
   return cb(null, true);
   } else {
   return reject();
   }
   }
   });
   }
   });
   });

   Role.registerResolver('MiniCabOperator', function(role, context, cb) {
   function reject() {
   process.nextTick(function() {
   cb(null, false);
   });
   }

   //If anonymous, reject
   if (!context.accessToken.id) {
   return reject();
   }

   var userId = context.accessToken.userId;
   if (!userId) {
   return reject();
   }

   Role.findOne({ where: { name: role } }, function(err, r) {
   if (err) {
   return cb(err);
   } else {
   RoleMapping.find({ where: { and: [{ principalId: userId }, { roleId: r.id }] } }, function(err, result) {
   if (err) {
   return cb(err);
   } else {
   if (result.length > 0) {
   return cb(null, true);
   } else {
   return reject();
   }
   }
   });
   }
   });
   });*/

};
