'use strict';
var apiService = require('../../services/api');
var _ = require('lodash');

module.exports = function(app) {

  var mcApiConfig = app.get('thirdPartyApis').minicabit;

  app.route(app.get('customApiRoot') + '/pca/find').get(function(req, res) {
    // var query = req.query['q'];
    // var country = req.query['country'];
    // var lastId = req.query['lastId'];
    // var limit = req.query['limit'];
    apiService.get(
      mcApiConfig.host + '/pca/find',
      req.query,
      { name: mcApiConfig.key.name, value: mcApiConfig.key.value },
      function(err, data) {
        if (err) {
          res.status(err.statusCode).json({ error: err });
        } else {
          res.json(data);
        }
      }
    );
  });

  app.route(app.get('customApiRoot') + '/pca/:id').get(function(req, res) {

    if (_.isEmpty(req.params['id'])) {
      return res.statusCode(400).json({ error: { message: 'Missing id' } });
    }

    apiService.get(
      mcApiConfig.host + '/pca/' + req.params['id'],
      null,
      { name: mcApiConfig.key.name, value: mcApiConfig.key.value },
      function(err, data) {
        if (err) {
          res.status(err.statusCode).json({ error: err });
        } else {
          res.json(data);
        }
      }
    );
  });
};
